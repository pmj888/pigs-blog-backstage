package com.pigs.pigsblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pigs.pigsblog.entity.pgArtitleSortRef;
import org.springframework.stereotype.Repository;

@Repository
public interface pgArtitleSortRefMapper extends BaseMapper<pgArtitleSortRef> {
}
