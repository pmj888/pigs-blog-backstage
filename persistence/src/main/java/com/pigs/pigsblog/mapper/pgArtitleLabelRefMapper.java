package com.pigs.pigsblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pigs.pigsblog.entity.pgArtitleLabelRef;
import org.springframework.stereotype.Repository;

@Repository
public interface pgArtitleLabelRefMapper extends BaseMapper<pgArtitleLabelRef> {



}
