package com.pigs.pigsblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgComments;
import com.pigs.pigsblog.model.ResultFormatPaging;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * 评论管理表
 * 
 * @author manager
 * @email *****@mail.com
 * @date 2020-06-01 11:07:48
 */
@Repository
public interface PgCommentsMapper extends BaseMapper<PgComments> {
	IPage<PgComments>findListByPage(Page<PgComments> page);
  /* IPage<PgComments> findListByPage(@Param("pagIng") Page PaIng,@Param("PgComments") PgComments pgComments);
*/
}
