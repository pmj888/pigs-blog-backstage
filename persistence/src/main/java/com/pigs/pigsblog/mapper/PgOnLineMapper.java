package com.pigs.pigsblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pigs.pigsblog.entity.PgOnLine;
import org.springframework.stereotype.Repository;


@Repository
public interface PgOnLineMapper extends BaseMapper<PgOnLine> {
}
