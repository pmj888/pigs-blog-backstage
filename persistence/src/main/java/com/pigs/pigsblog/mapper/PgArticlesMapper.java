package com.pigs.pigsblog.mapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgArticles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Repository
public interface PgArticlesMapper extends BaseMapper<PgArticles> {

    /**
     * 查询 全部文章 并分页
     * @param articleTitle
     * @param page
     * @return
     */
    IPage<PgArticles> queryPgArticlesList(String articleTitle, Page<PgArticles> page);


    /**
     * 发布文章
     * @param pgArticles
     * @return
     */
    Integer insertPgArticles(PgArticles pgArticles);

    /**
     * 查询分类使用量
     * @param sortId
     * @return
     */
    @Select("select count(1) from pg_artitle_sort_ref where sort_id=#{sortId}")
    public Integer selectCountArtitleSortBySortId(Long sortId);

}
