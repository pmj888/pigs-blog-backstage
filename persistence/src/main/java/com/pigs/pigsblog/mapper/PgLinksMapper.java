package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgLinks;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 友情鏈接 Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-05-30
 */
@Repository
public interface PgLinksMapper extends BaseMapper<PgLinks> {

}
