package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgRoleMenuRef;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-05-09
 */
@Repository
public interface PgRoleMenuRefMapper extends BaseMapper<PgRoleMenuRef> {

    /**
     * 通过角色id查询菜单权限
     *
     * @param roleId
     * @return
     */
    @Select("select pg_menu.menu_id as value,pg_menu.menu_name as name  from pg_menu left join pg_role_menu_ref on pg_role_menu_ref.menu_id=pg_menu.menu_id where role_id=#{roleId}")
    public List<PgMenuVo> queryRoleMenuRefListByRoleId(Long roleId);

    /**
     * 通过角色ID删除角色和菜单关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Delete("delete from pg_role_menu_ref where role_id=#{roleId}")
    public Integer deleteRoleMenuByRoleId(Long roleId);
}
