package com.pigs.pigsblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pigs.pigsblog.entity.PgSystemLog;
import org.springframework.stereotype.Repository;

/**
 * @author 安详的苦丁茶
 * @date 2020/6/2011:09
 */

@Repository
public interface PgSystemLogMapper extends BaseMapper<PgSystemLog> {
}
