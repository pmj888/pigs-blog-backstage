package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgLabels;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;



/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Repository
public interface PgLabelsMapper extends BaseMapper<PgLabels> {

}
