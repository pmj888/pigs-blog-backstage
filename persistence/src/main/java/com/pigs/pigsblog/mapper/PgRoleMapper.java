package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Repository
public interface PgRoleMapper extends BaseMapper<PgRole> {

    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    @Select("select count(1) from pg_role_menu_ref where menu_id=#{menuId} ")
    public Integer selectCountRoleMenuByMenuId(Long menuId);
}
