package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgArticlesImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 * 文章封面图
 * @author PIGS
 * @since 2020-05-20
 */
@Repository
public interface PgArticlesImgMapper extends BaseMapper<PgArticlesImg> {

}
