package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgSorts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */

@Repository
public interface PgSortsMapper extends BaseMapper<PgSorts> {

}


