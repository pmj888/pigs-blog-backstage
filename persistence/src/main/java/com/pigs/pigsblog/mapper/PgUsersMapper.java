package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgUsers;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Repository
public interface PgUsersMapper extends BaseMapper<PgUsers> {

    /**
     * 通过用户名查询信息
     * @param userName
     * @return
     */
    public PgUsers queryByName(String userName);

    /**
     * 查询用户信息
     * @param pgUsers
     * @return
     */
    public PgUsers queryUserInfo(@Param("pgUsers") PgUsers pgUsers);
}
