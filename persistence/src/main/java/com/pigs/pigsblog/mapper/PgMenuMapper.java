package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pigs.pigsblog.entity.PgRoleMenuRef;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-05-09
 */
@Repository
public interface PgMenuMapper extends BaseMapper<PgMenu> {

    /**
     * 级联查询父菜单
     *
     * @return
     */
    List<PgMenuVo> getAllMenus();

    /**
     * 级联查询子菜单
     *
     * @return
     */
    List<PgMenuVo> findMenuByParentId();

    /**
     * 查询菜单数量
     *
     * @param parentId 菜单父ID
     * @return 结果
     */
    @Select("select count(1) from pg_menu where parent_id=#{menuId} and state!=2 ")
    public Integer queryCountMenuByParentId(Long parentId);

    /**
     * 通过菜单id
     * 查询上级菜单
     * 查询菜单权限
     *
     * @param menuId
     * @return
     */
    @Select("SELECT t.menu_id, t.parent_id, t.menu_name, t.order_num, t.url, t.menu_type, t.perms, t.icon, t.remark, (SELECT menu_name FROM pg_menu WHERE menu_id = t.parent_id) parentName FROM pg_menu t where t.menu_id =#{menuId} ")
    public PgMenu queryMenuByMenuId(Long menuId);
}
