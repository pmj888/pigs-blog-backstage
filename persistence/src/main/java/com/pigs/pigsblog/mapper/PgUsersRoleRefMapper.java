package com.pigs.pigsblog.mapper;

import com.pigs.pigsblog.entity.PgUsersRoleRef;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author PIGS
 * @since 2020-05-04
 */
@Repository
public interface PgUsersRoleRefMapper extends BaseMapper<PgUsersRoleRef> {

}
