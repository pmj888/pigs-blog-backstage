package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgUsers;
import com.pigs.pigsblog.entity.PgUsersRoleRef;
import com.pigs.pigsblog.mapper.PgUsersMapper;
import com.pigs.pigsblog.mapper.PgUsersRoleRefMapper;
import com.pigs.pigsblog.service.PgUsersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.RedisUtil;
import com.pigs.pigsblog.util.SecurityUtils;
import com.pigs.pigsblog.util.SystemUtils;
import com.pigs.pigsblog.util.TimeUtitl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Service
public class PgUsersServiceImpl extends ServiceImpl<PgUsersMapper, PgUsers> implements PgUsersService {

    @Autowired
    private PgUsersMapper pgUsersMapper;

    private Logger logger = LoggerFactory.getLogger(PgUsersServiceImpl.class);

    @Autowired
    private PgUsersRoleRefMapper pgUsersRoleRefMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 通过用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @Override
    public PgUsers queryByName(String userName) {
        PgUsers pgUsersList = pgUsersMapper.queryByName(userName);
        if (pgUsersList != null) {
            return pgUsersList;
        }
        return null;
    }

    /**
     * 查询用户并分页
     * 实现类
     *
     * @param page
     * @param pgUsers
     * @return
     */
    @Override
    public IPage<PgUsers> queryUserList(PgUsers pgUsers, Integer page, Integer limit) {
        AbstractWrapper<PgUsers, String, QueryWrapper<PgUsers>> wrapper = new QueryWrapper<>();

        if (pgUsers != null) {
            if (!StringUtils.isEmpty(pgUsers.getUserName())) {
                wrapper.like("user_name", pgUsers.getUserName());
            }

        }
        wrapper.eq("user_state", pgUsers.getUserState());
        wrapper.orderByDesc("user_registration_time");
        Page<PgUsers> iPage = new Page<PgUsers>(page, limit);
        Page<PgUsers> pgUsersPage = pgUsersMapper.selectPage(iPage, wrapper);
        return pgUsersPage;
    }

    /**
     * 添加用户信息实现类
     *
     * @param pgUsers
     * @param roleId
     * @return
     */
    @Override
    public Integer saveUser(PgUsers pgUsers, Integer roleId) throws Exception {
        logger.info("pgUsers={}", pgUsers);

        /**
         * 从redis中查询用户名是否存在
         */
        List<Object> redisGetPgUsersId = redisUtil.lGet("pgUsersId", 0, -1);
        if (redisGetPgUsersId != null && redisGetPgUsersId.size() > 0) {
            logger.info("redisGetPgUsersId={}", redisGetPgUsersId);
            for (Object o : redisGetPgUsersId) {
                PgUsers redisGetPgUser = (PgUsers) redisUtil.get("pgUsers:" + o);
                if (redisGetPgUser != null) {
                    if (redisGetPgUser.getUserName().equals(pgUsers.getUserName())) {
                        logger.info("redisGetPgUser={}", redisGetPgUser);
                        return 101;
                    }
                }
            }
        }

        logger.info("---- redis没有当前的用户名存在，进入mysql查询 ", "----");
        /**
         * 查询数据库中登录名是否存在
         */
        AbstractWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name", pgUsers.getUserName());
        PgUsers pgUsersByName = pgUsersMapper.selectOne(wrapper);
        if (pgUsersByName != null) {
            logger.info("pgUsersByName={}", pgUsersByName);
            redisUtil.set("pgUsers:" + pgUsersByName.getUserId(), pgUsersByName);
            redisUtil.lSet("pgUsersId", pgUsersByName.getUserId());
            return 101;
        }

        /**
         * 如果数据中的不存在则添加
         */
        String salt = SecurityUtils.randomSalt();
        pgUsers.setSalt(salt);

        String password = SecurityUtils.encryptPassword(pgUsers.getUserPassword(), salt);
        pgUsers.setUserPassword(password);

        /**
         * 头像随机生成
         * 更好的生态化
         */
        String random = "";
        String[] doc = {"https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590812062178.jpeg",
                "https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590809782730.jpeg",
                "https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590809867485.jpeg",
                "https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590809945138.jpeg",
                "https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590811184855.jpeg",
                "https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590811132570.jpeg",
                "https://pigs.oss-cn-shenzhen.aliyuncs.com/image/casual-images/1590810016833.jpeg"};
        int index = (int) (Math.random() * doc.length);
        random = doc[index];
        pgUsers.setUserProfilePhoto(random);

        pgUsers.setUserRegistrationTime(TimeUtitl.dateTime());

        /**
         * 格式化当前时间然后获取年龄添加进去
         */
        Integer age = TimeUtitl.getAge(TimeUtitl.parse(pgUsers.getUserBirthday()));
        logger.info("age={}", age);
        if (age == 0) {
            age = 1;
        }
        pgUsers.setUserAge(age);

        Integer insert = pgUsersMapper.insert(pgUsers);
        if (insert > 0) {
            redisUtil.set("pgUsers:" + pgUsers.getUserId(), pgUsers);
            redisUtil.lSet("pgUsersId", pgUsers.getUserId());
            PgUsersRoleRef pgUsersRoleRef = new PgUsersRoleRef();
            pgUsersRoleRef.setRoleId(roleId);
            pgUsersRoleRef.setUsersId(pgUsers.getUserId());
            pgUsersRoleRef.setUsersRoleRefState(0);
            pgUsersRoleRef.setUsersRoleRefUpdateTime(TimeUtitl.dateTime());
            int insert1 = pgUsersRoleRefMapper.insert(pgUsersRoleRef);
            if (insert1 > 0) {
                redisUtil.set("pgUsersRoleRef:" + pgUsers.getUserId(), pgUsersRoleRef);
                return 1;
            }
        }

        return 100;
    }

    /**
     * 修改用户
     *
     * @param pgUsers
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public Integer editUser(PgUsers pgUsers, Integer roleId) throws Exception {

        /**
         * 从mysql中查询数据如果用户名不存在则修改
         * 如果存在并且不与通过id查询的数据相同就返回101
         * 如果当前的用户名与使用id查询的数据用户名相同则修改
         */
        PgUsers selectById = pgUsersMapper.selectById(pgUsers.getUserId());
        AbstractWrapper wrapper = new QueryWrapper<PgUsers>();
        wrapper.eq("user_name", pgUsers.getUserName());
        PgUsers selectOne = pgUsersMapper.selectOne(wrapper);
        if (selectById != null) {
            if (!selectById.getUserName().equals(pgUsers.getUserName())) {
                if (selectOne != null) {
                    return 101;
                }

            }
        }

        /**
         * 格式化当前时间然后获取年龄添加进去
         */
        Integer age = TimeUtitl.getAge(TimeUtitl.parse(pgUsers.getUserBirthday()));
        logger.info("age={}", age);
        if (age == 0) {
            age = 1;
        }
        pgUsers.setUserAge(age);

        if (SystemUtils.isEmpty(pgUsers.getUserPassword())) {
            String salt = SecurityUtils.randomSalt();
            pgUsers.setSalt(salt);
            String password = SecurityUtils.encryptPassword(pgUsers.getUserPassword(), salt);
            pgUsers.setUserPassword(password);
        } else {
            pgUsers.setUserPassword(null);
        }

        Integer updateById = pgUsersMapper.updateById(pgUsers);
        logger.info("updateById={}", updateById);


        PgUsersRoleRef pgUsersRoleRef = new PgUsersRoleRef();
        pgUsersRoleRef.setRoleId(roleId);
        pgUsersRoleRef.setUsersId(pgUsers.getUserId());

        Integer updateRole = pgUsersRoleRefMapper.updateById(pgUsersRoleRef);
        logger.info("updateRole={}", updateRole);

        if (updateById > 0 && updateRole > 0) {
            redisUtil.del("pgUsers:" + pgUsers.getUserId());
            redisUtil.del("pgUsersRoleRef:" + pgUsers.getUserId());
            return 1;
        }

        return 0;
    }

    /**
     * 查询用户信息
     *
     * @param pgUsers
     * @return
     */
    @Override
    public PgUsers queryUserInfo(PgUsers pgUsers) {
        logger.info("pgUsers={}", pgUsers);
        if (pgUsers != null) {
            AbstractWrapper wrapper = new QueryWrapper();

            if (pgUsers.getUserId() != null) {
                wrapper.eq("user_id", pgUsers.getUserId());
                PgUsers selectOne = pgUsersMapper.selectOne(wrapper);
                logger.info("selectOne ={}", selectOne);
                if (selectOne != null) {
                    return selectOne;
                }
            }

        }

        return null;
    }

    /**
     * 修改用户密码
     *
     * @param pgUsers
     * @return
     */
    @Override
    public Integer editUserPwd(PgUsers pgUsers, String formerPwd) {

        if (pgUsers != null) {
            if (pgUsers.getUserId() != null) {
                AbstractWrapper wrapper = new QueryWrapper();
                wrapper.eq("user_id", pgUsers.getUserId());
                PgUsers selectOne = pgUsersMapper.selectOne(wrapper);
                logger.info("selectOne={}", selectOne.getUserPassword());
                logger.info("getSalt={}", selectOne.getSalt());
                if (selectOne != null) {
                    if (SystemUtils.isEmpty(pgUsers.getUserPassword())) {
                        String password = SecurityUtils.encryptPassword(formerPwd, selectOne.getSalt());
                        logger.info("password={}", password);
                        if (!selectOne.getUserPassword().equals(password)) {
                            return 101;
                        }
                    }

                    if (SystemUtils.isEmpty(pgUsers.getUserPassword())) {
                        String salt = SecurityUtils.randomSalt();
                        pgUsers.setSalt(salt);
                        String password = SecurityUtils.encryptPassword(pgUsers.getUserPassword(), salt);
                        pgUsers.setUserPassword(password);
                        Integer updateById = pgUsersMapper.updateById(pgUsers);
                        logger.info("updateById={}", updateById);
                        if (updateById > 0) {
                            return updateById;
                        }
                    }


                }
            }

        }

        return 100;
    }


}
