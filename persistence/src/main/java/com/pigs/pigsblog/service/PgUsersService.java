package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgUsers;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
public interface PgUsersService extends IService<PgUsers> {


    /**
     * 通过用户名查询信息
     *
     * @param userName
     * @return
     */
    public PgUsers queryByName(String userName);

    /**
     * 查询用户并分页
     *
     * @param pgUsers
     * @param page
     * @param limit
     * @return
     */
    public IPage<PgUsers> queryUserList(PgUsers pgUsers, Integer page, Integer limit);

    /**
     * 添加用户
     *
     * @param pgUsers
     * @param roleId
     * @return
     * @throws Exception
     */
    public Integer saveUser(PgUsers pgUsers, Integer roleId) throws Exception;

    /**
     * 修改用户信息
     *
     * @param pgUsers
     * @param roleId
     * @return
     * @throws Exception
     */
    public Integer editUser(PgUsers pgUsers, Integer roleId) throws Exception;


    /**
     * 查询用户信息
     *
     * @param pgUsers
     * @return
     */
    public PgUsers queryUserInfo(PgUsers pgUsers);


    /**
     * 修改用户密码
     *
     * @param pgUsers
     * @param formerPwd
     * @return
     */
    public Integer editUserPwd(PgUsers pgUsers, String formerPwd);

}
