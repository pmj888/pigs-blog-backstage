package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pigs.pigsblog.entity.pgArtitleLabelRef;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 庞明杰
 * @since 2020-04-21
 */

public interface pgArtitleLabelRefService extends IService<pgArtitleLabelRef> {
}
