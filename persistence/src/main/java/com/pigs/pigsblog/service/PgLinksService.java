package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgLinks;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 友情鏈接 服务类
 * </p>
 *
 * @author PIGS
 * @since 2020-05-30
 */
public interface PgLinksService extends IService<PgLinks> {

    /**
     * 查询友情鏈接并分页
     *
     * @param pgLinks
     * @param page
     * @param limit
     * @return
     */
    public IPage<PgLinks> queryLinkList(PgLinks pgLinks, Integer page, Integer limit);

    /**
     * 添加友情鏈接
     *
     * @param pgLinks
     * @return
     */
    Integer saveLink(PgLinks pgLinks);

    /**
     * 編輯友情鏈接
     *
     * @param pgLinks
     * @return
     */
    Integer editLink(PgLinks pgLinks);

}
