package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgLinks;
import com.pigs.pigsblog.mapper.PgLinksMapper;
import com.pigs.pigsblog.service.PgLinksService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.TimeUtitl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 友情鏈接 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-05-30
 */
@Service
public class PgLinksServiceImpl extends ServiceImpl<PgLinksMapper, PgLinks> implements PgLinksService {

    @Autowired
    private PgLinksMapper pgLinksMapper;

    private Logger logger = LoggerFactory.getLogger(PgLinksServiceImpl.class);

    /**
     * 查询友情鏈接并分页
     *
     * @param pgLinks
     * @param page
     * @param limit
     * @return
     */
    @Override
    public IPage<PgLinks> queryLinkList(PgLinks pgLinks, Integer page, Integer limit) {

        AbstractWrapper wrapper = new QueryWrapper<PgLinks>();
        if (pgLinks != null) {
            if (!StringUtils.isEmpty(pgLinks.getWebsiteName())) {
                wrapper.like("website_name", pgLinks.getWebsiteName());
            }

        }
        wrapper.eq("website_state", 0);
        wrapper.orderByDesc("creation_time");
        Page<PgLinks> iPage = new Page<PgLinks>(page, limit);
        Page<PgLinks> pgLinksPage = pgLinksMapper.selectPage(iPage, wrapper);
        return pgLinksPage;
    }

    /**
     * 添加友情鏈接
     *
     * @param pgLinks
     * @return
     */
    @Override
    public Integer saveLink(PgLinks pgLinks) {
        pgLinks.setCreationTime(TimeUtitl.dateTime());
        pgLinks.setWebsiteState(0);

        Integer insert = pgLinksMapper.insert(pgLinks);
        return insert;
    }

    /**
     * 編輯友情鏈接
     *
     * @param pgLinks
     * @return
     */
    @Override
    public Integer editLink(PgLinks pgLinks) {
        Integer updateById = pgLinksMapper.updateById(pgLinks);
        return updateById;
    }
}
