package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.entity.pgArtitleSortRef;
import com.pigs.pigsblog.mapper.pgArtitleSortRefMapper;
import com.pigs.pigsblog.service.pgArtitleSortRefService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 庞明杰
 * @since 2020-04-21
 */
@Transactional
@Service
public class PgArtitleSortRefServiceImpl extends ServiceImpl<pgArtitleSortRefMapper, pgArtitleSortRef> implements pgArtitleSortRefService {

}
