package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgLabels;
import com.pigs.pigsblog.entity.PgSorts;
import com.pigs.pigsblog.mapper.PgArticlesMapper;
import com.pigs.pigsblog.mapper.PgSortsMapper;
import com.pigs.pigsblog.service.PgLabelsService;
import com.pigs.pigsblog.service.PgSortsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Service
@Transactional
public class PgSortsServiceImpl extends ServiceImpl<PgSortsMapper, PgSorts> implements PgSortsService {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(PgLabelsService.class);

    @Autowired
    private PgSortsMapper pgSortsMapper;

    @Autowired
    private PgArticlesMapper pgArticlesMapper;

    @Autowired
    private RedisUtil redisUtil;


    /**
     * 全查询分类信息
     *
     * @param pgSorts
     * @param page
     * @param limit
     * @return
     */
    @Override
    public IPage<PgSorts> selectSortsList(PgSorts pgSorts, Integer page, Integer limit) {
        AbstractWrapper<PgSorts, String, QueryWrapper<PgSorts>> wrapper = new QueryWrapper<PgSorts>();

        if (pgSorts != null) {
            if (!StringUtils.isEmpty(pgSorts.getSortName())) {
                wrapper.like("sort_name", pgSorts.getSortName());
            }
        }
        wrapper.eq("sort_state", pgSorts.getSortState());
        //根据id查询最新数据 降序
        wrapper.orderByDesc("sort_id");
        Page<PgSorts> iPage = new Page<PgSorts>(page, limit);
        Page<PgSorts> pgRolePage = pgSortsMapper.selectPage(iPage, wrapper);
        return pgRolePage;
    }

    /**
     * 添加分类信息
     *
     * @param pgSorts
     * @return
     */
    @Override
    public Integer savePgSorts(PgSorts pgSorts) {
        /**
         * 查询分类名称是否存在
         * 如果存在那就返回提示已经存在
         * 如果不存在那就执行添加
         */
        AbstractWrapper abstractWrapper = new QueryWrapper<PgLabels>();
        abstractWrapper.eq("sort_name", pgSorts.getSortName());
        PgSorts pgSortsWrapperGet = pgSortsMapper.selectOne(abstractWrapper);
        logger.info("pgSortsWrapperGet={}", pgSortsWrapperGet);
        if (pgSortsWrapperGet != null) {
            return 101;
        }

        pgSorts.setSortState(0);
        pgSorts.setParentSortId(1L);
        Integer insert = pgSortsMapper.insert(pgSorts);

        if (insert > 0) {
            redisUtil.lSet("sortId", pgSorts.getSortId());
            redisUtil.set("sorts:" + pgSorts.getSortId(), pgSorts);
        }
        return 200;
    }


    /**
     * 修改分类信息
     *
     * @param pgSorts
     * @return
     */
    @Override
    public Integer updatePgSorts(PgSorts pgSorts) {
        /**
         * 查询分类名称是否存在
         * 如果存在那就返回提示已经存在
         * 如果不存在那就修改
         */
        if (pgSorts != null) {
            if (pgSorts.getSortId() != null && !pgSorts.getSortName().isEmpty()) {
                PgSorts pgLabelsByIdInfo = pgSortsMapper.selectById(pgSorts.getSortId());
                AbstractWrapper wrapper = new QueryWrapper();
                wrapper.eq("sort_name", pgSorts.getSortName());
                PgSorts pgLabelsByNameInfo = pgSortsMapper.selectOne(wrapper);

                /**
                 * 如果通过id查询的分类信息与前台传过来的名称一致
                 * 可为修改
                 */
                if (pgLabelsByIdInfo != null) {
                    logger.info("pgLabelsByIdInfo={}", pgLabelsByIdInfo);
                    if (pgLabelsByIdInfo.getSortName().equals(pgSorts.getSortName())) {
                        Integer updateById = pgSortsMapper.updateById(pgSorts);
                        if (updateById > 0) {
                            redisUtil.set("label:" + pgSorts.getSortId(), pgSorts);
                            return 200;
                        }
                    }
                }

                /**
                 * 从前台传过来的分类名称与id查询的名称不一致
                 * 再通过名称查询的数据为空就可以修改
                 */
                if (pgLabelsByNameInfo != null) {
                    logger.info("pgLabelsByNameInfo={}", pgLabelsByNameInfo);
                    if (!pgLabelsByNameInfo.getSortName().equals(pgLabelsByIdInfo.getSortName())) {
                        return 101;
                    }
                }

            }
        }

        /**
         * 如果分类名称不一致就修改成 不存在的分类名称
         */
        Integer updateById = pgSortsMapper.updateById(pgSorts);
        if (updateById > 0) {
            redisUtil.set("sorts:" + pgSorts.getSortId(), pgSorts);
            return 200;
        }
        return 100;
    }

    /**
     * 删除分类
     *
     * @param sortId
     * @return
     */
    @Override
    public Integer deletePgSorts(Long sortId) {

        if (pgArticlesMapper.selectCountArtitleSortBySortId(sortId) > 0) {
            return 101;
        }

        PgSorts pgSorts = new PgSorts();
        pgSorts.setSortId(sortId);
        pgSorts.setSortState(2);
        Integer updateById = pgSortsMapper.updateById(pgSorts);
        return updateById;
    }


}
