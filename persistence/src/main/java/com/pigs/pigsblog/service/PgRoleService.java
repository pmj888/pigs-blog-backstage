package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pigs.pigsblog.entity.PgRoleMenuRef;
import com.pigs.pigsblog.entity.PgUsersRoleRef;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
public interface PgRoleService extends IService<PgRole> {
    /**
     * 查询用户角色
     *
     * @param userId
     * @return
     */
    public PgUsersRoleRef queryUserRoleInfo(Integer userId);

    /**
     * 查询角色并分页
     *
     * @param pgRole
     * @param page
     * @param limit
     * @return
     */
    public IPage<PgRole> queryRoleList(PgRole pgRole, Integer page, Integer limit);

    /**
     * 添加角色
     *
     * @param pgRole
     * @param select
     * @return
     */
    public Integer saveRole(PgRole pgRole, String select);

    /**
     * 条件查询角色信息
     *
     * @param pgRole
     * @return
     */
    public PgRole queryRoleInfo(PgRole pgRole);


    /**
     * 修改角色
     *
     * @param pgRole
     * @param select
     * @return
     */
    public Integer editRole(PgRole pgRole, String select);

    /**
     * 通过角色id查询菜单权限
     *
     * @param roleId
     * @return
     */
    public List<PgMenuVo> queryRoleMenuRefListByRoleId(Long roleId);

}
