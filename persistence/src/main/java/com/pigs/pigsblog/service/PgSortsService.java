package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgSorts;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
public interface PgSortsService extends IService<PgSorts> {

    /**
     * 全查询分类信息
     * @return
     */
    IPage<PgSorts> selectSortsList(PgSorts pgSorts,Integer page, Integer limit);

    /**
     * 添加分类
     * @param pgSorts
     * @return
     */
    Integer savePgSorts(PgSorts pgSorts);

    /**
     * 修改分类
     * @param pgSorts
     * @return
     */
    Integer updatePgSorts(PgSorts pgSorts);

    /**
     * 删除分类
     * @param sortId
     * @return
     */
    Integer deletePgSorts(Long sortId);

}
