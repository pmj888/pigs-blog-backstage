package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import com.pigs.pigsblog.entity.PgOnLine;

public interface PgOnLineService extends IService<PgOnLine> {


    /**
     * 查看所有在线用户
     *
     * @param
     * @return
     */
    IPage<PgOnLine> queryOnlineUser(PgOnLine pgOnLine, Integer page, Integer limit);

    /**
     * 根据SessionId踢出用户
     *
     * @param onLineId
     * @return
     */
    boolean forceLogout(String onLineId);

}
