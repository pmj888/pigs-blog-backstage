package com.pigs.pigsblog.service;

import com.pigs.pigsblog.entity.PgMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author PIGS
 * @since 2020-05-09
 */
public interface PgMenuService extends IService<PgMenu> {
    /**
     * 查询菜单信息并分页
     *
     * @param pgMenu
     * @return
     */
    public List<PgMenu> queryPgMenuList(PgMenu pgMenu);


    /**
     * 级联查询父菜单
     *
     * @return
     */
    List<PgMenuVo> getAllMenus();


    /**
     * 编辑 菜单
     *
     * @param pgMenu
     * @param select
     * @return
     */
    Integer editMenu(PgMenu pgMenu, Long select);

    /**
     * 添加 菜单
     *
     * @param pgMenu
     * @param select
     * @return
     */
    Integer saveMenu(PgMenu pgMenu, Long select);

    /**
     * 删除 菜单 把菜单的状态设置为 2
     * 0 显示
     * 1 隐藏
     * 2 删除
     *
     * @param menuId
     * @return
     */
    Integer delMenu(Long menuId);

    /**
     * 通过菜单id
     * 查询上级菜单
     * 查询菜单权限
     *
     * @param menuId
     * @return
     */
    public PgMenu queryMenuByMenuId(Long menuId);

}
