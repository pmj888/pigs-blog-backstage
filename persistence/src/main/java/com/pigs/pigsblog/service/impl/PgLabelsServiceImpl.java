package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgLabels;
import com.pigs.pigsblog.mapper.PgLabelsMapper;
import com.pigs.pigsblog.service.PgLabelsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Service
public class PgLabelsServiceImpl extends ServiceImpl<PgLabelsMapper, PgLabels> implements PgLabelsService {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(PgLabelsService.class);

    @Autowired
    private PgLabelsMapper pgLabelsMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 查询条件标签并分页
     *
     * @param pgLabels
     * @param page
     * @param limit
     * @return
     */
    @Override
    public IPage<PgLabels> selectPgLabelsList(PgLabels pgLabels, Integer page, Integer limit) {

        AbstractWrapper<PgLabels, String, QueryWrapper<PgLabels>> wrapper = new QueryWrapper<PgLabels>();
        if (pgLabels != null) {
            if (!StringUtils.isEmpty(pgLabels.getLabelName())) {
                wrapper.like("label_name", pgLabels.getLabelName());
            }
        }
        wrapper.eq("label_state", pgLabels.getLabelState());
        //根据id查询最新数据 降序
        wrapper.orderByDesc("label_id");
        Page<PgLabels> iPage = new Page<PgLabels>(page, limit);
        Page<PgLabels> pgRolePage = pgLabelsMapper.selectPage(iPage, wrapper);
        return pgRolePage;
    }

    /**
     * 添加标签
     * redis存进id
     *
     * @param pgLabels
     * @return
     */
    @Override
    public Integer savePgLabels(PgLabels pgLabels) {
        /**
         * 查询标签名称是否存在
         * 如果存在那就返回提示已经存在
         * 如果不存在那就添加
         */
        AbstractWrapper abstractWrapper = new QueryWrapper<PgLabels>();
        abstractWrapper.eq("label_name", pgLabels.getLabelName());
        PgLabels pgLabelsWrapperGet = pgLabelsMapper.selectOne(abstractWrapper);
        logger.info("pgLabelsWrapperGet={}", pgLabelsWrapperGet);
        if (pgLabelsWrapperGet != null) {
            return 101;
        }

        pgLabels.setLabelState(0);
        Integer insert = pgLabelsMapper.insert(pgLabels);

        if (insert > 0) {
            redisUtil.lSet("labelId", pgLabels.getLabelId());
            redisUtil.set("label:" + pgLabels.getLabelId(), pgLabels);
        }
        return 200;
    }

    /**
     * 修改标签
     *
     * @param pgLabels
     * @return
     */
    @Override
    public Integer updatePgLabels(PgLabels pgLabels) {

        /**
         * 查询标签名称是否存在
         * 如果存在那就返回提示已经存在
         * 如果不存在那就修改
         */
        if (pgLabels != null) {
            if (pgLabels.getLabelId() != null && !pgLabels.getLabelName().isEmpty()) {
                PgLabels pgLabelsByIdInfo = pgLabelsMapper.selectById(pgLabels.getLabelId());
                AbstractWrapper wrapper = new QueryWrapper();
                wrapper.eq("label_name", pgLabels.getLabelName());
                PgLabels pgLabelsByNameInfo = pgLabelsMapper.selectOne(wrapper);

                /**
                 * 如果通过id查询的标签信息与前台传过来的名称一致
                 * 可为修改
                 */
                if (pgLabelsByIdInfo != null) {
                    logger.info("pgLabelsByIdInfo={}", pgLabelsByIdInfo);
                    if (pgLabelsByIdInfo.getLabelName().equals(pgLabels.getLabelName())) {
                        Integer updateById = pgLabelsMapper.updateById(pgLabels);
                        if (updateById > 0) {
                            redisUtil.set("label:" + pgLabels.getLabelId(), pgLabels);
                            return 200;
                        }
                    }
                }

                /**
                 * 从前台传过来的标签名称与id查询的名称不一致
                 * 再通过名称查询的数据为空就可以修改
                 */
                if (pgLabelsByNameInfo != null) {
                    logger.info("pgLabelsByNameInfo={}", pgLabelsByNameInfo);
                    if (!pgLabelsByNameInfo.getLabelName().equals(pgLabelsByIdInfo.getLabelName())) {
                        return 101;
                    }
                }

            }
        }

        /**
         * 如果标签名称不一致就修改成 不存在的标签名称
         */
        Integer updateById = pgLabelsMapper.updateById(pgLabels);
        if (updateById > 0) {
            redisUtil.set("label:" + pgLabels.getLabelId(), pgLabels);
            return 200;
        }

        return 100;
    }


}

