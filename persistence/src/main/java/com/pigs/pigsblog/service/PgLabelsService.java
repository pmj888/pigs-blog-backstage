package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgLabels;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 服务类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
public interface PgLabelsService extends IService<PgLabels> {

    /**
     * 全查询标签信息
     *
     * @return
     */
    IPage<PgLabels> selectPgLabelsList(PgLabels pgLabels, Integer page, Integer limit);

    /**
     * 添加标签
     *
     * @param pgLabels
     * @return
     */
    Integer savePgLabels(PgLabels pgLabels);

    /**
     * 修改标签
     *
     * @param pgLabels
     * @return
     */
    Integer updatePgLabels(PgLabels pgLabels);


}
