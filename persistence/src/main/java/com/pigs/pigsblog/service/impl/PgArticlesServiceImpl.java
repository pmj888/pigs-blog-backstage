package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.*;
import com.pigs.pigsblog.mapper.PgArticlesImgMapper;
import com.pigs.pigsblog.mapper.PgArticlesMapper;
import com.pigs.pigsblog.mapper.pgArtitleLabelRefMapper;
import com.pigs.pigsblog.mapper.pgArtitleSortRefMapper;
import com.pigs.pigsblog.service.PgArticlesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.RedisUtil;
import com.pigs.pigsblog.util.TimeUtitl;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Transactional
@Service
public class PgArticlesServiceImpl extends ServiceImpl<PgArticlesMapper, PgArticles> implements PgArticlesService {

    @Autowired
    private PgArticlesImgMapper pgArticlesImgMapper;

    @Autowired
    private PgArticlesMapper pgArticlesMapper;

    @Autowired
    private pgArtitleLabelRefMapper pgArtitleLabelRefMapper;

    @Autowired
    private pgArtitleSortRefMapper pgArtitleSortRefMapper;

    @Autowired
    private RedisUtil redisUtil;

    private Logger logger= LoggerFactory.getLogger(PgArticlesServiceImpl.class);

    /**
     * 查询 全部文章 并分页
     *
     * @param page
     * @return
     */
    @Override
    public IPage<PgArticles> queryPgArticlesList(String articleTitle, Page<PgArticles> page) {
        return pgArticlesMapper.queryPgArticlesList(articleTitle, page);
    }


    /**
     * 发布文章
     *
     * @param maps
     * @return
     */
    @Override
    public Integer insertPgArticles(Map<String, Object> maps) {
        //通过shiro获取当前登录的用户
        PgUsers pgUsers = (PgUsers) SecurityUtils.getSubject().getPrincipal();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        PgArticles pgArticles = new PgArticles();
        pgArticles.setUserId(pgUsers.getUserId());
        pgArticles.setArticleTitle(maps.get("articleTitle").toString());
        pgArticles.setArticleContent(maps.get("test-editor-html-code").toString());
        pgArticles.setArticleViews(0L);
        pgArticles.setArticleCommentCount(0l);
        pgArticles.setArticleDate(TimeUtitl.dateTime());
        pgArticles.setArticleLikeCount(0L);
        pgArticles.setPgEditorFormat(maps.get("articleContent").toString());
        if ("on".equals(maps.get("articleState"))) {
            pgArticles.setArticleState(0);
        } else {
            pgArticles.setArticleState(1);
        }

        Integer insert = pgArticlesMapper.insertPgArticles(pgArticles);
       /* if (insert > 0) {
            redisUtil.lSet("articleId", pgArticles.getArticleId());
            redisUtil.set("article:" + pgArticles.getArticleId(), pgArticles);
        }*/

        PgArticlesImg articlesImg=new PgArticlesImg();
        articlesImg.setArticleId(pgArticles.getArticleId());
        articlesImg.setImgUrl(maps.get("imgUrl").toString());
        Integer saveArticlesImg = pgArticlesImgMapper.insert(articlesImg);
        logger.info("saveArticlesImg={}",saveArticlesImg);

        String pgLabels = (String) maps.get("pgLabels");
        String[] strings = pgLabels.split(",");
        if (strings != null || strings.length != 0) {
            pgArtitleLabelRef pgArtitleLabelRef = new pgArtitleLabelRef();
            for (String labelId : strings) {
                pgArtitleLabelRef.setLabelId(Long.valueOf(labelId).longValue());
                pgArtitleLabelRef.setArticleId(pgArticles.getArticleId());
                int insert1 = pgArtitleLabelRefMapper.insert(pgArtitleLabelRef);

               /* if (insert1 > 0) {
                    redisUtil.set("pgArtitleLabelRef:" + pgArticles.getArticleId() + labelId, pgArtitleLabelRef);
                }
                redisUtil.lSet("pgArtitleLabelRefId", pgArticles.getArticleId() + labelId);*/
            }
        }

        pgArtitleSortRef pgArtitleSortRef = new pgArtitleSortRef(pgArticles.getArticleId(), Long.valueOf(maps.get("pgSorts").toString()).longValue());
        int insert2 = pgArtitleSortRefMapper.insert(pgArtitleSortRef);
      /*  if (insert2 > 0) {
            redisUtil.lSet("pgArtitleSortRefId", pgArticles.getArticleId());
            redisUtil.set("pgArtitleSortRef:" + pgArtitleSortRef.getSortId(), pgArtitleSortRef);
        }*/
        return insert;
    }

    /**
     * 修改文章
     *
     * @param maps
     * @return
     */
    @Override
    public Integer updatePgArticles(Map<String, Object> maps) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        PgArticles pgArticles = new PgArticles();
        pgArticles.setArticleId(Long.parseLong(maps.get("articleId").toString()));
        pgArticles.setArticleTitle(maps.get("articleTitle").toString());
        pgArticles.setArticleContent(maps.get("test-editor-html-code").toString());
        pgArticles.setPgEditorFormat(maps.get("articleContent").toString());
        if ("on".equals(maps.get("articleState"))) {
            pgArticles.setArticleState(0);
        } else {
            pgArticles.setArticleState(1);
        }
        Integer insert = pgArticlesMapper.updateById(pgArticles);
        System.out.println(insert);
        if (insert > 0) {
            redisUtil.set("article:" + pgArticles.getArticleId(), pgArticles);
        }

        String pgLabels = (String) maps.get("pgLabels");
        String[] strings = pgLabels.split(",");
        int i = pgArtitleLabelRefMapper.deleteById(pgArticles.getArticleId());
        if (strings != null || strings.length != 0) {
            pgArtitleLabelRef pgArtitleLabelRef = new pgArtitleLabelRef();
            for (String labelId : strings) {
                pgArtitleLabelRef.setLabelId(Long.valueOf(labelId).longValue());
                pgArtitleLabelRef.setArticleId(pgArticles.getArticleId());
                int insert1 = pgArtitleLabelRefMapper.insert(pgArtitleLabelRef);
                if (insert1 > 0) {
                    redisUtil.set("pgArtitleLabelRef:" + pgArticles.getArticleId() + labelId, pgArtitleLabelRef);
                }
            }
        }


        pgArtitleSortRefMapper.deleteById(pgArticles.getArticleId());
        pgArtitleSortRef pgArtitleSortRef = new pgArtitleSortRef(pgArticles.getArticleId(), Long.valueOf(maps.get("pgSorts").toString()).longValue());
        int insert2 = pgArtitleSortRefMapper.insert(pgArtitleSortRef);
        if (insert2 > 0) {
            redisUtil.set("pgArtitleSortRef:" + pgArtitleSortRef.getSortId(), pgArtitleSortRef);
        }
        return insert;
    }


}
