package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pigs.pigsblog.entity.PgComments;
import com.pigs.pigsblog.model.ResultFormatPaging;
import org.apache.ibatis.annotations.Param;

import java.util.Map;


/**
 * 评论管理表
 *
 * @author manager
 * @email *****@mail.com
 * @date 2020-06-01 11:07:48
 */
public interface PgCommentsService extends IService<PgComments> {
    IPage<PgComments> findListByPage(Page<PgComments> page);
/*  IPage<PgComments> findListByPage(Integer Page,Integer Limit , PgComments pgComments);*/


}

