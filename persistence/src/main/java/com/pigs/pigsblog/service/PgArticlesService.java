package com.pigs.pigsblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgArticles;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 庞明杰
 * @since 2020-04-15
 */
public interface PgArticlesService extends IService<PgArticles> {

    /**
     * 查询 全部文章 并分页
     * @param articleTitle
     * @param page
     * @return
     */
    IPage<PgArticles> queryPgArticlesList(String articleTitle,Page<PgArticles> page);

    /**
     * 发布文章
     * @param maps
     * @return
     */
    Integer insertPgArticles(Map<String,Object> maps);

    /**
     * 修改文章
     * @param maps
     * @return
     */
    Integer updatePgArticles(Map<String,Object> maps);



}
