package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.entity.PgOnLine;
import com.pigs.pigsblog.mapper.PgOnLineMapper;
import com.pigs.pigsblog.service.PgOnLineService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PgOnLineServiceImpl extends ServiceImpl<PgOnLineMapper, PgOnLine> implements PgOnLineService {

    private Logger logger = LoggerFactory.getLogger(PgOnLineServiceImpl.class);

    @Autowired
    private SessionDAO sessionDAO;

    @Autowired
    private PgOnLineMapper pgOnLineMapper;

    /**
     * 查看所有在线用户
     *
     * @param pgOnLine
     * @param page
     * @param limit
     * @return
     */
    @Override
    public IPage<PgOnLine> queryOnlineUser(PgOnLine pgOnLine, Integer page, Integer limit) {
        System.out.println("进去实现层");
        AbstractWrapper wrapper = new QueryWrapper<PgOnLine>();
        if (pgOnLine != null) {
            if (!StringUtils.isEmpty(pgOnLine.getLineLoginName())) {
                wrapper.like("line_login_name", pgOnLine.getLineLoginName());
            }
        }

        /**
         * 根据创建时间来进行排序
         */
        wrapper.orderByDesc("line_create_time");
        Page<PgOnLine> iPage = new Page<PgOnLine>(page, limit);
        Page selectPage = pgOnLineMapper.selectPage(iPage, wrapper);
        return selectPage;
    }

    /**
     * 根据SessionId踢出用户
     *
     * @param sessionId
     * @return
     */
    @Override
    public boolean forceLogout(String sessionId) {
        Session session = sessionDAO.readSession(sessionId);
        sessionDAO.delete(session);
        PgOnLine online = new PgOnLine();
        online.setOnLineId(sessionId);
        online.setLineState(1);
        Integer editOnlineStateById = pgOnLineMapper.updateById(online);
        logger.info("editOnlineStateById={}", editOnlineStateById);
        if (editOnlineStateById > 0) {
            return true;
        }
        return false;
    }
}
