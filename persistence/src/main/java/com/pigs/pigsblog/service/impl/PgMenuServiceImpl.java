package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pigs.pigsblog.entity.PgMenu;
import com.pigs.pigsblog.entity.PgUsers;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import com.pigs.pigsblog.mapper.PgMenuMapper;
import com.pigs.pigsblog.mapper.PgRoleMapper;
import com.pigs.pigsblog.mapper.PgRoleMenuRefMapper;
import com.pigs.pigsblog.service.PgMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.TimeUtitl;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-05-09
 */
@Service
@Transactional
public class PgMenuServiceImpl extends ServiceImpl<PgMenuMapper, PgMenu> implements PgMenuService {

    @Autowired
    private PgMenuMapper pgMenuMapper;

    @Autowired
    private PgRoleMapper pgRoleMapper;

    private Logger logger = LoggerFactory.getLogger(PgMenuServiceImpl.class);

    @Autowired
    private PgRoleMenuRefMapper pgRoleMenuRefMapper;

    /**
     * 条件查询菜单并分页
     *
     * @param pgMenu
     * @return
     */
    @Override
    public List<PgMenu> queryPgMenuList(PgMenu pgMenu) {
        AbstractWrapper<PgMenu, String, QueryWrapper<PgMenu>> wrapper = new QueryWrapper<PgMenu>();
        if (pgMenu != null) {
            if (!StringUtils.isEmpty(pgMenu.getMenuName())) {
                wrapper.like("menu_name", pgMenu.getMenuName());
            }
            if (pgMenu.getMenuId() != null) {
                wrapper.eq("menu_id", pgMenu.getMenuId());
            }
        }

        List<PgMenu> pgMenuList = pgMenuMapper.selectList(wrapper);
        List<PgMenu> collect = pgMenuList.stream().filter(PgMenu -> PgMenu.getState() != 2).collect(Collectors.toList());

        return collect;
    }

    /**
     * 级联查询父菜单
     *
     * @return
     */
    @Override
    public List<PgMenuVo> getAllMenus() {
        List<PgMenuVo> pgMenuMapperAllMenus = pgMenuMapper.getAllMenus();
        return pgMenuMapperAllMenus;
    }

    /**
     * 编辑菜单
     * <p>
     * 查询数据库中的菜单id名称是否一致
     * 如果跟id一致的名称就可以修改
     * 如果不与id不一致名字一致则断定为菜单名称已经存在
     *
     * @param pgMenu
     * @param select
     * @return
     */
    @Override
    public Integer editMenu(PgMenu pgMenu, Long select) {
        /**
         * 从mysql中查询数据如果用户名不存在则修改
         * 如果存在并且不与通过id查询的数据相同就返回101
         * 如果当前的用户名与使用id查询的数据用户名相同则修改
         */
        PgMenu selectById = pgMenuMapper.selectById(pgMenu.getMenuId());
        AbstractWrapper wrapper = new QueryWrapper<PgMenu>();
        wrapper.eq("menu_name", pgMenu.getMenuName());
        PgMenu selectOne = pgMenuMapper.selectOne(wrapper);
        if (selectById != null) {
            if (!selectById.getMenuName().equals(pgMenu.getMenuName())) {
                if (selectOne != null) {
                    return 101;
                }

            }
        }

        Subject subject = SecurityUtils.getSubject();
        PgUsers pgUsersSubject = (PgUsers) subject.getPrincipal();
        pgMenu.setUpdateBy(pgUsersSubject.getUserName());
        pgMenu.setParentId(select);
        int updateById = pgMenuMapper.updateById(pgMenu);
        if (updateById > 0) {
            return 200;
        }
        return 100;
    }

    /**
     * 添加菜单
     *
     * @param pgMenu
     * @return
     */
    @Override
    public Integer saveMenu(PgMenu pgMenu, Long select) {
        if (pgMenu != null) {
            if (!StringUtils.isEmpty(pgMenu.getMenuName())) {
                AbstractWrapper wrapper = new QueryWrapper();
                wrapper.eq("menu_name", pgMenu.getMenuName());
                PgMenu selectOne = pgMenuMapper.selectOne(wrapper);
                logger.info("selectOne={}", selectOne);
                if (selectOne != null) {
                    return 101;
                }

                Subject subject = SecurityUtils.getSubject();
                PgUsers pgUsersSubject = (PgUsers) subject.getPrincipal();
                if (pgUsersSubject != null) {
                    pgMenu.setUpdateBy(pgUsersSubject.getUserName());
                    pgMenu.setCreateBy(pgUsersSubject.getUserName());
                    pgMenu.setCreateTime(TimeUtitl.dateTime());
                    pgMenu.setParentId(select);
                    Integer saveMenuNum = pgMenuMapper.insert(pgMenu);
                    logger.info("saveMenuNum={}", saveMenuNum);
                    return 200;
                }
            }
        }
        return 100;
    }

    /**
     * 删除 菜单 把菜单的状态设置为 2
     * 0 显示
     * 1 隐藏
     * 2 删除
     * 判断 是否是父菜单
     * 主有是子菜单才能设置状态为 2
     *
     * @param menuId
     * @return 101 存在子菜单,不允许删除
     * 102 菜单已分配,不允许删除
     * 100 删除失败
     * 200 删除成功
     */
    @Override
    public Integer delMenu(Long menuId) {
        if (pgMenuMapper.queryCountMenuByParentId(menuId) > 0) {
            return 101;
        }
        if (pgRoleMapper.selectCountRoleMenuByMenuId(menuId) > 0) {
            return 102;
        }
        PgMenu pgMenu = new PgMenu();
        pgMenu.setMenuId(menuId);
        pgMenu.setState(2);
        Integer updateById = pgMenuMapper.updateById(pgMenu);
        pgRoleMenuRefMapper.deleteById(menuId);
        return updateById;
    }

    /**
     * 通过菜单id
     * 查询上级菜单
     * 查询菜单权限
     *
     * @param menuId
     * @return
     */
    @Override
    public PgMenu queryMenuByMenuId(Long menuId) {
        if (menuId != null) {
            PgMenu queryMenuByMenuId = pgMenuMapper.queryMenuByMenuId(menuId);
            logger.info("queryMenuByMenuId={}", queryMenuByMenuId);
            if (queryMenuByMenuId != null) {
                return queryMenuByMenuId;
            }
        }
        return null;
    }


}
