package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgComments;
import com.pigs.pigsblog.mapper.PgCommentsMapper;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgCommentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


@Service("pgCommentsService")
@Transactional
public class PgCommentsServiceImpl extends ServiceImpl<PgCommentsMapper, PgComments> implements PgCommentsService {
   private Logger logger=  LoggerFactory.getLogger(PgCommentsServiceImpl.class);
    @Autowired
    private PgCommentsMapper pgCommentsMapper;

    @Override
    public  IPage<PgComments>  findListByPage(Page page) {
        System.out.println( pgCommentsMapper.findListByPage( page));
       return pgCommentsMapper.findListByPage( page);

    }

  /*  @Override
    public IPage<PgComments> findListByPage( Integer Page ,Integer Limit, PgComments pgComments) {
        if (Page!=null&&Limit!=null) {
            Page<PgComments> Commen = new Page<>(Page, Limit);
           *//* IPage<PgComments> pg = pgCommentsMapper.findListByPage(Commen, pgComments);*//*
            logger.info("pg={}", pg);
            return pg;
        }
        return null;
    }*/








   /* @Override
    public IPage<Map<String, Object>> findListByPage(Page<PgComments> page) {
        return pgCommentsMapper.findListByPage(page);
    }*/
}