package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgRole;
import com.pigs.pigsblog.entity.PgRoleMenuRef;
import com.pigs.pigsblog.entity.PgUsersRoleRef;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import com.pigs.pigsblog.mapper.PgRoleMapper;
import com.pigs.pigsblog.mapper.PgRoleMenuRefMapper;
import com.pigs.pigsblog.mapper.PgUsersRoleRefMapper;
import com.pigs.pigsblog.service.PgRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.util.RedisUtil;
import com.pigs.pigsblog.util.TimeUtitl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Service
@Transactional
public class PgRoleServiceImpl extends ServiceImpl<PgRoleMapper, PgRole> implements PgRoleService {

    private Logger logger = LoggerFactory.getLogger(PgRoleServiceImpl.class);

    @Autowired
    private PgUsersRoleRefMapper pgUsersRoleRefMapper;

    @Autowired
    private PgRoleMenuRefMapper pgRoleMenuRefMapper;

    @Autowired
    private PgRoleMapper pgRoleMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 查询用户 角色
     *
     * @param userId
     * @return
     */
    @Override
    public PgUsersRoleRef queryUserRoleInfo(Integer userId) {

        /**
         * redis中数据不存在就进入mysql中查询
         */
        if (userId != null) {
            AbstractWrapper wrapper = new QueryWrapper();
            wrapper.eq("users_id", userId);
            PgUsersRoleRef selectById = pgUsersRoleRefMapper.selectOne(wrapper);
            if (selectById != null) {
                redisUtil.set("pgUsersRoleRef:" + selectById.getUsersId(), selectById);
                logger.info("selectById={}", selectById);
                return selectById;
            }
        }

        return null;
    }

    /**
     * 条件查询角色信息 并分页
     *
     * @param pgRole
     * @param page
     * @param limit
     * @return
     */
    @Override
    public IPage<PgRole> queryRoleList(PgRole pgRole, Integer page, Integer limit) {
        AbstractWrapper<PgRole, String, QueryWrapper<PgRole>> wrapper = new QueryWrapper<PgRole>();

        if (pgRole != null) {
            if (!StringUtils.isEmpty(pgRole.getRoleName())) {
                wrapper.like("role_name", pgRole.getRoleName());
            }
        }

        wrapper.eq("role_state", pgRole.getRoleState());
        wrapper.orderByDesc("role_create_time");
        Page<PgRole> iPage = new Page<PgRole>(page, limit);
        Page<PgRole> pgRolePage = pgRoleMapper.selectPage(iPage, wrapper);
        return pgRolePage;
    }

    /**
     * 添加角色实现类
     *
     * @param pgRole
     * @return
     */
    @Override
    public Integer saveRole(PgRole pgRole, String select) {

        AbstractWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("role_name", pgRole.getRoleName());
        PgRole selectOne = pgRoleMapper.selectOne(wrapper);

        if (selectOne != null) {
            if (selectOne.getRoleName().equals(pgRole.getRoleName())) {
                return 101;
            }
        }

        AbstractWrapper wrapperDescription = new QueryWrapper<>();
        wrapperDescription.eq("role_description", pgRole.getRoleDescription());
        PgRole selectOneDescription = pgRoleMapper.selectOne(wrapperDescription);
        if (selectOneDescription != null) {
            if (selectOneDescription.getRoleDescription().equals(pgRole.getRoleDescription())) {
                return 102;
            }
        }

        pgRole.setRoleCreateTime(TimeUtitl.dateTime());
        Integer insertRole = pgRoleMapper.insert(pgRole);

        if (!StringUtils.isEmpty(select)) {
            String[] menuId;
            menuId = select.split(",");
            PgRoleMenuRef pgRoleMenuRef = new PgRoleMenuRef();
            for (String x : menuId) {
                pgRoleMenuRef.setMenuId(Long.valueOf(x));
                pgRoleMenuRef.setRoleId(Long.valueOf(pgRole.getRoleId()));
                pgRoleMenuRefMapper.insert(pgRoleMenuRef);
            }
        }

        if (insertRole > 0) {
            return 1;
        }
        return null;
    }

    /**
     * 条件查询角色信息
     *
     * @param pgRole
     * @return
     */
    @Override
    public PgRole queryRoleInfo(PgRole pgRole) {
        if (pgRole != null) {
            if (pgRole.getRoleId() != null) {
                AbstractWrapper wrapper = new QueryWrapper<>();
                wrapper.eq("role_id", pgRole.getRoleId());
                PgRole selectOne = pgRoleMapper.selectOne(wrapper);
                logger.info("queryRoleInfo={}", selectOne);
                if (selectOne != null) {
                    return selectOne;
                }
            }
        }
        return null;
    }

    /**
     * 修改角色信息
     * 返回 101说明角色名称已经存在
     * 返回 102说明角色标识已经存在
     *
     * @param pgRole
     * @return
     */
    @Override
    public Integer editRole(PgRole pgRole, String select) {

        PgRole selectById = pgRoleMapper.selectById(pgRole.getRoleId());
        AbstractWrapper wrapper = new QueryWrapper<PgRole>();
        wrapper.eq("role_name", pgRole.getRoleName());
        PgRole selectOne = pgRoleMapper.selectOne(wrapper);
        if (selectById != null) {
            if (selectOne != null) {
                if (!selectById.getRoleName().equals(pgRole.getRoleName())) {
                    return 101;
                }
            }
        }

        AbstractWrapper wrapperDescription = new QueryWrapper<>();
        wrapperDescription.eq("role_description", pgRole.getRoleDescription());
        PgRole selectOneDescription = pgRoleMapper.selectOne(wrapperDescription);
        if (selectById != null) {
            if (selectOneDescription != null) {
                if (!selectById.getRoleDescription().equals(pgRole.getRoleDescription())) {
                    return 102;
                }
            }
        }

        /**
         * 删除角色菜单权限
         * 然后保存新的菜单权限
         */
        Integer deleteRoleMenuByRoleId = pgRoleMenuRefMapper.deleteRoleMenuByRoleId(Long.valueOf(pgRole.getRoleId()));
        logger.info("deleteRoleMenuByRoleId={}", deleteRoleMenuByRoleId);

        if (!StringUtils.isEmpty(select)) {
            String[] menuId;
            menuId = select.split(",");
            PgRoleMenuRef pgRoleMenuRef = new PgRoleMenuRef();
            for (String x : menuId) {
                pgRoleMenuRef.setMenuId(Long.valueOf(x));
                pgRoleMenuRef.setRoleId(Long.valueOf(pgRole.getRoleId()));
                pgRoleMenuRefMapper.insert(pgRoleMenuRef);
            }
        }

        Integer updateByIdRole = pgRoleMapper.updateById(pgRole);
        logger.info("updateByIdRole={}", updateByIdRole);
        return updateByIdRole;
    }

    /**
     * 通过角色id查询菜单权限
     *
     * @param roleId
     * @return
     */
    @Override
    public List<PgMenuVo> queryRoleMenuRefListByRoleId(Long roleId) {
        if (roleId != null) {
            List<PgMenuVo> pgRoleMenuRefList = pgRoleMenuRefMapper.queryRoleMenuRefListByRoleId(roleId);
            return pgRoleMenuRefList;
        }
        return null;
    }


}
