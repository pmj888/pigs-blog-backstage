package com.pigs.pigsblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pigs.pigsblog.entity.pgArtitleLabelRef;
import com.pigs.pigsblog.mapper.pgArtitleLabelRefMapper;
import com.pigs.pigsblog.service.pgArtitleLabelRefService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 庞明杰
 * @since 2020-04-21
 */
@Transactional
@Service
public class pgArtitleLabelRefServiceImpl extends ServiceImpl<pgArtitleLabelRefMapper, pgArtitleLabelRef> implements pgArtitleLabelRefService {


}
