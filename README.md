# pigs-admin

 
### git提交方式

feat:新功能（feature）

fix:修补bug

docs:文档（documentation）

style:格式（不影响代码运行的变动）

refactor:重构（即不是新增功能，也不是修改bug的代码变动）

test:增加测试

chore:构建过程或辅助工具的变动

#### 介绍
博客后端系统

 **springboot多模块** 

        模型 model

        持久层 persistence

        表示层 web

        工具层 util

        web 依赖于persistence

        persistence 依赖于 model
        
        model 依赖于 util

        web->persistence->model->util
   
![输入图片说明](https://images.gitee.com/uploads/images/2020/0414/204816_29c5aab5_5335816.png "屏幕截图.png")

redis存值与取值
`

     /**
      * 添加用户
      * redis存进id
      * @param userEntity
      * @return
      */
     @Override
     public Integer saveUser(UserEntity userEntity) {
         Integer insert = userMapper.insert(userEntity);
         if (insert > 0) {
             redisUtil.sSet("userId", userEntity.getId());
             redisUtil.set("user:" + userEntity.getId(), userEntity);
         }
         return insert;
     }
 
     /**
      * 修改用户
      * 通过用户id获取修改用户信息
      * @param userEntity
      * @return
      */
     @Override
     public Integer updateUser(UserEntity userEntity) {
         Integer updateById = userMapper.updateById(userEntity);
         if (updateById > 0) {
             redisUtil.set("user:" + userEntity.getId(), userEntity);
         }
         return updateById;
     }
 
     /**
      * 查询用户全部信息
      * 查询用户信息如果不存在那就去mysql中获取
      * @return
      */
     @Override
     public List<UserEntity> queryUser() {
         Set<Object> userId = redisUtil.sGet("userId");
         Set<Object> objects = redisUtil.sGet("user:" + userId);
 
 
         List<UserEntity> userEntityList = userMapper.selectList(null);
         if (userEntityList.size() > 0) {
             System.out.println("MySQL中的数据=" + userEntityList);
 
             for (UserEntity userEntity : userEntityList) {
                 redisUtil.set("user:" + userEntity.getId(), userEntity);
             }
         }
 
         return userEntityList;
     }
 
     /**
      * 通过id删除用户也把redis的id删除
      * 
      * @param userId
      * @return
      */
     @Override
     public Integer delUser(Integer userId) {
         Integer deleteById = userMapper.deleteById(userId);
         if (deleteById > 0) {
             Long delete = redisUtil.delete("user:" + String.valueOf(userId));
         }
         return deleteById;
     }
 
 }
`

###  **项目概述及分析** 


 **1.1 项目背景** 

一直以来都想开发属于自己的网站，随着时间的推移，渐渐的懂的东西也多了，这次选的开发项目是个人博客系统，前端模板来源于网上，在此感谢开源项目项目仅仅使用学习.....禁止商业（贩卖等）...如果侵权请联系，本人处于学习中.....

 ** **1.2 分析** ** 

 **系统主要完成以下几方面的功能：** 

1、用户管理：用户的注册和登录，发表博文和评论。

2、博文管理：用户可以在网站中发表和设置博文。

3、评论管理：用户可以评论博文和回复其他用户的评论。

4、分类管理：添加和删除分类，给文章设置分类。

5、标签管理：添加和删除标签，给文章设置标签。

6、权限管理: 添加用户权限，CURD

7、角色管理: 添加用户角色，CRUD

 **1.3 系统功能** 

 **1.3.1 用户管理** 

用户的相关信息如下：用户ID、用户IP、用户名、用户昵称、用户密码、用户邮箱、用户头像、注册时间、用户生日、用户年龄、用户手机号。

用户注册时需提供用户名、用户密码、用户邮箱或用户手机号。

用户登录时可以采用用户名或邮箱或手机号进行登录。

用户可以发布博文、发表评论、回复，还可以添加其他用户为好友。

 **1.3.2 博文管理

博文的相关信息如下：博文ID、发布日期、发表用户、博文标题、博文内容、点赞数、回复数、游览量。

博文可以被普通用户发布、修改、删除和评论，但修改和删除仅限于自己发表的动态。

博文发布时需要设置分类、标签。

 **1.3.3 评论管理** 

评论的相关信息如下：评论ID、评论日期、点赞数、发表用户、评论文章ID、评论内容、父评论ID。

评论可以被用户发表和删除以及被其他用户回复。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0517/210314_436704c1_5530449.png "屏幕截图.png")
 **1.3.4 分类管理** 

分类的相关信息如下：分类ID、分类名称、分类别名、分类描述、父分类ID。

只有管理员可以添加、删除、修改分类。

分类的作用不仅可以将文章分类，还可以作为博客的菜单。

 **1.3.5 标签管理** 

标签的相关信息如下：标签ID、标签名称、标签别名、标签描述。

用户发表文章时可以设置标签，标签不仅可以将文章分类，还可以作为博客的菜单。

 **1.3.6 权限管理**

对应没有权限的用户是无法进行操作的

**1.3.7 角色管理**
    
角色

  
### 完成进度效果图
![登录](https://images.gitee.com/uploads/images/2020/0516/024233_a60d7691_5335816.png "1.png")
![首页](https://images.gitee.com/uploads/images/2020/0516/024247_4ad71b71_5335816.png "2.png")
![用户列表](https://images.gitee.com/uploads/images/2020/0516/024256_2a0500b9_5335816.png "3.png")
![修改用户](https://images.gitee.com/uploads/images/2020/0516/024307_50cd4aed_5335816.png "4.png")
![搜索用户](https://images.gitee.com/uploads/images/2020/0516/024340_ab45eef0_5335816.png "5.png")
![角色管理](https://images.gitee.com/uploads/images/2020/0516/024409_23f80789_5335816.png "6.png")
![菜单列表](https://images.gitee.com/uploads/images/2020/0516/024424_3d59ccc3_5335816.png "7.png")
![修改用户头像信息](https://images.gitee.com/uploads/images/2020/0516/024445_b51bc3f4_5335816.png "8.png")



