package com.pigs.pigsblog.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgLabels;
import com.pigs.pigsblog.entity.PgSorts;
import com.pigs.pigsblog.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 标签 测试类
 *
 * @author 安详的苦丁茶
 * @version 1.0
 * @date 2020/4/17 21:53
 */

@SpringBootTest
public class PgLabelsServiceTest {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(PgLabelsServiceTest.class);

    @Autowired
    private PgLabelsService pgLabelsService;

    @Autowired
    private RedisUtil redisUtil;


    /**
     * 全查询标签信息
     */
    @Test
    public void selectPgLabelsList() {
        List<PgLabels> list = pgLabelsService.list();
        for (PgLabels pgLabels : list) {
            System.out.println(pgLabels);
        }
    }


    /**
     * 添加标签信息
     */
    @Test
    public void savePgLabels() {
        PgLabels pgLabels = new PgLabels();

        pgLabels.setLabelName("bb");
        pgLabels.setLabelAlias("奥利给");
        pgLabels.setLabelDescription("么么哒");
        pgLabels.setLabelState(0);

        boolean flag = pgLabelsService.save(pgLabels);

        List<PgLabels> list = pgLabelsService.list();
        for (PgLabels labels : list) {
            System.out.println(labels);
        }

        if (flag) {
            logger.info("添加成功");
        } else {
            logger.info("添加失败");
        }
    }


    /**
     * 修改标签信息
     */
    @Test
    public void updatePgLabels() {
        PgLabels pgLabels = new PgLabels();

        pgLabels.setLabelId((long) 2);
        pgLabels.setLabelName("gg");
        pgLabels.setLabelAlias("奥利给");
        pgLabels.setLabelDescription("么么哒");
        pgLabels.setLabelState(0);

        boolean flag = pgLabelsService.updateById(pgLabels);

        List<PgLabels> list = pgLabelsService.list();
        for (PgLabels labels : list) {
            System.out.println(labels);
        }

        if (flag) {
            logger.info("修改成功");
        } else {
            logger.info("修改失败");
        }
    }


    /**
     * 修改标签状态
     */
    @Test
    public void deletePgLabelsId() {
        PgLabels pgLabels = new PgLabels();

        pgLabels.setLabelId((long) 8);
        pgLabels.setLabelState(1);
        boolean flag = pgLabelsService.updateById(pgLabels);

        List<PgLabels> list = pgLabelsService.list();
        for (PgLabels labels : list) {
            System.out.println(labels);
        }

        if (flag) {
            logger.info("删除成功");
        } else {
            logger.info("删除失败");
        }
    }




    /**
     * 从reid中获取标签信息
     */
    @Test
    void getRedisLabelData() {
        /**
         * 查询标签的全部id
         * 然后循环查询出标签全部信息
         */
        List<Object> labelId = redisUtil.lGet("labelId", 0, -1);
        logger.info("labelId={}", labelId);

        for (Object o : labelId) {
            PgLabels pgLabels = (PgLabels) redisUtil.get("label:" + o);
            logger.info("pgLabels={}", pgLabels);
        }

        logger.info("--------- 分割线 ={}", "------");
        /**
         * 通过指定的id查询标签信息
         */
        PgLabels pgLabels = (PgLabels) redisUtil.get("label:" + 1);
        logger.info("pgLabels={}", pgLabels);
    }


}