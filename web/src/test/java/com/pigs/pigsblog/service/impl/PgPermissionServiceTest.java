package com.pigs.pigsblog.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgMenu;
import com.pigs.pigsblog.service.PgMenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author PIGS
 * @version 1.0
 * @date 2020/5/9 8:51
 * @effect : 权限测试类
 */
@SpringBootTest
public class PgPermissionServiceTest {

    @Autowired
    private PgMenuService pgMenuService;

    @Test
    public void queryPermissionList() {
        PgMenu pgMenu = new PgMenu();
        pgMenu.setState(0);
        List<PgMenu> pgMenuList = pgMenuService.queryPgMenuList(pgMenu);

    }
}
