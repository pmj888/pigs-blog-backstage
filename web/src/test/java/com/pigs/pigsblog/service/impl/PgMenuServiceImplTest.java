package com.pigs.pigsblog.service.impl;

import com.pigs.pigsblog.entity.vo.PgMenuVo;
import com.pigs.pigsblog.service.PgMenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author PIGS
 * @version 0.0.1
 * @date 2020/5/19 11:25
 **/
@SpringBootTest
public class PgMenuServiceImplTest {

    @Autowired
    private PgMenuService pgMenuService;

    @Test
    public void queryForMenu() {


        System.out.println("------------------------------------------------------------------");
        List<PgMenuVo> allMenus = pgMenuService.getAllMenus();
        for (PgMenuVo allMenu : allMenus) {
            System.out.println(allMenu);
        }
    }
}