package com.pigs.pigsblog.service;

import com.pigs.pigsblog.entity.PgSorts;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


/**
 * 分类测试类
 *
 * @author 安详的苦丁茶
 * @version 1.0
 * @date 2020/4/17 20:51
 */

@SpringBootTest
public class PgSortsServiceTest {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(PgSortsServiceTest.class);

    @Autowired
    private PgSortsService pgSortsService;

    /**
     * 全查询分类信息
     */
    @Test
    public void selecTSortsAll() {
        List<PgSorts> list = pgSortsService.list();
        for (PgSorts pgSorts : list) {
            System.out.println(pgSorts);
        }
    }


    /**
     * 添加分类信息
     */
    @Test
    public void insertSorts(){
        PgSorts pgSorts=new PgSorts();
        pgSorts.setSortName("ff");
        pgSorts.setSortAlias("奥利给");
        pgSorts.setSortDescription("么么哒");
        pgSorts.setParentSortId((long) 2);

        boolean flag=pgSortsService.save(pgSorts);
        if (flag){
            logger.info("添加成功");
        }else {
            logger.info("添加失败");
        }
    }


    /**
     * 修改分类信息
     */
    @Test
    public void updateSorts(){
        PgSorts pgSorts=new PgSorts();
        pgSorts.setSortId((long) 3);
        pgSorts.setSortName("高级分类");
        pgSorts.setSortAlias("奥利给");
        pgSorts.setSortDescription("呆瓜");
        pgSorts.setParentSortId((long) 2);

        boolean flag=pgSortsService.updateById(pgSorts);
        if (flag){
            logger.info("修改成功");
        }else {
            logger.info("修改失败");
        }
    }

    @Test
    public void deleteSorts(){

        boolean flag=pgSortsService.removeById(2);
        if (flag){
            logger.info("删除成功");
        }else {
            logger.info("删除失败");
        }
    }

}