package com.pigs.pigsblog.system;


import com.pigs.pigsblog.system.server.Cpu;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author PIGS
 * @version 0.0.1
 * @date 2020/5/28 22:07
 **/
@SpringBootTest
public class SystemServerTest {

    @Test
    public void copyTo() throws Exception {
        SystemServer systemServer=new SystemServer();
        systemServer.copyTo();
        System.out.println(systemServer.getCpu().getCpuNum());

    }
}