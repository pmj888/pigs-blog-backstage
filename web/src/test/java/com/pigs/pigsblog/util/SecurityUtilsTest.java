package com.pigs.pigsblog.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;



/**
 * @author PIGS
 * @version 1.0
 * @date 2020/3/25 11:41
 * @effect :
 */
@SpringBootTest
public class SecurityUtilsTest {

    @Test
    public void encryptPassword() {
        System.out.println(SecurityUtils.encryptPassword("123456","25abe3ed66de96cee8d53936a38f8ac561e5637522948599"));
    }

    @Test
    public void randomSalt() {
        System.out.println(SecurityUtils.randomSalt());
    }
}