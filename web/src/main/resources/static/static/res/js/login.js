layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    var myDate = new Date();
    $(".year").html(myDate.getFullYear());

    /**
     * 监听提交
     */

    $('#changeUser').click(function () {

        if ($("input[name='userName']").val() == '') {
            $.message({
                message: "请输入账号",
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($("input[name='userPassword']").val() == '') {
            $.message({
                message: "请输入密码",
                type: 'warning',
                showClose: true
            });
            return false;
        }

        /**
         * 又换了种风格，并且设定最长等待10秒
         */
        /*var index = layer.load(2, {time: 5 * 5000});*/
        var index = top.layer.msg('正在验证登录，请销后...', {icon: 16, time: 8000, shade: 0.8});
        $.ajax({
            url: "/pgUsers/goLogin",
            type: 'post',
            datatype: "json",
            data: {
                userName: $("input[name='userName']").val(),
                userPassword: $("input[name='userPassword']").val(),
            },
            success: function (data) {
                top.layer.close(index);

                if (data.code == 200) {
                    window.location.href = "/index.html";
                } else {
                    $.message({
                        message: data.msg,
                        type: 'warning',
                        showClose: true
                    });
                }
            },
            error: function () {
                $.message({
                    message: "稍等再尝试..",
                    type: 'error',
                    showClose: true
                });
            }, complete: function () {
                layer.close(index);
            }
        });

    })

    $('#forgetPassword').click(function () {
        $.message({
            message: '请联系管理员',
            type: 'warning',
            showClose: true
        });

    })

});
