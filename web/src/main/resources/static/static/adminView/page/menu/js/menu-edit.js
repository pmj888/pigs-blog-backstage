var $;
layui.config({
    base: '/static/res/iconpicker/module/'
}).use(['form', 'layer', 'jquery', 'iconPicker'], function () {
    var form = layui.form,
        iconPicker = layui.iconPicker,
        eleTree = layui.eleTree,
        layer = parent.layer === undefined ? layui.layer : parent.layer;
    $ = layui.jquery;
    var layerIndex = layer.load(2);

    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }

    var menuList = xmSelect.render({
        el: '#menuList',
        model: {label: {type: 'text'}},
        radio: true,
        clickClose: true,
        filterable: true,
        theme: {
            color: '#33cabb',
        },
        tree: {
            show: true,
            strict: false,
            expandedKeys: [-1],
        },
        height: 'auto',
        data: []
    })

    /**
     * 动态加载 菜单 tree
     */
    $.ajax({
        type: "get",
        url: "/pgMenu/queryMenuTree",
        datatype: "json",
        success: function (data) {
            var res = data.data;
            menuList.update({
                data: res,
                autoRow: true,
            })
        }, error: function () {
            $.message({
                message: 'boom..',
                type: 'error',
                showClose: true
            });
        }, complete: function (XHR, TS) {
            /**
             * 查询菜单信息 并回显
             */
            $.ajax({
                type: "GET",
                url: "/pgMenu/queryMenuByMenuId",
                datatype: "json",
                data: {
                    menuId: $.getUrlParam("menuId")
                },
                success: function (data) {
                    // console.log(data)
                    if (data.code == 200) {
                        $('.menuId').val(data.data.menuId);
                        $('.url').val(data.data.url);
                        $('.menuName').val(data.data.menuName);
                        $('.remark').val(data.data.remark);
                        $('#iconPicker').val(data.data.icon);
                        $('.orderNum').val(data.data.orderNum);
                        $('.perms').val(data.data.perms);
                        $("input[type=radio][name='menuType'][value='" + data.data.menuType + "']").attr("checked", 'checked');
                        $("#target").each(function () {
                            $(this).children("option").each(function () {
                                if (this.value == data.data.target) {
                                    $(this).attr("selected", "selected");
                                }
                            });
                        })
                        $(".state").each(function () {
                            $(this).children("option").each(function () {
                                if (this.value == data.data.state) {
                                    $(this).attr("selected", "selected");
                                }
                            });
                        })

                        menuList.update([{name: data.data.menuName, value: data.data.menuId, disabled: 'disabled'}])

                        if (data.data.parentName != null) {
                            menuList.setValue([{name: data.data.parentName, value: data.data.parentId, selected: true}])
                        } else {
                            xmSelect.batch('#menuList', 'update', {
                                disabled: true,
                            });
                            menuList.setValue([{name: "无", value: 0, selected: true}])
                        }

                        form.render();
                    }

                }, error: function () {
                    $.message({
                        message: 'boom..',
                        type: 'error',
                        showClose: true
                    });
                }, complete: function (XHR, TS) {

                }
            });
            layer.close(layerIndex);
        }
    });


    /**
     * 图标选择
     */
    iconPicker.render({
        elem: '#iconPicker',
        type: 'fontClass',
        search: true,
        page: true,
        limit: 12,
        click: function (data) {
            //console.log(data);
        },
        success: function (d) {
            // console.log(d);

        }
    });


    form.on("submit(menuEdit)", function (data) {

        if ($('.menuName').val() == '') {
            $.message({
                message: '菜单名不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.orderNum').val() == '') {
            $.message({
                message: '排序不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        } else {
            var patrn = /^[0-9]*$/;
            if (!patrn.test($('.orderNum').val())) {
                $.message({
                    message: '排序只能输入数字',
                    type: 'warning',
                    showClose: true
                });
                return false;
            }
        }

        var layerIndex = layer.load(2);
        console.log(data.field)
        /**
         * 弹出loading
         * 然后刷新父页面
         */
        $.ajax({
            type: "POST",
            url: "/pgMenu/editMenu",
            datatype: "json",
            data: data.field,
            success: function (data) {
                //  console.log(data)
                if (data.code == 200) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });
                    setTimeout(function () {
                        layer.closeAll("iframe");
                        parent.location.reload();
                    }, 2000);
                } else if (data.code == 101) {
                    $.message({
                        message: "菜单名已存在",
                        type: 'warning',
                        showClose: true
                    });
                } else {
                    $.message({
                        message: "操作失败",
                        type: 'warning',
                        showClose: true
                    });
                }

            }, error: function () {
                $.message({
                    message: '系统匆忙，请销后再试 ！',
                    type: 'error',
                    showClose: true
                });
            }, complete: function (XHR, TS) {
                layer.close(layerIndex);
            }
        });

        return false;
    })


})


