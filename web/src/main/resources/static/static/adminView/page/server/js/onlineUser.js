layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    var layerIndex = layer.load(2, {time: 5 * 1000});

    /***
     *
     * 显示在线用户
     *
     */
    table.render({
        skin: 'line',
        elem: '#onlineList'
        , loading: false
        , url: '/tiOnline/queryOnlineUser'
        , cols: [
            [
                {type: 'checkbox', fixed: 'left'}
                , {field: 'onLineId', title: '会话ID', width: 330}
                , {
                field: 'lineLoginName', title: '登录名称'
            }
                , {
                field: 'lineIpAddress', title: '主机'
            }
                , {
                field: 'lineBrowserType', title: '浏览器'
            }, {
                field: 'lineOs', title: '操作系统'
            }
                , {
                field: 'lineState', title: '会话状态', align: "center",
                templet: function (d) {
                    if (d.lineState === 1) {
                        return '<span class="layui-btn layui-btn-xs" style="border-radius: 20px">离线</span>';
                    }
                    return '<span class="layui-btn layui-btn-primary layui-btn-xs" style="border-radius: 20px">在线</span>';
                }
            }
                , {
                field: 'lineCreateTime', title: '登录时间'
            }, {
                field: 'lineLastTime', title: '最后一次访问时间'
            }
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 140}
            ]
        ]
        , limit: 20
        , limits: [20, 25, 50, 100]
        , parseData: function (data) {
            /**
             * 打印数据
             *console.log(data)
             */
            layer.close(layerIndex);
        }
        , page: true
        , id: 'onlineListReload'
    });

    //监听工具条
    table.on('tool(online)', function (obj) {
        var data = obj.data;

        var layerIndex = layer.load(2, {time: 5 * 1000});
        if (obj.event === 'forceLogout') {
            layer.confirm('确定注销选中用户吗？', {title: '系统提示'}, function (index) {
                    var layerIndex = layer.load(2, {time: 5 * 1000});
                    $.ajax({
                        url: '/tiOnline/forceLogout',
                        data: {
                            onLineId: data.onLineId
                        },
                        type: 'put',
                        success: function (data) {
                            // console.log(data)
                            if (data.code == 200) {
                                $.message({
                                    message: data.msg,
                                    type: 'success',
                                    showClose: true
                                });
                            } else {
                                $.message({
                                    message: data.msg,
                                    type: 'warning',
                                    showClose: true
                                });
                            }
                        }, error: function () {
                            $.message({
                                message: 'boom..',
                                type: 'error',
                                showClose: true
                            });
                        }, complete: function (XHR, TS) {
                            layer.close(index);
                            layer.close(layerIndex);
                        }
                    })
                }, function () {
                    layer.close(layerIndex);
                }
            );
        }
    });


    var $ = layui.$, active = {
        /**
         * 批量删除链接
         *
         * */
        closeBtn: function () {
            var $checkbox = $('table input[type="checkbox"][name="layTableCheckbox"]');
            var $checked = $('table input[type="checkbox"][name="layTableCheckbox"]:checked');
            if ($checkbox.is(":checked")) {
                var checkStatusId = table.checkStatus('onlineListReload'),
                    data = checkStatusId.data,
                    onLineId = [];

                for (var i in data) {
                    onLineId.push(data[i].onLineId)
                }

                layer.confirm('确定要注销' + data.length + '个用户吗?', {title: '系统信息'}, function (index) {
                    var layerIndex = layer.load(2, {time: 5 * 1000});
                    $.ajax({
                        url: '/tiOnline/forceLogouts',
                        data: {
                            onLineId: onLineId
                        },
                        type: 'PUT',
                        success: function (res) {

                            if (res.code == 200) {
                                $.message({
                                    message: res.msg,
                                    type: 'success',
                                    showClose: true
                                });
                                table.reload('onlineListReload', {});
                            } else {
                                $.message({
                                    message: res.msg,
                                    type: 'warning',
                                    showClose: true
                                });
                            }

                        }, error: function (data) {
                            $.message({
                                message: '系统异常..',
                                type: 'error',
                                showClose: true
                            });
                        }, complete: function () {
                            layer.close(index);
                            layer.close(layerIndex);
                        }
                    })


                })
            } else {
                $.message({
                    message: '请选中要注销的用户',
                    type: 'warning',
                    showClose: true
                });
            }
        },
        reload: function () {
            var onlineReload = $('#onlineReload');

            /**
             * 搜索内容
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('onlineListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    lineLoginName: onlineReload.val()
                }
            }, 'data');

        }
    };

    /**
     * 搜索框架 搜索内容
     * @type {*|pe}
     */
    $('.searchBtn').on('click', function () {
        var layerIndex = layer.load(2, {time: 5 * 1000});
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });

    /**
     * 批量强退用户
     */
    $('.forceLogouts').on('click', function () {
        var layerIndex = layer.load(2, {time: 5 * 1000});
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });
})
