layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    var layerIndex = layer.load(2, {time: 5 * 1000});

    /***
     *
     * 显示友鏈
     *
     */
    table.render({
        skin: 'line',
        elem: '#systemLogList'
        , loading: false
        , url: '/pgSystemLog/querySystemLog'
        , cols: [
            [
                {type: 'checkbox', fixed: 'left'}
                , {field: 'logUserName', title: '登录名称'}
                , {
                field: 'logIpAddr', title: '登录地址'
            }
                , {
                field: 'logMethod', title: '请求方式'
            }
                , {
                field: 'logUri', title: 'URI'
            }
                , {
                field: 'logState', title: '会话状态', align: "center",
                templet: function (d) {
                    if (d.logState === 1) {
                        return '<span class="layui-btn layui-btn-xs" style="border-radius: 20px">失败</span>';
                    }
                    return '<span class="layui-btn layui-btn-primary layui-btn-xs" style="border-radius: 20px">成功</span>';
                }
            }
                , {
                field: 'logSpendTime', title: '耗时/S'
            }, {
                field: 'logStartTime', title: '访问时间'
            }
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 160}
            ]
        ]
        , limit: 20
        , limits: [20, 25, 50, 100]
        , parseData: function (data) {
            /**
             * 打印数据
             */
            console.log(data)
            layer.close(layerIndex);
        }
        , page: true
        , id: 'onlineListReload'
        , where: {logState: 0}

    });


    /**
     * 搜索框架 搜索内容
     */
    var $ = layui.$, active = {
        reload: function () {
            var demoReload = $('#demoReload');
            /**
             * 搜索内容
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('onlineListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    logUserName: demoReload.val(),
                    logState: 0
                }
            }, 'data');

        }
    };

    /**
     * 搜索框架 搜索内容
     * @type {*|pe}
     */
    $('.demoTable .layui-btn').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });

    //监听工具条
    table.on('tool(systemLog)', function (obj) {
        var data = obj.data;
        //  console.log(data)
        var layerIndex = layer.load(2, {time: 5 * 1000});
        if (obj.event === 'delSystemLog') {
            layer.confirm('确定要删除该日志信息吗？', {title: '系统提示'}, function (index) {
                    var layerIndex = layer.load(2, {time: 5 * 1000});
                    $.ajax({
                        url: '/tiSystemLog/delete',
                        data: {
                            logId: data.logId
                        },
                        type: 'put',
                        success: function (data) {
                            if (data.code == 200) {
                                $.message({
                                    message: "操作成功",
                                    type: 'success',
                                    showClose: true
                                });
                            } else {
                                $.message({
                                    message: data.msg,
                                    type: 'warning',
                                    showClose: true
                                });
                            }
                        }, error: function () {
                            $.message({
                                message: 'boom..',
                                type: 'error',
                                showClose: true
                            });
                        }, complete: function (XHR, TS) {
                            layer.close(index);
                            layer.close(layerIndex);
                        }
                    })
                }, function () {
                    layer.close(layerIndex);
                }
            );
        }
    });


    var $ = layui.$, active = {
        /**
         * 批量删除链接
         *
         * */
        closeBtn: function () {
            var $checkbox = $('table input[type="checkbox"][name="layTableCheckbox"]');
            var $checked = $('table input[type="checkbox"][name="layTableCheckbox"]:checked');
            if ($checkbox.is(":checked")) {
                var checkStatusId = table.checkStatus('onlineListReload'),
                    data = checkStatusId.data,
                    onlineSessionId = [];

                for (var i in data) {
                    onlineSessionId.push(data[i].onlineSessionId)
                }

                layer.confirm('确定要注销' + data.length + '个用户吗?', {title: '系统信息'}, function (index) {
                    var layerIndex = layer.load(2, {time: 5 * 1000});
                    $.ajax({
                        url: '/tiOnline/forceLogouts',
                        data: {
                            onlineSessionId: onlineSessionId
                        },
                        type: 'PUT',
                        success: function (res) {

                            if (res.code == 200) {
                                $.message({
                                    message: res.msg,
                                    type: 'success',
                                    showClose: true
                                });
                                table.reload('onlineListReload', {});
                            } else {
                                $.message({
                                    message: res.msg,
                                    type: 'warning',
                                    showClose: true
                                });
                            }

                        }, error: function (data) {
                            $.message({
                                message: '系统异常..',
                                type: 'error',
                                showClose: true
                            });
                        }, complete: function () {
                            layer.close(index);
                            layer.close(layerIndex);
                        }
                    })


                })
            } else {
                $.message({
                    message: '请选中要注销的用户',
                    type: 'warning',
                    showClose: true
                });
            }
        }
    };

    /**
     * 批量强退用户
     */
    $('.forceLogouts').on('click', function () {
        var layerIndex = layer.load(2, {time: 5 * 1000});
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });
})

