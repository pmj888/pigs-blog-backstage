layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'layedit', 'laydate', 'upload'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        upload = layui.upload;
    $ = layui.jquery;

    //创建一个编辑器
    var editIndex = layedit.build('news_content');
    var addNewsArray = [], addNews;


    /**
     * 监听提交 添加数据
     */
    form.on('submit(add)', function (data) {

        if ($("#sortName").val() == '') {
            $.message({
                message: '分类名称不能为空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($("#sortAlias").val() == '') {
            $.message({
                message: '分类别名不能为空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($("#sortDescription").val() == '') {
            $.message({
                message: '分类描述不能为空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        console.log(data.field)
        layer.confirm("确认要添加吗?", {
            yes: function (index, layero) {
                $.ajax({
                    type: "POST",
                    url: "/pgSorts/save",
                    data: data.field,
                    datatype: "json",
                    success: function (data) {
                        console.log(data);

                        if (data.code == 101) {
                            $.message({
                                message: "相同分类名已存在 !",
                                type: 'warning',
                                showClose: true
                            });
                            return false;
                        }

                        if (data.code == 100) {
                            $.message({
                                message: data.msg,
                                type: 'warning',
                                showClose: true
                            });
                            return false;
                        }

                        $.message({
                            message: "操作成功",
                            type: 'success',
                            showClose: true
                        });

                        setTimeout(function () {
                            parent.location.reload();
                        }, 2000);

                    },
                    error: function () {
                        $.message({
                            message: '系统匆忙，请销后再试 ！',
                            type: 'error',
                            showClose: true
                        });
                    }
                });
                layer.close(index);
            }
        });

        return false;
    })

})
