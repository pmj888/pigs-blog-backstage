layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    var layerIndex = layer.load(2);

    /***
     *
     * 显示全部管理员信息
     *
     */
    table.render({
        skin: 'line',
        elem: '#userList'
        , url: '/pgUsers/queryUserList'
        , cols: [[
            {type: 'checkbox', fixed: 'left'},
            {
                field: 'image', title: '头像', width: 60,
                templet: function (data) {
                    return '<img class="layui-circle" width="26" height="26" src=' + data.userProfilePhoto + '>'
                }
            }
            , {field: 'userName', title: '登录名'}
            , {field: 'userIp', title: 'IP'}
            , {
                field: 'userAge', title: '年龄'
            }, {
                field: 'userNickname', title: '昵称'
            }
            , {
                field: 'userState', title: '状态', width: 85, templet: function (data) {
                    if (data.userState == 0) {
                        return '<div> <input type="checkbox" checked="" name="codeSwitch" lay-skin="switch" id="open" lay-filter="switchTest" switchId=' + data.userId + '' +
                            ' lay-text="启用|禁用"  value=' + data.userState + '></div>';
                    }
                    return '<div> <input type="checkbox" lay-skin="switch" name="codeSwitch"  switchId=' + data.userId + ' lay-filter="switchTest"' +
                        'lay-text="启用|禁用" value=' + data.userState + '></div>';
                }
            }
            , {
                field: 'sex', title: '性别',
                templet: function (data) {
                    if (data.userSex == 1) {
                        return '<span>' + '男' + '</span>'
                    }
                    return '<span>' + '女' + '</span>'
                }
            }
            , {
                field: 'userTelephoneNumber', title: '手机号码', sort: true
            }, {
                field: 'userEmail', title: '邮箱', sort: true
            }, {
                field: 'userAge', title: '年龄'
            }, {
                field: 'userBirthday', title: '用户生日', sort: true
            }, {
                field: 'userRegistrationTime', title: '加入时间', sort: true
            }
            , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 55}
        ]]
        , limit: 20
        , limits: [20, 25, 50, 100]
        , parseData: function (data) {
            /**
             * 打印数据
             *console.log(data)
             */
            layer.close(layerIndex);
        }
        /**
         * 开启头部工具栏，并为其绑定左侧模板
         */
        , toolbar: '#utilsa'

        /**
         * 开启分页
         */
        , page: true
        , id: 'userListReload'
        , where: {userState: 0}
    });


    /**
     * 搜索框架 搜索内容
     * @type {*|pe}
     */
    var $ = layui.$, active = {
        reload: function () {
            var demoReload = $('#demoReload');
            /**
             * 搜索内容
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('userListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    userName: demoReload.val(),
                    userState: 0
                }
            }, 'data');

        }
        , recycleBin: function () {
            var demoReload = $('#demoReload');
            /**
             * 回收站
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('userListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    userName: demoReload.val(),
                    userState: 1
                }
            }, 'data');

        }
    };
    $('.demoTable .layui-btn').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });


    /**
     * 回收站
     */
    $('.recycleBin').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });


    /**
     * 监听开关 状态 操作
     */
    form.on('switch(switchTest)', function (data) {
        var layerIndex = layer.load(2);

        /**
         * 安全认证
         * 待开发
         */

        /**
         * 禁用用户
         * 状态 赋值为0
         */
        if ((this.checked ? 'true' : 'false') == 'false') {

            $.ajax({
                url: '/pgUsers/updateUserState',
                data: {
                    userState: 1,
                    userId: data.elem.getAttribute("switchId")
                },
                type: 'put', //HTTP请求类型
                success: function (data) {
                    if (data.code == 200) {
                        $.message({
                            message: '操作成功',
                            type: 'success',
                            showClose: true
                        });
                    }

                }, error: function () {

                }

            })


        } else {
            /**
             * 启动用户
             * 状态 赋值为1
             */

            $.ajax({
                url: '/pgUsers/updateUserState',
                data: {
                    userState: 0,
                    userId: data.elem.getAttribute("switchId")
                },
                type: 'put',
                success: function (data) {
                    if (data.code == 200) {
                        $.message({
                            message: '操作成功',
                            type: 'success',
                            showClose: true
                        });
                    }

                }, error: function (data) {

                }

            })
        }
        layer.close(layerIndex);
    })

    /**
     * 添加用户
     * 改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
     */
    $(window).one("resize", function () {
        $(".userAddBtn").click(function () {
            var layerIndex = layer.load(2);
            var index = layui.layer.open({
                title: "添加用户",
                type: 2,
                content: "/static/adminView/page/user/addUser.html",
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            })
            layui.layer.full(index);

            layer.close(layerIndex);
        })


    }).resize();

    /**
     *修改用户
     */
    table.on('tool(demo)', function (obj) {
        var layerIndex = layer.load(2);

        var data = obj.data;
        if (obj.event === 'detail') {
            layer.msg('ID：' + data.id + ' 的查看操作');
        } else if (obj.event === 'editUser') {
            var index = layui.layer.open({
                title: "修改用户信息",
                type: 2,
                content: "editUser.html?userId=" + data.userId,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            });

            layui.layer.full(index);

        }

        layer.close(layerIndex);
    });


});
