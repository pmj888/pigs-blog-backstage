layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'layedit', 'laydate', 'upload'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        upload = layui.upload;
    $ = layui.jquery;

    //创建一个编辑器
    var editIndex = layedit.build('news_content');
    var addNewsArray = [], addNews;


    /**
     * 监听提交
     */
    form.on('submit(update)', function (data) {

        if ($("#labelName").val() === '') {
            $.message({
                message: '标签名称不能为空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($("#labelAlias").val() === '') {
            $.message({
                message: '标签别名不能为空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($("#labelDescription").val() == '') {
            $.message({
                message: '标签描述不能为空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        console.log(data.field)

        layer.confirm("确认要修改吗?", {
            yes: function (index, layero) {

                $.ajax({
                    url: "/pgLabels/update",
                    type: 'PUT',
                    dataType: 'json',
                    data: data.field,
                    success: function (data) {
                        console.log(data);

                        if (data.code === 101) {
                            $.message({
                                message: "相同标签名已存在 !",
                                type: 'warning',
                                showClose: true
                            });
                            return false;
                        }

                        if (data.code === 100) {
                            $.message({
                                message: data.msg,
                                type: 'warning',
                                showClose: true
                            });
                            return false;
                        }

                        $.message({
                            message: "操作成功",
                            type: 'success',
                            showClose: true
                        });

                        setTimeout(function () {
                            parent.location.reload();
                        }, 2000);

                    },
                    error: function () {
                        $.message({
                            message: '系统匆忙，请销后再试 ！',
                            type: 'error',
                            showClose: true
                        });
                    }

                });
                layer.close(index);
            }
        })

        return false;
    })

})
