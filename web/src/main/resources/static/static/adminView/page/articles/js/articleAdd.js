 //注册插件
layui.config({
    base: '' //路径为插件
}).extend({
    formSelects: 'formSelects-v4'
});


//使用插件，在layui.use中添加formSelects
layui.use(['element', 'laydate', 'table', 'layer', 'form', 'laypage', 'upload', 'tree', 'formSelects'],
    function () {
        var element = layui.element
        laydate = layui.laydate,
            table = layui.table,
            layer = layui.layer,
            form = layui.form,
            upload = layui.upload,
            tree = layui.tree,
            laypage = layui.laypage,
            formSelects = layui.formSelects;
        form.render();
        formSelects.btns('select2', []);
        getClassify();


        //添加文章
        form.on('submit(addNews)', function (data) {
            console.log(data.field);
            /*console.log(data.field.test-editor-html-code);*/
            //发异步，把数据提交给php
            // layer.alert("保存成功", {icon: 6},function () {
            // 获得frame索引
            var index = parent.layer.getFrameIndex(window.name);
            //关闭当前frame
            //parent.layer.close(index);
            $.ajax({
                //提交数据的类型 POST GET
                type: "Put",
                //提交的网址
                url: "/pgArticles/insertArticle",
                //提交的数据
                data: JSON.stringify(data.field),
                //返回数据的格式
                datatype: "json",//“xml”, “html”, “script”, “json”, “jsonp”, “text”.
                contentType: "application/json; charset=utf-8",
                //成功返回之后调用的函数
                success: function (data) {
                    console.log(data);
                    if (data.code == 200) {
                        $.message({
                            message: "操作成功",
                            type: 'success',
                            showClose: true
                        });

                        setTimeout(function () {
                            window.parent.location.reload();//添加成功后刷新父界面
                        }, 1000);

                    } else {
                        $.message({
                            message: "系统匆忙，请销后再试 ！",
                            type: 'error',
                            showClose: true
                        });
                    }
                }
            });
            return false;
        });

        //设置editormd编辑器
        $(function () {
            var editor = editormd("test-editor", {
                width: "100%",            //宽度，不填为100%
                height: "500px",           //高度，不填为100%
                //theme : "dark",             //主题,不填为默认主题
                path: "editormd/lib/",   //editor.md插件的lib目录地址
                saveHTMLToTextarea: true, // 保存 HTML 到 Textarea
                toolbarAutoFixed: true,      //工具栏自动固定定位的开启与禁用

                placeholder: '本编辑器支持Markdown编辑，左边编写，右边预览',  //默认显示的文字，这里就不解释了
                syncScrolling: "single",
                emoji: false,
                taskList: true,
                tocm: true,         // Using [TOCM]
                tex: true,                   // 开启科学公式TeX语言支持，默认关闭
                flowChart: true,             // 开启流程图支持，默认关闭
                sequenceDiagram: true,       // 开启时序/序列图支持，默认关闭,
            });
        });
    });


function getClassify() {
    //请求所有的标签添加到下拉多选框里
    var keys = []
    $.ajax({
        url: "/pgArticles/queryLabelsList",
        type: "GET",
        async: true,
        success: function (data) {
            console.log(data, '查询标签');
            if (data) {
                for (var i = 0; i < data.length; i++) { //formSelects-v4只支持name和value两个属性，使用时必须为josn格式属性必须为name和value
                    var temp = {
                        "name": data[i].labelName,
                        "value": data[i].labelId  //value为唯一标识，此处为id
                    }
                    keys.push(temp)
                }
                formSelects.data('select2', 'local', { //请求数据后，将数据动态渲染到下拉框中
                    arr: keys
                });
                form.render();
            }
        },
    });

    //请求所有的分类添加到下拉列表里
    $.ajax({
        url: '/pgArticles/querySortsList',
        success: function (res) {
            if (res != null || res != '') {
                console.log(res.data)
                for (var i = 0; i < res.data.length; i++) {
                    console.log(res.data[i].sortName)
                    $("#classifyId").append("<option value=\"" + res.data[i].sortId + "\">" + res.data[i].sortName + "</option>");
                }
                //重新渲染
                layui.form.render("select");
            } else {
                layer.msg(res.message);
            }
        }
    });
}





