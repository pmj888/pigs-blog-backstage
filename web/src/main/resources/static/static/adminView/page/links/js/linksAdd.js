layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'layedit', 'laydate'], function () {
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        $ = layui.jquery;

    //创建一个编辑器
    var editIndex = layedit.build('links_content');

    form.on("submit(addLinks)", function (data) {
        if ($('.websiteName').val() == '') {
            $.message({
                message: '网站名不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }
        if ($('.websiteUrl').val() == '') {
            $.message({
                message: '网站地址不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        } else {
            var str = $('.websiteUrl').val()
            var Expression = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
            var objExp = new RegExp(Expression);
            if (objExp.test(str) != true) {
                $.message({
                    message: '网址格式不正确！',
                    type: 'warning',
                    showClose: true
                });
                return false;
            }
        }

        if ($('.authorMail').val() != '') {
            var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
            if (!reg.test($('.authorMail').val())) {
                $.message({
                    message: '邮箱格式不正确',
                    type: 'warning',
                    showClose: true
                });
                return false;
            }
        }

        var layerIndex = layer.load(2);
        /**
         * 添加友情链接
         * 然后刷新父页面
         */
        $.ajax({
            type: "POST",
            url: "/pgLinks/saveLink",
            datatype: "json",
            data: data.field,
            success: function (data) {
             //   console.log(data.data)
                if (data.code == 200) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });
                    setTimeout(function () {
                        layer.closeAll("iframe");
                        parent.location.reload();
                    }, 2000);
                } else {
                    $.message({
                        message: "操作失败",
                        type: 'warning',
                        showClose: true
                    });
                }

            }, error: function () {
                $.message({
                    message: '系统匆忙，请销后再试 ！',
                    type: 'error',
                    showClose: true
                });
            }, complete: function (XHR, TS) {
                layer.close(layerIndex);
            }

        });

        return false;
    })

})
