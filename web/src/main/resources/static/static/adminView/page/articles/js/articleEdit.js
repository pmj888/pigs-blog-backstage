//注册插件
layui.config({
    base: '' //路径为插件
}).extend({
    formSelects: 'formSelects-v4'
});


//使用插件，在layui.use中添加formSelects
layui.use(['element', 'laydate', 'table', 'layer', 'form', 'laypage', 'upload', 'tree', 'formSelects'],
    function () {
        var element = layui.element
        laydate = layui.laydate,
            table = layui.table,
            layer = layui.layer,
            form = layui.form,
            upload = layui.upload,
            tree = layui.tree,
            laypage = layui.laypage,
            formSelects = layui.formSelects;
        form.render();
        formSelects.btns('select2', []);
        getClassify();

        //URL参数获取   比如：获取url传过来articleId参数的值 $.getUrlParam("articleId")
        $.getUrlParam = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return unescape(r[2]);
            }
            return null;
        }


        //修改操作发送请求到后台获取数据回显
        var vArray = new Array();
        $.ajax({
            url: "/pgArticles/queryArticleById",
            type: "get",
            async: true,
            data: {
                articleId: $.getUrlParam("articleId")
            },
            success: function (data) {
                console.log(data, '根据id查询文章')

                if (data.code = 200) {
                    $(".newsName").val(data.data.article.articleTitle);
                    $(".articleContent").val(data.data.article.pgEditorFormat);
                    $(".articleId").val($.getUrlParam("articleId"))
                    $('select[name="pgSorts"]').find('option[value="' + data.data.ArtitleSort.sortId + '"]').attr('selected', 'selected')
                    for (i = 0; i < data.data.pgArtitleLabelRefList.length; i++) {
                        //console.log(data.data.pgArtitleLabelRefList[i].labelId);
                        vArray[i] = data.data.pgArtitleLabelRefList[i].labelId;
                    }

                    if (data.data.article.articleState == 0) {
                        $(".isShow").attr("checked", "checked");
                    }
                }
            },
        });

        //修改文章时查询所有标签添加到下拉多选框中
        var keys = []
        $.ajax({
            url: "/pgArticles/queryLabelsList",
            type: "get",
            async: true,
            /*data:{typeKey},*/
            success: function (data) {
                console.log(data, '标签');
                if (data) {
                    for (var i = 0; i < data.length; i++) { //formSelects-v4只支持name和value两个属性，使用时必须为josn格式属性必须为name和value
                        var temp = {
                            "name": data[i].labelName,
                            "value": data[i].labelId  //value为唯一标识，此处为id
                        }
                        keys.push(temp)
                    }
                    /*console.log(keys)*/
                    formSelects.data('select2', 'local', { //请求数据后，将数据动态渲染到下拉框中
                        arr: keys
                    });
                    setTimeout(function () {
                        formSelects.value('select2', vArray);
                    }, 200)
                    console.log(vArray, '数组')
                    formSelects.value('select2', vArray);
                    form.render();
                }
            },
        });


        //提交修改
        form.on('submit(updatePgArticles)', function (data) {
            console.log(data.field);
            /*console.log(data.field.test-editor-html-code);*/
            //发异步，把数据提交给php
            // layer.alert("保存成功", {icon: 6},function () {
            // 获得frame索引
            var index = parent.layer.getFrameIndex(window.name);
            //关闭当前frame
            //parent.layer.close(index);
            $.ajax({
                //提交数据的类型 POST GET
                type: "Put",
                //提交的网址
                url: "/pgArticles/updateArticle",
                //提交的数据
                data: JSON.stringify(data.field),
                //返回数据的格式
                datatype: "json",//“xml”, “html”, “script”, “json”, “jsonp”, “text”.
                contentType: "application/json; charset=utf-8",
                //成功返回之后调用的函数
                success: function (data) {
                    console.log(data);
                    if (data.code == 200) {
                        //obj.del();
                        layer.msg(data.msg, {
                            icon: 6,
                            time: 2000
                        });

                        setTimeout(function () {
                            window.parent.location.reload();//添加成功后刷新父界面
                        }, 1000);

                    } else {
                        layer.msg(data.msg, {icon: 5});
                    }
                }
            });
            return false;
        });

        //设置editormd编辑器
        $(function () {
            var editor = editormd("test-editor", {
                width: "100%",            //宽度，不填为100%
                height: "700px",           //高度，不填为100%
                //theme : "dark",             //主题,不填为默认主题
                path: "editormd/lib/",   //editor.md插件的lib目录地址
                saveHTMLToTextarea: true, // 保存 HTML 到 Textarea
                toolbarAutoFixed: true,      //工具栏自动固定定位的开启与禁用
            });
        });
    });


function getClassify() {

    //修改文章时查询所有分类添加到下拉列表中
    $.ajax({
        url: '/pgArticles/querySortsList',
        success: function (res) {
            if (res != null || res != '') {
                console.log(res.data)
                for (var i = 0; i < res.data.length; i++) {
                    console.log(res.data[i].sortName)
                    $("#classifyId").append("<option value=\"" + res.data[i].sortId + "\">" + res.data[i].sortName + "</option>");
                }
                //重新渲染
                layui.form.render("select");
            } else {
                layer.msg(res.message);
            }
        }
    });
}


