var $;
layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate'], function () {
    var form = layui.form,
        laydate = layui.laydate,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage;
    $ = layui.jquery;

    var menuList = xmSelect.render({
        el: '#menuList',
        filterable: true,
        direction: 'auto',
        theme: {
            color: '#33cabb',
        },
        tree: {
            show: true,
            showFolderIcon: true,
            showLine: true,
            indent: 20,
            strict: false,
            expandedKeys: [-3],
        },
        toolbar: {
            show: true,
            list: ['ALL', 'REVERSE', 'CLEAR']
        },
        height: 'auto',
        data: []
    })

    /**
     * 动态加载 菜单 tree
     */
    axios({
        method: 'get',
        url: '/pgMenu/queryMenuTree',
    }).then(response => {
        var res = response.data;
        menuList.update({
            data: res.data,
            autoRow: true,
        })
    });

    form.on("submit(roleSave)", function (data) {

        if ($('.roleName').val() == '') {
            $.message({
                message: '角色名不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.roleDescription').val() == '') {
            $.message({
                message: '角色描述不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.xm-select-default').val() == '') {
            $.message({
                message: '请选择菜单权限',
                type: 'warning',
                showClose: true
            });
            return false;
        }


        /**
         * 弹出loading
         * 然后刷新父页面
         */
        var layerIndex = layer.load(2);

        $.ajax({
            type: "POST",
            url: "/pgRole/saveRole",
            datatype: "json",
            data: data.field,
            success: function (data) {

                if (data.code == 1) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });
                    setTimeout(function () {
                        layer.closeAll("iframe");
                        parent.location.reload();
                    }, 1500);

                } else if (data.code == 101) {
                    $.message({
                        message: "角色名已存在 ！",
                        type: 'warning',
                        showClose: true
                    });
                } else if (data.code == 102) {
                    $.message({
                        message: "描述已存在 ！",
                        type: 'warning',
                        showClose: true
                    });
                }
            }, error: function () {
                $.message({
                    message: "系统匆忙，请销后再试 ！",
                    type: 'error',
                    showClose: true
                });

            }, complete: function () {
                layer.close(layerIndex);
            }
        });
        return false;
    })


})


