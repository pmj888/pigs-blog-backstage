var $;
layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery', 'laydate'], function () {
    var form = layui.form,
        laydate = layui.laydate,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage;
    $ = layui.jquery;

    laydate.render({
        //指定元素
        elem: '#userBirthday'
        , calendar: true
        , format: 'yyyy-M-d'
        , max: minDate()

    });

    function minDate() {
        var now = new Date();
        return now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
    }


    /**
     * 查询角色 并回显
     */
    $.ajax({
        type: "GET",
        url: "/pgRole/queryRoleInfo",
        datatype: "json",
        success: function (data) {

            if (data.code == 200) {
                $.each(data.data, function (index, item) {
                    $('#role').append(new Option(item.roleDescription, item.roleId));
                });
                layui.form.render("select");
            }

        }, error: function () {
            $.message({
                message: "系统匆忙，请销后再试 ！",
                type: 'error',
                showClose: true
            });
        }
    });

    form.on("submit(addUser)", function (data) {

        if ($('.userName').val() == '') {
            $.message({
                message: '登录名不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.userPassword').val() == '') {
            $.message({
                message: '密码不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.userNickname').val() == '') {
            $.message({
                message: '昵称不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.userEmail').val() == '') {
            $.message({
                message: '邮箱不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        if (!reg.test($('.userEmail').val())) {
            $.message({
                message: '邮箱格式不正确',
                type: 'warning',
                showClose: true
            });
            return false;
        }


        if ($('.userTelephoneNumber').val() == '') {
            $.message({
                message: '手机号码不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        } else {
            var pattern = /^1[34578]\d{9}$/;
            if (!pattern.test($('.userTelephoneNumber').val())) {

                $.message({
                    message: '号码格式不正确',
                    type: 'warning',
                    showClose: true
                });
                return false;
            }

        }

        if ($('.userBirthday').val() == '') {
            $.message({
                message: '生日日期不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        /**
         * 弹出loading
         * 然后刷新父页面
         */
        var layerIndex = layer.load(2);

        $.ajax({
            type: "POST",
            url: "/pgUsers/saveUser",
            datatype: "json",
            data: data.field,
            success: function (data) {
                if (data.code == 101) {
                    $.message({
                        message: '登录名已存在 ！',
                        type: 'warning',
                        showClose: true
                    });
                }

                if (data.code == 1) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });
                    setTimeout(function () {
                        layer.closeAll("iframe");
                        parent.location.reload();
                    }, 2000);

                }
            }, error: function () {
                $.message({
                    message: "系统匆忙，请销后再试 ！",
                    type: 'error',
                    showClose: true
                });
            }
        });
        layer.close(layerIndex);

        return false;
    })


})


