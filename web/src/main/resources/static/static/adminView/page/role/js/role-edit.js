var $;
layui.config({
    base: "js/"
}).use(['form', 'layer', 'jquery'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer;
    $ = layui.jquery;

    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }

    var menuList = xmSelect.render({
        el: '#menuList',
        filterable: true,
        direction: 'down',
        theme: {
            color: '#33cabb',
        },
        tree: {
            show: true,
            showFolderIcon: true,
            showLine: true,
            indent: 20,
            strict: false,
            simple: true,
            expandedKeys: [-3],
        },
        toolbar: {
            show: true,
            list: ['ALL', 'REVERSE', 'CLEAR']
        },
        height: 'auto',
        data: []
    })

    /**
     * 动态加载 菜单 tree
     *
     */
    $.ajax({
        type: "get",
        url: "/pgMenu/queryMenuTree",
        datatype: "json",
        success: function (data) {
            var res = data.data;
            menuList.update({
                data: res,
                autoRow: true,
            })
        }, error: function () {
            $.message({
                message: "系统匆忙，请销后再试 ！",
                type: 'error',
                showClose: true
            });
        }, complete: function (XHR, TS) {
            /**
             * 通过角色id查询菜单权限
             */
            $.ajax({
                type: "GET",
                url: "/pgRole/queryRoleMenuRefListByRoleId",
                datatype: "json",
                data: {
                    roleId: $.getUrlParam("roleId")
                },
                success: function (data) {

                    if (data.code == 200) {
                        menuList.setValue(data.data)
                        form.render();
                    }

                }, error: function () {
                    $.message({
                        message: "系统匆忙，请销后再试 ！",
                        type: 'error',
                        showClose: true
                    });
                }, complete: function (XHR, TS) {

                }
            });
        }
    });


    /**
     * 查询角色信息 并回显
     */
    $.ajax({
        type: "POST",
        url: "/pgRole/queryRoleInfo",
        datatype: "json",
        data: {
            roleId: $.getUrlParam("roleId")
        },
        success: function (data) {
            // console.log(data)
            if (data.code == 200) {
                $('.roleId').val(data.data.roleId);
                $('.roleDescription').val(data.data.roleDescription);
                $('.roleName').val(data.data.roleName);

                $(".roleState").each(function () {
                    $(this).children("option").each(function () {
                        if (this.value == data.data.roleState) {
                            $(this).attr("selected", "selected");
                        }
                    });
                })
                form.render();
            }

        }, error: function () {
            $.message({
                message: "系统匆忙，请销后再试 ！",
                type: 'error',
                showClose: true
            });
        }, complete: function () {

        }
    });


    form.on("submit(editUser)", function (data) {
        if ($('.roleName').val() == '') {
            $.message({
                message: '角色名不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        if ($('.roleDescription').val() == '') {
            $.message({
                message: '角色描述不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }

        /**
         * 弹出loading
         * 然后刷新父页面
         */
        var layerIndex = layer.load(2);
        $.ajax({
            type: "PUT",
            url: "/pgRole/editRole",
            datatype: "json",
            data: data.field,
            success: function (data) {

                //console.log(data)

                if (data.code == 101) {
                    $.message({
                        message: '登录名已存在 ！',
                        type: 'warning',
                        showClose: true
                    });
                }

                if (data.code == 102) {
                    $.message({
                        message: '描述已存在 ！',
                        type: 'warning',
                        showClose: true
                    });
                }

                if (data.code == 1) {
                    $.message({
                        message: '操作成功',
                        type: 'success',
                        showClose: true
                    });
                    setTimeout(function () {
                        layer.closeAll("iframe");
                        parent.location.reload();
                    }, 2000);

                }

            }, error: function () {
                $.message({
                    message: "系统匆忙，请销后再试 ！",
                    type: 'error',
                    showClose: true
                });
            }, complete: function () {
                layer.close(layerIndex);
            }
        });

        return false;
    })

})


