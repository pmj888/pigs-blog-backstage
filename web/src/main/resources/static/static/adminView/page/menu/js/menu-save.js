var $;
layui.config({
    base: '/static/res/iconpicker/module/'
}).use(['form', 'layer', 'jquery', 'iconPicker'], function () {
    var form = layui.form,
        iconPicker = layui.iconPicker,
        eleTree = layui.eleTree,
        layer = parent.layer === undefined ? layui.layer : parent.layer;
    $ = layui.jquery;
    var layerIndex = layer.load(2);
    var menuList = xmSelect.render({
        el: '#menuList',
        model: {label: {type: 'text'}},
        radio: true,
        clickClose: true,
        filterable: true,
        theme: {
            color: '#33cabb',
        },
        tree: {
            show: true,
            strict: false,
            expandedKeys: [-1],
        },
        height: 'auto',
        data: []
    })

    /**
     * 动态加载 菜单 tree
     */
    axios({
        method: 'get',
        url: '/pgMenu/queryMenuTree',
    }).then(response => {
        var res = response.data;
        res.data.push({name: '主目录', value: 0, selected: true});
        menuList.update({
            data: res.data,
            autoRow: true,
        });
        layer.close(layerIndex);
    }).catch(err => {
        $.message({
            message: err.msg,
            type: 'warning',
            showClose: true
        });
    });

    /**
     * 图标选择
     */
    iconPicker.render({
        elem: '#iconPicker',
        type: 'fontClass',
        search: true,
        page: true,
        limit: 12,
        click: function (data) {
            //console.log(data);
        },
        success: function (d) {
            // console.log(d);
        }
    });


    form.on("submit(menuSave)", function (data) {

        if ($('.menuName').val() == '') {
            $.message({
                message: '菜单名不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        }
        if ($('.orderNum').val() == '') {
            $.message({
                message: '排序不能空',
                type: 'warning',
                showClose: true
            });
            return false;
        } else {
            var patrn = /^[0-9]*$/;
            if (!patrn.test($('.orderNum').val())) {
                $.message({
                    message: '排序只能输入数字',
                    type: 'warning',
                    showClose: true
                });
                return false;
            }
        }
        var layerIndex = layer.load(2);
        /**
         * 弹出loading
         * 然后刷新父页面
         */
        $.ajax({
            type: "POST",
            url: "/pgMenu/saveMenu",
            datatype: "json",
            data: data.field,
            success: function (data) {
                if (data.code == 200) {
                    $.message({
                        message: "添加成功",
                        type: 'success',
                        showClose: true
                    });
                    setTimeout(function () {
                        layer.closeAll("iframe");
                        parent.location.reload();
                    }, 2000);
                } else if (data.code == 101) {
                    $.message({
                        message: "菜单名已存在",
                        type: 'warning',
                        showClose: true
                    });
                } else {
                    $.message({
                        message: "添加失败",
                        type: 'warning',
                        showClose: true
                    });
                }

            }, error: function () {
                $.message({
                    message: '系统匆忙，请销后再试 ！',
                    type: 'error',
                    showClose: true
                });
            }, complete: function (XHR, TS) {
                layer.close(layerIndex);
            }

        });

        return false;
    })


})


