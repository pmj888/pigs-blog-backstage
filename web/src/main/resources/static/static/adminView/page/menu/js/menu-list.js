layui.config({
    base: '/static/res/layui/module/'
}).extend({
    treetable: 'treetable-lay/treetable'
}).use(['form', 'layer', 'treetable', 'table'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        table = layui.table,
        treetable = layui.treetable;

    form.render();

    var renderTable = function () {

        /**
         * 树形图标显示在第几列
         * 最上级的父级id
         * id字段的名称
         * pid字段的名称
         * 是否默认折叠
         * 父级展开时是否自动展开所有子级
         */
        treetable.render({
            elem: '#menuTable',
            url: '/pgMenu/queryMenuList',
            treeColIndex: 1,
            checked: true,
            treeSpid: '0',
            treeIdName: 'menuId',
            treePidName: 'parentId',
            treeDefaultClose: true,
            treeLinkage: true,
            page: false,
            icon: false,
            cols: [
                [
                    {type: 'checkbox'},
                    {
                        field: 'menuName', title: '菜单名称',
                        templet: function (d) {
                            return '<span><i class="layui-icon">' + d.icon + '</i>' + d.menuName + '</span>';
                        }
                    },
                    {field: 'orderNum', title: '排序'},
                    {field: 'url', title: '请求地址'},
                    {
                        field: 'menuType', title: '类型', align: "center",
                        templet: function (d) {
                            if (d.menuType == 'M') {
                                return '<span class="layui-btn layui-btn-xs">目录</span>';
                            } else if (d.menuType == 'C') {
                                return '<span class="layui-btn layui-btn-normal layui-btn-xs">菜单</span>';
                            } else {
                                return '<span class="layui-btn  layui-btn-primary layui-btn-xs">按钮</span>';
                            }
                        }
                    }, {
                    field: 'state', title: '可见', align: "center",
                    templet: function (d) {
                        if (d.menuType == 'F') {
                            return '-';
                        }
                        if (d.state == '0') {
                            return '<span class="layui-btn layui-btn-primary layui-btn-xs" style="border-radius: 20px">显示</span>';
                        } else if (d.state == '1') {
                            return '<span class="layui-btn  layui-btn-xs" style="border-radius: 20px;background-color:  #ef5285">隐藏</span>';
                        }

                    }
                }, {field: 'perms', title: '权限标识符'},
                    {templet: complain, minwidth: '50px', title: '操作'}
                ]
            ],
            done: function (data) {

            }

        });
    };
    renderTable();


    /**
     * 添加角色
     * 改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
     */
    $(window).one("resize", function () {
        $("#saveMenu").click(function () {
            var layerIndex = layer.load(2);
            var index = layui.layer.open({
                title: "添加菜单",
                type: 2,
                content: "/static/adminView/page/menu/menu-save.html",
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回菜单列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            })
            layui.layer.full(index);
            layer.close(layerIndex);
        })
    }).resize();

    /**
     * 折叠与展开
     */
    $('#btn-expand').click(function () {
        treetable.expandAll('#menuTable');
    });
    $('#btn-fold').click(function () {
        treetable.foldAll('#menuTable');
    });

    /**
     * 操作中显示的内容
     * @param d
     * @returns {string}
     */
    function complain(d) {
        if (d.url != null) {
            return [
                '<a class="operation layui-btn layui-btn-xs" lay-event="editMenu" href="javascript:void(0)"  title="编辑">',
                '<i class="layui-icon layui-icon-edit"></i></a>',
                '<a class="operation layui-btn layui-btn-danger layui-btn-xs" lay-event="delMenu" href="javascript:void(0)"  title="删除">',
                '<i class="layui-icon layui-icon-delete"></i></a>',
            ].join('');
        } else {
            return '';
        }

    }

    //监听工具条
    table.on('tool(menuTable)', function (obj) {
        var layerIndex = layer.load(2);
        var data = obj.data;
        var layEvent = obj.event;
        if (data.menuName != null) {
            if (layEvent === 'delMenu') {
                layer.confirm('确定删除该条菜单信息吗？', {title: '系统提示'}, function (index) {
                    $.ajax({
                        type: "DELETE",
                        url: "/pgMenu/delMenu",
                        datatype: "json",
                        data: {
                            menuId: data.id
                        },
                        success: function (data) {
                            // console.log(data)
                            if (data.code == 101) {
                                $.message({
                                    message: '存在子菜单,不允许删除..',
                                    type: 'warning',
                                    showClose: true
                                });
                            } else if (data.code == 102) {
                                $.message({
                                    message: '菜单已分配,不允许删除..',
                                    type: 'warning',
                                    showClose: true
                                });
                            } else if (data.code == 1) {
                                obj.del();
                                $.message({
                                    message: '操作成功..',
                                    type: 'success',
                                    showClose: true
                                });
                            } else {
                                $.message({
                                    message: '操作失败..',
                                    type: 'warning',
                                    showClose: true
                                });
                            }
                        }, error: function () {
                            $.message({
                                message: 'boom..',
                                type: 'error',
                                showClose: true
                            });
                        }, complete: function (XHR, TS) {
                            layer.close(index);
                        }
                    });

                });
            } else if (layEvent === 'editMenu') {
                var index = layui.layer.open({
                    title: "修改菜单信息",
                    type: 2,
                    content: "/static/adminView/page/menu/menu-edit.html?menuId=" + data.menuId,
                    success: function (layero, index) {
                        setTimeout(function () {
                            layui.layer.tips('点击此处返回菜单列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        }, 1000)
                    }
                });

                layui.layer.full(index);
            }

            layer.close(layerIndex);
        }
    });
});
