layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    var layerIndex = layer.load(2,{time: 5*1000});

    /***
     *
     * 显示全部角色信息
     *
     */
    table.render({
        elem: '#roleList'
        , url: '/pgRole/queryRoleList'
        , cols: [
            [
                {field: 'roleName', title: '角色名'}
                , {field: 'roleDescription', title: '角色描述'}
                , {
                field: 'roleState', title: '状态', width: 85, templet: function (data) {
                    if (data.roleState == 0) {
                        return '<div> <input type="checkbox" checked="" name="codeSwitch" lay-skin="switch" id="open" lay-filter="switchTest" switchId=' + data.roleId + '' +
                            ' lay-text="启用|禁用"  value=' + data.roleState + '></div>';
                    }
                    return '<div> <input type="checkbox" lay-skin="switch" name="codeSwitch"  switchId=' + data.roleId + ' lay-filter="switchTest"' +
                        'lay-text="启用|禁用" value=' + data.roleState + '></div>';
                }
            }
                , {
                field: 'roleCreateTime', title: '创建时间', sort: true
            }, {
                field: 'roleUpdateTime', title: '更新时间', sort: true
            }
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 100}
            ]
        ]
        , skin: 'line'
        , limit: 20
        , limits: [10, 25, 50, 100]
        , parseData: function (data) {
            /**
             * 打印数据
             *console.log(data)
             */
            layer.close(layerIndex);
        }

        /**
         * 开启分页
         */
        , page: true
        , id: 'roleListReload'
        , where: {roleState: 0}

    });


    /**
     * 搜索框架 搜索内容
     * @type {*|pe}
     */
    var $ = layui.$, active = {
        reload: function () {
            var demoReload = $('#demoReload');
            /**
             * 搜索内容
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('roleListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    roleName: demoReload.val(),
                    roleState: 0
                }
            }, 'data');

        }
        , recycleBin: function () {
            var demoReload = $('#demoReload');
            /**
             * 回收站
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('roleListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    roleName: demoReload.val(),
                    roleState: 1
                }
            }, 'data');

        }
    };

    $('.demoTable .layui-btn').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });


    /**
     * 回收站
     */
    $('.recycleBin').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });


    /**
     * 监听开关 状态 操作
     */
    form.on('switch(switchTest)', function (data) {
        var layerIndex = layer.load(2);

        /**
         * 安全认证
         * 待开发
         */

        /**
         * 禁用角色
         * 状态 赋值为0
         */
        if ((this.checked ? 'true' : 'false') == 'false') {
            $.ajax({
                url: '/pgRole/updateRoleState',
                data: {
                    roleState: 1,
                    roleId: data.elem.getAttribute("switchId")
                },
                type: 'PUT',
                success: function (data) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });

                }, error: function () {
                    $.message({
                        message: "系统匆忙，请销后再试 ！",
                        type: 'error',
                        showClose: true
                    });
                }
                , complete: function () {
                    layer.close(layerIndex);
                }

            })
        } else {
            /**
             * 启动角色
             * 状态 赋值为1
             */
            $.ajax({
                url: '/pgRole/updateRoleState',
                data: {
                    roleState: 0,
                    roleId: data.elem.getAttribute("switchId")
                },
                type: 'put',
                success: function (data) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });

                }, error: function () {
                    $.message({
                        message: "系统匆忙，请销后再试 ！",
                        type: 'error',
                        showClose: true
                    });

                }, complete: function () {
                    layer.close(layerIndex);
                }

            })
        }

    });


    /**
     * 添加角色
     * 改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
     */
    $(window).one("resize", function () {
        $(".saveRole").click(function () {
            var layerIndex = layer.load(2);
            var index = layui.layer.open({
                title: "添加角色",
                type: 2,
                content: "/static/adminView/page/role/role-save.html",
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回角色列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            })
            layui.layer.full(index);
            layer.close(layerIndex);
        })


    }).resize();

    /**
     *修改角色
     */
    table.on('tool(demo)', function (obj) {

        var data = obj.data;
        if (obj.event === 'delRole') {

            $.message({
                message: '待开发..',
                type: 'success',
                showClose: true
            });


        } else if (obj.event === 'edit') {
            var index = layui.layer.open({
                title: "修改角色信息",
                type: 2,
                content: "/static/adminView/page/role/role-edit.html?roleId=" + data.roleId,
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回角色列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            });
            layui.layer.full(index);
        }

    });


});
