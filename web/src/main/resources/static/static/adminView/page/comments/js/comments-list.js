//获取token
var token = CoreUtil.getData("access_token");
//地址栏转义token中的#号
// var tokenQuery = token.replace("#", "%23");
var tableIns1;
var table = layui.table;
var form = layui.form;
var layer = layui.layer;
var $ = jQuery = layui.jquery;
var laydate = layui.laydate;

layui.use(['table', 'layer', 'laydate'], function () {

    //加载table
    tableIns1 = table.render({
        skin: 'line',
        elem: '#showTable'
        , contentType: 'application/json'
        , headers: {"authorization": token}
        , page: true //开启分页
        , url: '/pgComments/listByPage' //数据接口
        , method: 'POST'
        , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": CoreUtil.isEmpty(res.data) ? 0 : res.data.total, //解析数据长度
                "data": CoreUtil.isEmpty(res.data) ? null : res.data.records //解析数据列表
            }
        }
        , cols: [
            [
                {type: 'checkbox', fixed: 'left'},
                {field: 'userName', title: '发表用户',totalRow: true},
                {field: 'articleTitle', title: '博文标题',totalRow: true},
                {field: 'commentLikeCount', title: '点赞数'},
                {field: 'commentDate', title: '评论日期', sort: true},
                {field: 'commentContent', title: '评论内容', sort: true},
                {field: 'commentState', title: '状态', align: 'center', unresize: "true",templet: '#isStatusTool'},
                {width: 120, toolbar: "#tool", title: '操作'}
            ]
        ]
        , toolbar: '#toolbar'

    });


    //表头工具
    table.on('toolbar(showTable)', function(obj){
        switch(obj.event){
            case 'batchDeleted':
                var checkStatus = table.checkStatus(obj.config.commentId);
                var data = checkStatus.data;
                if(data.length==0){
                    layer.msg("请选择要批量删除的列");
                }else {
                    var commentId = [];
                    $(data).each(function (index,item) {
                        commentId.push(item.commentId);
                    });
                    tipDialog(commentId);
                }
                break;
            case 'add':
                $(".table_div").hide();
                $(".operation").show();
                $(".title").html("新增");
                $(".operation input[name=commentId]").val("");
                $(".operation input[name=userName]").val("");
                $(".operation input[name=articleTitle]").val("");
                $(".operation input[name=commentLikeCount]").val("");
                $(".operation input[name=commentDate]").val("");
                $(".operation input[name=commentContent]").val("");
                $(".operation input[name=parentCommentId]").val("");
                $(".operation input[name=commentState]").val("");
                break;
        };
    });
    //列操作
    table.on('tool(showTable)',function (obj) {
        var data = obj.data;//获得当前行数据
        switch (obj.event) {
            case 'del':
                var commentId=[];
                commentId.push(data.commentId);
                tipDialog(commentId);
                break;
            case 'edit':
                $(".table_div").hide();
                $(".operation").show();
                $(".title").html("编辑");
                $(".operation input[name=commentId]").val(data.commentId);
                $(".operation input[name=userName]").val(data.userName);
                $(".operation input[name=articleTitle]").val(data.articleTitle);
                $(".operation input[name=commentLikeCount]").val(data.commentLikeCount);
                $(".operation input[name=commentDate]").val(data.commentDate);
                $(".operation input[name=commentContent]").val(data.commentContent);
                $(".operation input[name=parentCommentId]").val(data.parentCommentId);
                $(".operation input[name=commentState]").val(data.commentState);
                break;
        }
    });

    //导出
    $('#export').on('click', function () {
        //原先分页limit
        var exportParams = {
            limit: 10000,
            key: $("#key").val()
        };
        CoreUtil.sendPost("/pgComments/listByPage", exportParams, function (res) {
            //初始化渲染数据
            if (res.data.records != null) {
                table.exportFile(tableIns1.config.commentId, res.data.records, 'xls');
            }
        });
    });

    //删除
    var tipDialog=function (commentId) {
        layer.open({
            content: "确定要删除么?",
            yes: function(index, layero){
                layer.close(index); //如果设定了yes回调，需进行手工关闭
                CoreUtil.sendDelete("/pgComments/delete",commentId,function (res) {
                    layer.msg(res.msg, {time:1000},function () {
                        search();
                    });
                });
            }
        });
    };


    form.on('switch(commentState)', function (data) {
        var commentState = data.elem.checked ? 1 : 0; //1为已审核,0为未审核
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.post("/pgComments/v1/comment/commentstate",
            {
                "commentId": data.value,
                "commentState": commentState
            }, function (s) {
                setTimeout(function () {
                    top.layer.close(index);
                    form.render('select', 'commentState');
                    top.layer.msg(s.message);
                }, 500);
            });
    });



    //返回
    $("#btn_cancel").click(function() {
        $(".table_div").show();
        $(".operation").hide();
        return false;
    });

    //监听保存
    form.on('submit(submit)', function(data){
        if(data.field.commentId===undefined || data.field.commentId===null || data.field.commentId===""){
            CoreUtil.sendPost("/pgComments/add",data.field,function (res) {
                $(".table_div").show();
                $(".operation").hide();
                search();
            });
        }else {
            CoreUtil.sendPut("/pgComments/update",data.field,function (res) {
                $(".table_div").show();
                $(".operation").hide();
                search();
            });
        }

        return false;
    });
});

//执行查询
function search() {
    //这里以搜索为例
    tableIns1.reload({
        where: { //设定异步数据接口的额外参数，任意设
            key: $("#key").val()
        }
        , page: {
            curr: 1 //重新从第 1 页开始
        }
    });
};