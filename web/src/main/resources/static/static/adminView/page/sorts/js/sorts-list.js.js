layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    var layerIndex = layer.load(2);


    /***
     *
     * 显示全部分类信息
     *
     */
    table.render({
        skin: 'line'
        , elem: '#valueTable'
        , url: '/pgSorts/list'
        , cols: [
            [

                {type: 'checkbox', fixed: 'left'}
                , {field: 'sortName', title: '分类名称'}
                , {field: 'sortAlias', title: '分类别名', edit: 'text'}
                , {field: 'sortDescription', title: '分类描述', edit: 'text'}
                , {
                field: 'sortState', title: '状态', templet: function (data) {
                    if (data.sortState == 0) {
                        return '<div> <input type="checkbox" checked="" name="codeSwitch" lay-skin="switch" id="open" lay-filter="switchTest" switchId=' + data.sortId + '' +
                            ' lay-text="已启用|已禁用"  value=' + data.sortState + '></div>';
                    }
                    return '<div> <input type="checkbox" lay-skin="switch" name="codeSwitch"  switchId=' + data.sortId + ' lay-filter="switchTest"' +
                        'lay-text="已启用|已禁用" value=' + data.sortState + '></div>';

                }
            }

                , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 160}

            ]
        ]
        , limit: 10
        , limits: [10, 20, 25, 50, 100]
        , parseData: function (data) {

            layer.close(layerIndex);
            // 打印数据
            console.log(data)
        }

        /**
         * 开启分页
         */
        , page: true
        , where: {sortState: 0}
        , id: 'labelListReload'
    });

    /**
     * 搜索框架 搜索内容
     * @type {*|pe}
     */
    var $ = layui.$, active = {
        reload: function () {
            var demoReload = $('#demoReload');
            /**
             * 搜索内容
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('labelListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    sortName: demoReload.val(),
                    sortState: 0
                }
            }, 'data');

        }
        , recycleBin: function () {
            var demoReload = $('#demoReload');
            /**
             * 回收站
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('labelListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    sortName: demoReload.val(),
                    sortState: 1
                }
            }, 'data');

        }
    };

    /**
     * 模糊搜索
     */
    $('.demoTable .layui-btn').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });

    /**
     * 回收站
     */
    $('.recycleBin').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });


    /**
     * 监听状态开关操作
     */
    form.on('switch(switchTest)', function (data) {
        /**
         * 禁用标签
         * 状态 赋值为 1
         */
        var layerIndex = layer.load(2);
        if ((this.checked ? 'true' : 'false') == 'false') {
            $.ajax({
                url: '/pgSorts/delete',
                data: {
                    sortState: 1,
                    sortId: data.elem.getAttribute("switchId")
                },
                type: 'PUT',
                success: function (data) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });

                }, error: function () {
                    $.message({
                        message: "请求异常 !",
                        type: 'error',
                        showClose: true
                    });
                }

            })
        } else {
            /**
             * 启动标签
             * 状态 赋值为 0
             */
            $.ajax({
                url: '/pgSorts/delete',
                data: {
                    sortState: 0,
                    sortId: data.elem.getAttribute("switchId")
                },
                type: 'PUT',
                success: function (data) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });

                }, error: function () {
                    $.message({
                        message: "请求异常 !",
                        type: 'error',
                        showClose: true
                    });
                }

            })
        }
        layer.close(layerIndex);

    });

    /**
     * 添加分类信息
     * 改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
     */
    $(window).one("resize", function () {
        $(".sortsSave").click(function () {
            var index = layui.layer.open({
                title: "添加",
                type: 2,
                content: "./sorts-save.html",
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回分类列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            })
            layui.layer.full(index);
        })
        layer.close(layerIndex);
    }).resize();


    /**
     * 监听删除操作
     */
    table.on('tool(labels)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        var layerIndex = layer.load(2);

        if (layEvent === 'delSorts') {
            layer.confirm('确定删除该分类信息吗？', {title: '系统提示'}, function (index) {
                $.ajax({
                    type: "DELETE",
                    url: "/pgSorts/delSorts",
                    datatype: "json",
                    data: {
                        sortId: data.sortId
                    },
                    success: function (data) {

                        if (data.code == 101) {
                            $.message({
                                message: '分类已分配,不允许删除哦',
                                type: 'warning',
                                showClose: true
                            });

                        } else if (data.code == 1) {
                            obj.del();
                            $.message({
                                message: '操作成功',
                                type: 'success',
                                showClose: true
                            });
                        } else {
                            $.message({
                                message: '操作失败',
                                type: 'warning',
                                showClose: true
                            });
                        }
                    }, error: function () {
                        $.message({
                            message: '请求异常',
                            type: 'error',
                            showClose: true
                        });
                    }, complete: function (XHR, TS) {
                        layer.close(index);
                        layer.close(layerIndex);
                    }
                });

            });
            layer.close(layerIndex);

        } else if (obj.event === 'edit') {
            editSorts(data);
        }

    });


    /**
     * 监听单元格编辑
     */
    table.on('edit(labels)', function (obj) {
        var value = obj.value //得到修改后的值
            , data = obj.data //得到所在行所有键值
            , field = obj.field; //得到字段

        const jiuData = $(this).prev().text();

        if ((field == 'sortDescription')) {
            if (value == '') {
                $.message({
                    message: '分类描述不能为空',
                    type: 'warning',
                    showClose: true
                });
                $(this).val(jiuData)
                return false;
            }
        }
        if ((field == 'sortAlias')) {
            if (value == '') {
                $.message({
                    message: '分类别名不能为空',
                    type: 'warning',
                    showClose: true
                });
                $(this).val(jiuData)
                return false;
            }
        }

        //编辑
        var layerIndex = layer.load(2);
        $.ajax({
            type: "PUT",
            url: "/pgSorts/update",
            datatype: "json",
            data: data,
            success: function (data) {

                if (data.code == 200) {
                    $.message({
                        message: '操作成功',
                        type: 'success',
                        showClose: true
                    });
                } else {
                    $.message({
                        message: data.msg,
                        type: 'warning',
                        showClose: true
                    });
                }
            }, error: function () {
                $.message({
                    message: '请求异常 !',
                    type: 'error',
                    showClose: true
                });
            }, complete: function (XHR, TS) {
                layer.close(layerIndex);
            }

        });
    });


    /**
     *修改分类信息
     */
    function editSorts(data) {
        var layerIndex = layer.load(2);

        var index = layui.layer.open({
            title: "修改",
            type: 2,
            content: "./sorts-edit.html",
            success: function (layer, index) {

                var body = layui.layer.getChildFrame('body', index);
                if (data) {

                    /**
                     * 取到弹出层里的元素，并把编辑的内容放进去
                     * 重新渲染表单
                     */
                    body.find("#sortId").val(data.sortId)
                    body.find("#sortName").val(data.sortName);
                    body.find("#sortAlias").val(data.sortAlias);
                    body.find("#sortDescription").val(data.sortDescription);

                    body.find("#sortState").each(function () {
                        $(this).children("option").each(function () {
                            if (this.value == data.data.sortState) {
                                $(this).attr("selected", "selected");
                            }
                        });
                    })
                    form.render();
                }

                setTimeout(function () {
                    layui.layer.tips('点击此处返回分类列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 1000)
            }
        });
        layui.layer.full(index);
        layer.close(layerIndex);
    }


});
