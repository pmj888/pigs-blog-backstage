layui.config({
    base: "js/"
}).use(['table', 'layer', 'form', 'jquery'], function () {
    var table = layui.table,
        form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    var layerIndex = layer.load(2);


    /***
     *
     * 显示全部标签信息
     *
     */
    table.render({
        skin: 'line'
        , elem: '#valueTable'
        , url: '/pgLabels/list'
        , cols: [
            [
                {type: 'checkbox', fixed: 'left'}
                , {field: 'labelName', title: '标签名称'}
                , {field: 'labelAlias', title: '标签别名', edit: 'text'}
                , {field: 'labelDescription', title: '标签描述', edit: 'text'}
                , {
                field: 'labelState', title: '状态', templet: function (data) {
                    if (data.labelState == 0) {
                        return '<div> <input type="checkbox" checked="" name="codeSwitch" lay-skin="switch" id="open" lay-filter="switchTest" switchId=' + data.labelId + '' +
                            ' lay-text="已启用|已禁用"  value=' + data.labelState + '></div>';
                    }
                    return '<div> <input type="checkbox" lay-skin="switch" name="codeSwitch"  switchId=' + data.labelId + ' lay-filter="switchTest"' +
                        'lay-text="已启用|已禁用" value=' + data.labelState + '></div>';

                }
            }

                , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 80}

            ]
        ]
        , limit: 10
        , limits: [10, 20, 25, 50, 100]
        , parseData: function (data) {

            layer.close(layerIndex);
            //打印数据
            console.log(data)
        }
        /**
         * 开启分页
         */
        , page: true
        , where: {labelState: 0}
        , id: 'labelListReload'
    });

    /**
     * 搜索框架 搜索内容
     * @type {*|pe}
     */
    var $ = layui.$, active = {
        reload: function () {
            var demoReload = $('#demoReload');
            /**
             * 搜索内容
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('labelListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    labelName: demoReload.val(),
                    labelState: 0
                }
            }, 'data');

        }
        , recycleBin: function () {
            var demoReload = $('#demoReload');
            /**
             * 回收站
             * 执行重载
             * 重新从第 1 页开始
             */
            table.reload('labelListReload', {
                page: {
                    curr: 1
                }
                , where: {
                    labelName: demoReload.val(),
                    labelState: 1
                }
            }, 'data');

        }
    };

    /**
     * 模糊搜索
     */
    $('.demoTable .layui-btn').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });

    /**
     * 回收站
     */
    $('.recycleBin').on('click', function () {
        var layerIndex = layer.load(2);
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
        layer.close(layerIndex);
    });


    /**
     * 监听状态开关操作
     */
    form.on('switch(switchTest)', function (data) {
        /**
         * 禁用标签
         * 状态 赋值为 1
         */
        var layerIndex = layer.load(2);

        if ((this.checked ? 'true' : 'false') == 'false') {
            $.ajax({
                url: '/pgLabels/delete',
                data: {
                    labelState: 1,
                    labelId: data.elem.getAttribute("switchId")
                },
                type: 'PUT',
                success: function (data) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });

                }, error: function () {
                    $.message({
                        message: "请求异常 !",
                        type: 'error',
                        showClose: true
                    });
                }

            })
        } else {
            /**
             * 启动标签
             * 状态 赋值为 0
             */
            $.ajax({
                url: '/pgLabels/delete',
                data: {
                    labelState: 0,
                    labelId: data.elem.getAttribute("switchId")
                },
                type: 'PUT',
                success: function (data) {
                    $.message({
                        message: "操作成功",
                        type: 'success',
                        showClose: true
                    });

                }, error: function () {
                    $.message({
                        message: "请求异常 !",
                        type: 'error',
                        showClose: true
                    });
                }

            })
        }
        layer.close(layerIndex);

    });


    /**
     * 添加标签信息
     * 改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
     */
    $(window).one("resize", function () {
        var layerIndex = layer.load(2);

        $(".saveLabel").click(function () {
            var index = layui.layer.open({
                title: "添加标签",
                type: 2,
                content: "./labels-save.html",
                success: function (layero, index) {
                    setTimeout(function () {
                        layui.layer.tips('点击此处返回标签列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            })
            layui.layer.full(index);
        })
        layer.close(layerIndex);

    }).resize();


    /**
     * 监听单元格编辑
     */
    table.on('edit(labels)', function (obj) {
        var value = obj.value //得到修改后的值
            , data = obj.data //得到所在行所有键值
            , field = obj.field; //得到字段

        const jiuData = $(this).prev().text();

        if ((field == 'labelDescription')) {
            if (value == '') {
                $.message({
                    message: '标签描述不能为空',
                    type: 'warning',
                    showClose: true
                });
                $(this).val(jiuData)
                return false;
            }
        }
        if ((field == 'labelAlias')) {
            if (value == '') {
                $.message({
                    message: '标签别名不能为空',
                    type: 'warning',
                    showClose: true
                });
                $(this).val(jiuData)
                return false;
            }
        }

        //编辑
        var layerIndex = layer.load(2);
        $.ajax({
            type: "PUT",
            url: "/pgLabels/update",
            datatype: "json",
            data: data,
            success: function (data) {

                if (data.code == 200) {
                    $.message({
                        message: '操作成功',
                        type: 'success',
                        showClose: true
                    });
                } else {
                    $.message({
                        message: data.msg,
                        type: 'warning',
                        showClose: true
                    });
                }
            }, error: function () {
                $.message({
                    message: 'boom..',
                    type: 'error',
                    showClose: true
                });
            }, complete: function (XHR, TS) {
                layer.close(layerIndex);
            }

        });
    });


    /**
     *修改标签信息
     */
    table.on('tool(labels)', function (obj) {
        var layerIndex = layer.load(2);
        var data = obj.data;

        if (obj.event === 'detail') {
            layer.msg('ID：' + data.labelId + ' 的查看操作');
        } else if (obj.event === 'edit') {
            // layer.alert('编辑行：<br>' + JSON.stringify(data));
            console.log(data)
            var index = layui.layer.open({
                title: "修改标签信息",
                type: 2,
                content: "./labels-edit.html",
                success: function (layer, index) {

                    var body = layui.layer.getChildFrame('body', index);
                    if (data) {

                        /**
                         * 取到弹出层里的元素，并把编辑的内容放进去
                         * 重新渲染表单
                         */
                        body.find("#labelId").val(data.labelId)
                        body.find("#labelName").val(data.labelName);
                        body.find("#labelAlias").val(data.labelAlias);
                        body.find("#labelDescription").val(data.labelDescription);

                        $("#labelState").each(function () {
                            $(this).children("option").each(function () {
                                if (this.value == data.data.labelState) {
                                    $(this).attr("selected", "selected");
                                }
                            });
                        })
                        form.render();

                    }

                    setTimeout(function () {
                        layui.layer.tips('点击此处返回标签列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 1000)
                }
            });

            layui.layer.full(index);
        }
        layer.close(layerIndex);
    });


});
