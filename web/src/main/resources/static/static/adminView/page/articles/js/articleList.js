layui.use(['laydate', 'laypage', 'layer', 'table', 'carousel', 'upload', 'element', 'slider', 'form', 'table'], function () {
    var laydate = layui.laydate //日期
        , laypage = layui.laypage //分页
        , layer = layui.layer //弹层
        , table = layui.table //表格
        , carousel = layui.carousel //轮播
        , upload = layui.upload //上传
        , element = layui.element //元素操作
        , slider = layui.slider //滑块
        , form = layui.form
        , $ = layui.$;

    //执行一个 table 实例
    table.render({
        id: 'articleId',//注意：1.如果需要多列隐藏实现方法是   id:'id,sex', 2.在表格的配置中编写完成之后，要注意在cols中不要在写这一列，这样才能实现列的隐藏。而且在获取表格的数据时是能获取到的
        elem: '#demo'
        , url: '/pgArticles/queryArticlesList' //数据接口
        , title: '文章列表'
        , page: true //开启分页
        , toolbar: '#utilsa' //#utilsa开启工具栏，此处显示默认图标，可以自定义模板，详见文档
        , cols: [[ //表头
            {type: 'checkbox', fixed: 'left'}
            /* ,{field: 'articleId', title: '文章编号', sort: true, fixed: 'left', totalRowText: '合计：'}*/
            , {field: 'articleTitle', title: '文章标题', width: '33%'}
            , {field: 'userName', title: '发布人', width: '7%', sort: true, totalRow: true}
            , {field: 'articleViews', title: '浏览量', width: '7%', sort: true, totalRow: true}
            , {field: 'articleCommentCount', title: '评论数', width: '7%', sort: true, totalRow: true}
            , {field: 'articleLikeCount', title: '点赞数', width: '7%', sort: true, totalRow: true}
            , {field: 'articleDate', title: '发布时间', width: '14%', sort: true, totalRow: true}
            , {field: 'articleState', title: '是否展示', width: '7%', align: 'center', templet: statusTpl}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barDemo'}
        ]],
        skin: 'line', //表格风格
        page: true, //是否显示分页
        id: 'articleReload',
        skip: true, //开启跳页
        limits: [10, 20, 30],
        limit: 10, //每页默认显示的数量
    });


    //根据文章标题关键字模糊搜索
    var $ = layui.$, active = {
        reload: function () {
            var articleTitle = $('#articleTitle');
            //执行重载
            table.reload('articleReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    articleTitle: articleTitle.val(),//逗号隔开
                }
            }, 'data');
        }
    };


    //搜索按钮点击事件
    $('#search').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });


    //修改文章状态（是否展示）
    form.on('switch(switchTest)', function (obj) {
        var articleState = this.checked ? '0' : '1';
        console.log("onoff:" + articleState, "id:" + this.value);
        $.post('/pgArticles/updateArticleState', {articleId: this.value, articleState: articleState}, function (data) {

            if (data) {
                layer.msg("状态修改成功");
                console.log(data, "状态")
            } else {
                layer.msg("状态修改失败");
            }
        });
    });


    //监听头工具栏事件
    table.on('toolbar(test)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id)
            , data = checkStatus.data; //获取选中的数据
        console.log(data)
        switch (obj.event) {
            case 'add':
                layer.msg('添加');
                break;
            case 'update':
                if (data.length === 0) {
                    layer.msg('请选择一行');
                } else if (data.length > 1) {
                    layer.msg('只能同时编辑一个');
                } else {
                    layer.alert('编辑 [id]：' + checkStatus.data[0].id);
                }
                break;
            case 'delete':
                if (data.length === 0) {
                    layer.msg('请选择一行');
                } else {
                    layer.msg('删除');
                }
                break;
        }
        ;
    });


    //监听行工具事件
    table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            , layEvent = obj.event; //获得 lay-event 对应的值
        console.log(data)
        if (layEvent === 'detail') {
            //ayer.msg('查看操作');
            parent.layer.open({
                type: 2,
                skin: 'layui-layer-molv',
                closeBtn: 1,
                anim: 3,
                title: '查看文章',
                area: ['100%', '100%'],
                shadeClose: false,
                content: "/static/adminView/page/articles/articleQuery.html",
                success: function (layero, index) {
                    var body = parent.layui.layer.getChildFrame('body', index);
                    if (data) {
                        // 取到弹出层里的元素，并把编辑的内容放进去
                        body.find("#articleContent").append(data.articleContent);
                        body.find(".articleTitle").append(data.articleTitle);
                        body.find(".follow-nickName").html(data.userName);
                        body.find(".time").html(data.articleDate);
                        body.find(".read-count").html(data.articleViews);
                        body.find(".get-collection").html(data.articleLikeCount);
                    }
                }
            });
        } else if (layEvent === 'del') {
            layer.confirm('确定删除该文章信息吗？', function (index) {
                $.ajax({
                    url: "/pgArticles/delArticleById",
                    type: "DELETE",
                    data: {articleId: data.articleId},
                    success: function (data) {
                        if (data.code == 200) {
                            //删除这一行，前端界面的修改，直接删除了这一条数据
                            obj.del();
                            //关闭弹框
                            layer.close(index);
                            //显示提示框
                            layer.msg("删除成功", {icon: 6});
                        } else {
                            layer.msg("删除失败", {icon: 5});
                        }
                    }
                });
                return false;
            });
        } else if (layEvent === 'edit') {
            console.log("获得当前行数据:")
            console.log(data)
            //  console.log("获得当前行数据"+data)
            // console.log("获得 lay-event 对应的值"+layEvent)
            //layer.msg('编辑操作');
            parent.layer.open({
                type: 2,
                skin: 'layui-layer-molv',
                closeBtn: 1,
                anim: 3,
                title: '编辑文章',
                area: ['100%', '100%'],
                shadeClose: false,
                content: "/static/adminView/page/articles/articleEdit.html?articleId=" + data.articleId,
                success: function (layero, index) {
                    setTimeout(function () {
                        parent.layui.layer.tips('点击此处返回文章列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500)
                }
            });
        }
    });


    //添加文章
    //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
    $(window).one("resize", function () {
        $(".newsAdd_btn").click(function () {
            parent.layer.open({
                type: 2,
                skin: 'layui-layer-molv',
                closeBtn: 1,
                anim: 3,
                title: '添加文章',
                area: ['100%', '100%'],
                shadeClose: false,
                content: "/static/adminView/page/articles/articleAdd.html",
                success: function (layero, index) {
                    setTimeout(function () {
                        parent.layui.layer.tips('点击此处返回文章列表', '.layui-layer-setwin .layui-layer-close', {
                            tips: 3
                        });
                    }, 500)
                }
            });
        })
    }).resize();


});