$(function () {
    /**
     * 折线统计图
     */
    var echartsRecords = echarts.init(document.getElementById('echarts-records'), 'walden');
    var optionRecords = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '邮件营销',
                type: 'line',
                data: [120, 132, 101, 134, 90, 230, 210]
            },
            {
                name: '联盟广告',
                type: 'line',
                data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
                name: '视频广告',
                type: 'line',
                data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
                name: '直接访问',
                type: 'line',
                data: [320, 332, 301, 334, 390, 330, 320]
            },
            {
                name: '搜索引擎',
                type: 'line',
                data: [820, 932, 901, 934, 1290, 1330, 1320]
            }
        ]
    };

    echartsRecords.setOption(optionRecords);


    /**
     * 基于准备好的dom，初始化echarts图表
     * 仪表
     * 内存消耗
     */
    var myChart = echarts.init(document.getElementById('main'));
    var option = {
        tooltip: {
            formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true}
            }
        },
        series: [
            {
                name: '消耗',
                type: 'gauge',
                min: 0,
                max: 100,
                splitNumber: 10,       // 分割段数，默认为5
                axisLine: {            // 坐标轴线
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: [[0.2, '#33cabb'], [0.8, '#36a2eb'], [1, '#ff6384']],
                        width: 8
                    }
                },
                axisTick: {            // 坐标轴小标记
                    splitNumber: 10,   // 每份split细分多少段
                    length: 12,        // 属性length控制线长
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: 'auto'
                    }
                },
                axisLabel: {           // 坐标轴文本标签，详见axis.axisLabel
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        color: 'auto'
                    }
                },
                splitLine: {           // 分隔线
                    show: true,        // 默认显示，属性show控制显示与否
                    length: 30,         // 属性length控制线长
                    lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                        color: 'auto'
                    }
                },
                pointer: {
                    width: 4
                },
                title: {
                    show: true,
                    offsetCenter: [0, '-40%'],       // x, y，单位px
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'bolder'
                    }
                },
                detail: {
                    formatter: '{value}/%',
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        color: 'auto',
                        fontWeight: 'bolder',
                        fontSize: 15
                    }
                },
                data: [{value: 100, name: '100/G'}]
            }
        ]
    };

    // 为echarts对象加载数据
    myChart.setOption(option);
    /**
     * 查询用户角色信息 并回显
     */
    setInterval(function () {
        $.ajax({
            type: "GET",
            url: "/system/resourceUsage",
            datatype: "json",
            success: function (data) {
                //  console.log(data)
                if (data.code == 200) {
                    option.series[0].data[0].name = data.data[1] + 'G';
                    option.series[0].data[0].value = data.data[0];
                    myChart.setOption(option, true);
                }
            }
        });
    }, 2000);

    /**
     * 浏览器大小改变时重置大小
     */
    window.onresize = function () {
        echartsRecords.resize();
        myChart.resize();
    };

})