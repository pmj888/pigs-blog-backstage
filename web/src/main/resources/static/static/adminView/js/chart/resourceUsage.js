$(document).ready(function () {
    var myChart = echarts.init(document.getElementById('main'));
    option = {
        tooltip: {
            formatter: "{a} <br/>{c} {b}"
        },
        series: [
            {
                name: 'CPU',
                type: 'gauge',
                center: ['40%', '50%'],
                z: 3,
                min: 0,
                max: 100,
                splitNumber: 8,
                radius: '50%',
                axisLine: {            // 坐标轴线
                    lineStyle: {       // 属性lineStyle控制线条样式
                        width: 6
                    }
                },
                axisTick: {            // 坐标轴小标记
                    length: 15,        // 属性length控制线长
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: 'auto'
                    }
                },
                splitLine: {           // 分隔线
                    length: 20,         // 属性length控制线长
                    lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                        color: 'auto'
                    }
                },
                pointer: {
                    width: 4
                },
                title: {
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal',
                        fontSize: 16,
                        // fontStyle: 'italic'
                    }
                },
                detail: {
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal'
                    }
                },
                data: [{value: 50, name: 'CPU/%'}]
            },
            {
                name: '内存',
                type: 'gauge',
                center: ['70%', '50%'],
                z: 3,
                min: 0,
                max: 100,
                splitNumber: 8,
                radius: '50%',
                axisLine: {            // 坐标轴线
                    lineStyle: {       // 属性lineStyle控制线条样式
                        width: 6
                    }
                },
                axisTick: {            // 坐标轴小标记
                    length: 15,        // 属性length控制线长
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: 'auto'
                    }
                },
                splitLine: {           // 分隔线
                    length: 20,         // 属性length控制线长
                    lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                        color: 'auto'
                    }
                },
                pointer: {
                    width: 4
                },
                title: {
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal',
                        fontSize: 16,
                        // fontStyle: 'italic'
                    }
                },
                detail: {
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal'
                    }
                },
                data: [{value: 50, name: '内存/G'}]
            },
            {
                name: 'JVM',
                type: 'gauge',
                center: ['10%', '50%'],    // 默认全局居中
                z: 3,
                min: 0,
                max: 100,
                splitNumber: 8,
                radius: '50%',
                axisLine: {            // 坐标轴线
                    lineStyle: {       // 属性lineStyle控制线条样式
                        width: 6
                    }
                },
                axisTick: {            // 坐标轴小标记
                    length: 15,        // 属性length控制线长
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: 'auto'
                    }
                },
                splitLine: {           // 分隔线
                    length: 20,         // 属性length控制线长
                    lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                        color: 'auto'
                    }
                },
                pointer: {
                    width: 4
                },
                title: {
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal',
                        fontSize: 16,
                        // fontStyle: 'italic'
                    }
                },
                detail: {
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal'
                    }
                },
                data: [{value: 50, name: 'JVM/M'}]
            }

        ]
    };


    $.ajax({
        type: "get",
        url: "/system/resourceUsage",
        datatype: "json",
        success: function (data) {
            console.log(data)
            if (data.code == 200) {

                option.series[0].data[0].value = data.data.cpu.used

                option.series[1].max = data.data.mem.total;
                option.series[1].data[0].value = data.data.mem.used

                option.series[2].max = data.data.jvm.total;
                option.series[2].data[0].value = data.data.jvm.used

                myChart.setOption(option);
            }
        }
    });


})