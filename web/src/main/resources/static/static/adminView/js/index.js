var $, tab, skyconsWeather;
layui.config({
    base: "/static/adminView/js/"
}).use(['bodyTab', 'form', 'element', 'layer', 'colorpicker', 'jquery'], function () {
    var form = layui.form,
        layer = layui.layer,
        element = layui.element
        , colorpicker = layui.colorpicker;
    $ = layui.jquery;
    tab = layui.bodyTab({
        openTabNum: "50",  //最大可打开窗口数量
        url: "/static/adminView/json/navs.json" //获取菜单json地址
    });

    var up = '<input type="hidden" name="color" value="" id="test-all-input"><div id="test-all"></div>';

    /**
     * 更换皮肤
     */
    function skins() {
        var skin = window.localStorage.getItem("skin");
        if (skin) {  //如果更换过皮肤
            $(".layui-layout-admin .layui-header").css("background-color", skin);
            $(".layui-bg-black").css("background-color", skin);
            $(".hideMenu").css("background-color", skin);
        }
    }

    skins();
    $(".changeSkin").click(function () {
        layer.open({
            title: "更换皮肤",
            area: ["230px", "250px"],
            type: "1",
            content: '<div class="skins_box">' +
                '<form class="layui-form">' +
                '<div class="layui-form-item" style="margin-bottom: 55%">\n' +
                ' 自定义&nbsp;' +
                '<input type="hidden" name="color" value="" id="test-all-input">\n' +
                '<div id="test-all"></div>\n' +
                '</div>' +
                '<div class="layui-form-item skinBtn">' +
                '<a href="javascript:;" class="layui-btn  layui-btn-sm layui-btn-small layui-btn-primary" lay-submit="" lay-filter="changeSkin">确定更换</a>' +
                '<a href="javascript:;" class="layui-btn  layui-btn-sm layui-btn-small" lay-submit="" lay-filter="noChangeSkin">我再想想</a>' +
                '</div>' +
                '</form>' +
                '</div>',
            success: function (index, layero) {
                form.on("submit(changeSkin)", function (data) {
                    console.log($('#test-all-input').val())
                    window.localStorage.setItem("skin", $('#test-all-input').val());
                    layer.closeAll("page");
                });
                form.on("submit(noChangeSkin)", function () {
                    $("body").removeAttr("class").addClass("main_body " + window.localStorage.getItem("skin") + "");
                    $(".layui-bg-black,.hideMenu,.layui-layout-admin .layui-header").removeAttr("style");
                    skins();
                    layer.closeAll("page");
                });
            },
            cancel: function () {
                $("body").removeAttr("class").addClass("main_body " + window.localStorage.getItem("skin") + "");
                $(".layui-bg-black,.hideMenu,.layui-layout-admin .layui-header").removeAttr("style");
                skins();
            }
        })
        //开启全功能
        colorpicker.render({
            elem: '#test-all'
            , color: 'rgba(7, 155, 140, 1)'
            , format: 'rgb'
            , predefine: true
            , alpha: true
            , done: function (color) {
                $('#test-all-input').val(color); //向隐藏域赋值
                color || this.change(color); //清空时执行 change
            }
            , change: function (color) {
                //给当前页面头部和左侧设置主题色
                $('.header').css('background-color', color);
                $('.left-nav').css('background-color', color);
            }
        });
    })


    /**
     * 清除缓存
     */
    $(".clearCache").click(function () {
        window.localStorage.clear();
        window.localStorage.clear();
        var layerIndex = layer.load(2, {time: 5 * 1000});
        setTimeout(function () {
            $.message({
                message: '缓存清除成功！',
                type: 'success',
                showClose: true
            });
            layer.close(layerIndex);
        }, 1000);
    })

    //隐藏左侧导航
    $(".hideMenu").click(function () {
        $(".layui-layout-admin").toggleClass("showMenu");
        //渲染顶部窗口
        tab.tabMove();
    })

    //渲染左侧菜单
    tab.render();

    /**
     * 查询用户信息显示到页面
     */
    $.ajax({
        type: "get",
        url: "/pgUsers/getShiroUserInfo",
        datatype: "json",
        success: function (data) {
            // console.log(data)
            if (data.code == 200) {
                $('.userProfilePhoto img').attr('src', data.data.userProfilePhoto);
                $('.userProfilePhoto').attr('title', data.data.userNickname);
                /**
                 *  如果昵称、登录名 大于 4、5位
                 *  那就会截取前4、5 位 并拼接 ..
                 */
                var userNickname = data.data.userNickname;
                if (userNickname.length > 4) {
                    $('.userProfilePhoto cite').html(userNickname.substring(0, 3) + "..");
                } else {
                    $('.userProfilePhoto cite').html(userNickname);
                }
                var userName = data.data.userName;
                $('.user-photo').attr('title', userName);
                if (userName.length > 10) {
                    $('.userName').text(userName.substring(0, 10) + "..");
                } else {
                    $('.userName').text(userName);
                }

            }
        }
    });

    //锁屏
    function lockPage() {
        layer.open({
            id: 'lockLay',
            title: false,
            type: 1,
            content: $('#lockHtml').html(),
            closeBtn: 0,
            shade: 0.01
        })
        $(".admin-header-lock-input").focus();
    }

    $(".lockcms").on("click", function () {
        window.localStorage.setItem("lockcms", true);
        lockPage();
    })

    // 判断是否显示锁屏
    if (window.sessionStorage.getItem("lockcms") == "true") {
        lockPage();
    }

    // 解锁
    $("body").on("click", "#lockLay button", function () {
        if ($(this).siblings(".admin-header-lock-input").val() == '') {
            $.message({
                message: '请输入解锁密码！',
                type: 'warning',
                showClose: true
            });
            $(this).siblings(".admin-header-lock-input").focus();
        } else {
            if ($(this).siblings(".admin-header-lock-input").val() == "123456") {
                window.sessionStorage.setItem("lockcms", false);
                $(this).siblings(".admin-header-lock-input").val('');
                layer.closeAll("page");
            } else {
                $.message({
                    message: '密码错误，请重新输入！',
                    type: 'warning',
                    showClose: true
                });
                $(this).siblings(".admin-header-lock-input").val('').focus();
            }
        }
    });

    $(document).on('keydown', function (event) {
        var event = event || window.event;
        if (event.keyCode == 13) {
            $("#lockLay button").click();
        }
    });


    //手机设备的简单适配
    var treeMobile = $('.site-tree-mobile'),
        shadeMobile = $('.site-mobile-shade')

    treeMobile.on('click', function () {
        $('body').addClass('site-mobile');
    });

    shadeMobile.on('click', function () {
        $('body').removeClass('site-mobile');
    });

    // 添加新窗口
    $("body").on("click", ".layui-nav .layui-nav-item a", function () {
        //如果不存在子级
        if ($(this).siblings().length == 0) {
            addTab($(this));
            $('body').removeClass('site-mobile');  //移动端点击菜单关闭菜单层
        }
        $(this).parent("li").siblings().removeClass("layui-nav-itemed");
    })

    //公告层
    function showNotice() {
        layer.open({
            type: 1,
            title: "系统公告",
            closeBtn: false,
            area: '310px',
            shade: 0.8,
            id: 'LAY_layuipro',
            btn: ['火速围观'],
            moveType: 1,
            content: '<div style="padding:15px 20px; text-align:justify; line-height: 22px; text-indent:2em;border-bottom:1px solid #e2e2e2;"><p>项目已上线啦</p></div>',
            success: function (layero) {
                var btn = layero.find('.layui-layer-btn');
                btn.css('text-align', 'center');
                btn.on("click", function () {
                    window.sessionStorage.setItem("showNotice", "true");
                })
                if ($(window).width() > 432) {  //如果页面宽度不足以显示顶部“系统公告”按钮，则不提示
                    btn.on("click", function () {
                        layer.tips('系统公告躲在了这里', '#showNotice', {
                            tips: 3
                        });
                    })
                }
            }
        });
    }

    //判断是否处于锁屏状态(如果关闭以后则未关闭浏览器之前不再显示)
    if (window.sessionStorage.getItem("lockcms") != "true" && window.sessionStorage.getItem("showNotice") != "true") {
        showNotice();
    }

    /**
     * 点击查看公告
     */
    $(".showNotice").on("click", function () {
        showNotice();
    })

    //刷新后还原打开的窗口
    if (window.sessionStorage.getItem("menu") != null) {
        menu = JSON.parse(window.sessionStorage.getItem("menu"));
        curmenu = window.sessionStorage.getItem("curmenu");
        var openTitle = '';
        for (var i = 0; i < menu.length; i++) {
            openTitle = '';
            if (menu[i].icon) {
                openTitle += '<i class="layui-icon ' + menu[i].icon + '"></i>';
            }
            openTitle += '<cite>' + menu[i].title + '</cite>';
            openTitle += '<i class="layui-icon layui-unselect layui-tab-close" data-id="' + menu[i].layId + '">&#x1006;</i>';
            element.tabAdd("bodyTab", {
                title: openTitle,
                content: "<iframe src='" + menu[i].href + "' data-id='" + menu[i].layId + "'></frame>",
                id: menu[i].layId
            })
            //定位到刷新前的窗口
            if (curmenu != "undefined") {
                if (curmenu == '' || curmenu == "null") {  //定位到后台首页
                    element.tabChange("bodyTab", '');
                } else if (JSON.parse(curmenu).title == menu[i].title) {  //定位到刷新前的页面
                    element.tabChange("bodyTab", menu[i].layId);
                }
            } else {
                element.tabChange("bodyTab", menu[menu.length - 1].layId);
            }
        }
        //渲染顶部窗口
        tab.tabMove();
    }

    /**
     * 刷新当前
     * 此处添加禁止连续点击刷新一是为了降低服务器压力，另外一个就是为了防止超快点击造成chrome本身的一些js文件的报错(不过貌似这个问题还是存在，不过概率小了很多)
     */
    $(".refresh").on("click", function () {
        if ($(this).hasClass("refreshThis")) {
            $(this).removeClass("refreshThis");
            $(".clildFrame .layui-tab-item.layui-show").find("iframe")[0].contentWindow.location.reload(true);
            setTimeout(function () {
                $(".refresh").addClass("refreshThis");
            }, 2000)
        } else {
            $.message({
                message: '不要急，慢点..',
                type: 'warning',
                showClose: true
            });
        }
    })

    //关闭其他
    $(".closePageOther").on("click", function () {
        if ($("#top_tabs li").length > 2 && $("#top_tabs li.layui-this cite").text() != "后台首页") {
            var menu = JSON.parse(window.sessionStorage.getItem("menu"));
            $("#top_tabs li").each(function () {
                if ($(this).attr("lay-id") != '' && !$(this).hasClass("layui-this")) {
                    element.tabDelete("bodyTab", $(this).attr("lay-id")).init();
                    //此处将当前窗口重新获取放入session，避免一个个删除来回循环造成的不必要工作量
                    for (var i = 0; i < menu.length; i++) {
                        if ($("#top_tabs li.layui-this cite").text() == menu[i].title) {
                            menu.splice(0, menu.length, menu[i]);
                            window.sessionStorage.setItem("menu", JSON.stringify(menu));
                        }
                    }
                }
            })
        } else if ($("#top_tabs li.layui-this cite").text() == "后台首页" && $("#top_tabs li").length > 1) {
            $("#top_tabs li").each(function () {
                if ($(this).attr("lay-id") != '' && !$(this).hasClass("layui-this")) {
                    element.tabDelete("bodyTab", $(this).attr("lay-id")).init();
                    window.sessionStorage.removeItem("menu");
                    menu = [];
                    window.sessionStorage.removeItem("curmenu");
                }
            })
        } else {
            $.message({
                message: '没有关闭的窗口了@_@',
                type: 'warning',
                showClose: true
            });
        }
        //渲染顶部窗口
        tab.tabMove();
    })
    //关闭全部
    $(".closePageAll").on("click", function () {
        if ($("#top_tabs li").length > 1) {
            $("#top_tabs li").each(function () {
                if ($(this).attr("lay-id") != '') {
                    element.tabDelete("bodyTab", $(this).attr("lay-id")).init();
                    window.sessionStorage.removeItem("menu");
                    menu = [];
                    window.sessionStorage.removeItem("curmenu");
                }
            })
        } else {
            $.message({
                message: '没有关闭的窗口了@_@',
                type: 'warning',
                showClose: true
            });
        }
        //渲染顶部窗口
        tab.tabMove();
    })

    /**
     * 退出关闭顶部窗口
     */
    $(".signOut").on("click", function () {
        window.sessionStorage.removeItem("menu");
        menu = [];
        window.sessionStorage.removeItem("curmenu");
    })

})

//打开新窗口
function addTab(_this) {
    tab.tabAdd(_this);
}

/**
 * 捐赠
 */
function donation() {
    layer.tab({
        area: ['254px', '373px'],
        tab: [{
            title: "微信",
            content: "<div style='padding:25px;overflow:hidden;'><img style='width: 202px;height:280px' src='/static/res/image/wx.png'></div>"
        }, {
            title: "支付宝",
            content: "<div style='padding:25px;overflow:hidden;'><img style='width: 201px;height:280px' src='/static/res/image/zfb.jpg'></div>"
        }]
    })
}
