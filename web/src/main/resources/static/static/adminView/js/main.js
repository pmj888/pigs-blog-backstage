layui.config({
    base: "js/"
}).use(['form', 'element', 'layer', 'jquery'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        element = layui.element,
        $ = layui.jquery;
    //  var layerIndex = layer.load(2,{time: 5*1000});

    $(".panel a").on("click", function () {
        window.parent.addTab($(this));
    })


    //新消息
    $.get("../json/message.json",
        function (data) {
            $(".newMessage span").text(108);
        }
    )


    //数字格式化
    $(".panel span").each(function () {
        $(this).html($(this).text() > 9999 ? ($(this).text() / 10000).toFixed(2) + "<em>万</em>" : $(this).text());
    })


})
