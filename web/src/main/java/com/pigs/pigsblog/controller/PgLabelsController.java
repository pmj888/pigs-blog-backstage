package com.pigs.pigsblog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgLabels;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgLabelsService;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 标签控制层
 *
 * @author PIGS
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/pgLabels/")
public class PgLabelsController {

    private Logger logger = LoggerFactory.getLogger(PgLabelsController.class);

    @Autowired
    private PgLabelsService pgLabelsService;


    /**
     * 分页全查询标签信息
     *
     * @return
     */
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "page", value = "当前页", required = true, dataType = "Integer", defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "每页记录数", required = true, dataType = "Integer", defaultValue = "5")
    })

    /**
     * 全查询标签信息
     *
     * @return
     */
    @ApiOperation("查询标签信息")
    @GetMapping("/list")
    public ResultFormatPaging pgLabelsList(PgLabels pgLabels, Integer page, Integer limit) {
        logger.info("pgLabel={}", pgLabels);
        IPage<PgLabels> pgSortsListIPage = pgLabelsService.selectPgLabelsList(pgLabels, page, limit);
        List<PgLabels> pgLabelsList = pgSortsListIPage.getRecords();
        logger.info("pgLabelsList={}", pgLabelsList);
        return ResultPagingUtil.pagingSuccess(0, pgSortsListIPage.getTotal(), pgLabelsList);
    }


    /**
     * 添加标签信息
     *
     * @param pgLabels
     * @return
     */
    @ApiOperation("添加标签信息")
    @PostMapping("/save")
    @ResponseBody
    public ResultFormat savePgLabels(PgLabels pgLabels) {

        Integer savePgLabels = pgLabelsService.savePgLabels(pgLabels);

        if (savePgLabels == 101) {
            logger.info("相同标签名已存在");
            return ResultUtil.error(101, "");
        }

        if (savePgLabels > 0) {
            logger.info("添加成功");
            return ResultUtil.success();
        } else {
            logger.info("添加失败");
            return ResultUtil.error(100, "添加失败");
        }

    }


    /**
     * 修改标签信息
     *
     * @param pgLabels
     * @return
     */
    @ApiOperation("修改标签信息")
    @PutMapping("/update")
    @ResponseBody
    public ResultFormat updatePgLabels(PgLabels pgLabels) {

        Integer updatePgLabels = pgLabelsService.updatePgLabels(pgLabels);

        logger.info("updatePgLabels={}", updatePgLabels);

        if (updatePgLabels == 101) {
            return ResultUtil.error(101, "");
        }

        if (updatePgLabels == 200) {
            return ResultUtil.success();
        }

        return ResultUtil.error(100, "修改失败");

    }


    /**
     * 修改标签状态
     *
     * @param pgLabels
     * @return
     */
    @ApiOperation("修改标签状态")
    @PutMapping("/delete")
    public ResultFormat deletePgLabels(PgLabels pgLabels) {

        if (pgLabels.getLabelState() != null && pgLabels.getLabelId() != null) {
            logger.info("pgLabels={}", pgLabels);
            boolean flag = pgLabelsService.updateById(pgLabels);

            if (flag) {
                logger.info("修改成功");
                return ResultUtil.success();
            }
        }

        return ResultUtil.error(100, "修改失败");
    }

}
