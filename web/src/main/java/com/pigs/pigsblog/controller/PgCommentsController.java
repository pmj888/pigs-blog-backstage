package com.pigs.pigsblog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgComments;
import com.pigs.pigsblog.enums.HttpStatusConstants;
import com.pigs.pigsblog.model.DataResult;
import com.pigs.pigsblog.model.Result;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgCommentsService;
import com.pigs.pigsblog.util.ResultGenerator;
import com.pigs.pigsblog.util.ResultPagingUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;




/**
 * 评论管理表
 * @author manager
 * @email *****@mail.com
 * @date 2020-06-01 11:07:48
 */
@Controller
@RequestMapping("/")
public class PgCommentsController {
    private Logger logger= LoggerFactory.getLogger(PgCommentsController.class);
    @Autowired
    private PgCommentsService pgCommentsService;


    /**
    * 跳转到页面
    */
    @GetMapping("/index/pgComments")
    public String pgComments() {
        return "pgcomments/list";
        }

    @ApiOperation(value = "新增")
    @PostMapping("pgComments/add")
    @RequiresPermissions("pgComments:add")  //@ApiParam("JSON参数")
    @ResponseBody
    public DataResult add(@RequestBody  PgComments pgComments){
        pgCommentsService.save(pgComments);
        return DataResult.success();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("pgComments/delete")
    @RequiresPermissions("pgComments:delete")
    @ResponseBody
    public DataResult delete(@RequestBody @ApiParam(value = "id集合") List<String>  commentId){
        System.out.println("ids的值是："+commentId.toString());
        pgCommentsService.removeByIds(commentId);
        return DataResult.success();
    }

    @ApiOperation(value = "更新")
    @PutMapping("pgComments/update")
    @RequiresPermissions("pgComments:update")
    @ResponseBody
    public DataResult update(@RequestBody PgComments pgComments){
        pgCommentsService.updateById(pgComments);
        return DataResult.success();
    }

    @ApiOperation(value = "查询分页数据")
    @PostMapping("pgComments/listByPage")
    @ResponseBody
    public DataResult findListByPage(PgComments pgComments ){
        System.out.println("进来"+pgComments);
       Page page = new Page(pgComments.getPage(), pgComments.getLimit());
        //查询条件示例
        //queryWrapper.eq("id", pgComments.getId());
        IPage<PgComments> iPage = pgCommentsService.findListByPage(page);

        System.out.print(iPage.getRecords());
       return DataResult.success(iPage);
      /*  System.out.println("进来了："+page);
        if (page !=null && limit!=null){
         IPage<PgComments> result =pgCommentsService.findListByPage(page,limit,pgComments);
            System.out.println(result);
          List<PgComments>pgCommentsList=result.getRecords();
        }
        return ResultPagingUtil.pagingSuccess(0,0,"失败。。。");*/
    }

    @ResponseBody  // 1为已审核,0为未审核
    @PostMapping("pgComments/v1/comment/commentstate")
    public Result updateLinkStatus(PgComments PgComments){
        boolean flag = pgCommentsService.updateById(PgComments);
        if (flag){
            return ResultGenerator.getResultByHttp(HttpStatusConstants.OK);
        }
        return ResultGenerator.getResultByHttp(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

}
