package com.pigs.pigsblog.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgOnLine;
import com.pigs.pigsblog.mapper.PgOnLineMapper;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgOnLineService;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import org.apache.shiro.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/tiOnline")
public class PgOnLineController {

    private Logger logger = LoggerFactory.getLogger(PgOnLineController.class);


    @Autowired
    private PgOnLineService pgOnLineService;

    @Autowired
    private PgOnLineMapper pgOnLineMapper;


    /**
     * 查看所有在线用户
     *
     * @return
     */
    @GetMapping("/queryOnlineUser")
    public ResultFormatPaging queryOnlineUser(PgOnLine pgOnLine, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {
        logger.info("pgOnLine={}", pgOnLine);

        IPage<PgOnLine> onlineIPage = pgOnLineService.queryOnlineUser(pgOnLine, page, limit);
        List<PgOnLine> records = onlineIPage.getRecords();
        return ResultPagingUtil.pagingSuccess(0, onlineIPage.getTotal(), records);
    }


    /**
     * 强制注销当前登录用户
     *
     * @param onLineId
     * @return
     */
    @PutMapping("/forceLogout")
    public ResultFormat forceLogout(String onLineId) {
        System.out.println("进来");
        logger.info("onLineId={}", onLineId);
        if (onLineId.equalsIgnoreCase((String) SecurityUtils.getSubject().getSession().getId())) {
            return ResultUtil.error(100, "无法强退当前登陆用户");
        }
        PgOnLine readSession = pgOnLineMapper.selectById(onLineId);
        if (readSession != null) {
            if (readSession.getLineState().equals(1)) {
                return ResultUtil.error(100, "用户已下线");
            }
        }
        try {
            pgOnLineService.forceLogout(onLineId);
            return ResultUtil.success();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("e.getMessage()={}", e.getMessage());
            return ResultUtil.error(100, "踢出用户失败");
        }
    }


    /**
     * 批量强制注销当前登录用户
     *
     * @param onLineId
     * @return
     */
    @PutMapping("/forceLogouts")
    public ResultFormat forceLogouts(@RequestParam(value = "onLineId[]") String[] onLineId) {
        for (String sessionId : onLineId) {
            logger.info("onLineId={}", onLineId);
            if (sessionId.equalsIgnoreCase((String) SecurityUtils.getSubject().getSession().getId())) {
                return ResultUtil.error(100, "无法强退当前登陆用户");
            }
            PgOnLine readSession = pgOnLineMapper.selectById(sessionId);
            if (readSession != null) {
                if (readSession.getLineState().equals(1)) {
                    return ResultUtil.error(100, "用户已下线");
                }
            }
            pgOnLineService.forceLogout(sessionId);
        }
        return ResultUtil.success();

    }

}
