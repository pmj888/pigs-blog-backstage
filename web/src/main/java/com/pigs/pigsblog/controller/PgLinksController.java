package com.pigs.pigsblog.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgLinks;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgLinksService;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 友情鏈接 前端控制器
 * </p>
 *
 * @author PIGS
 * @since 2020-05-30
 */
@RestController
@RequestMapping("/pgLinks/")
public class PgLinksController {

    private Logger logger = LoggerFactory.getLogger(PgLinksController.class);

    @Autowired
    private PgLinksService pgLinksService;

    /**
     * 查询友情鏈接并分页
     *
     * @param pgLinks
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("queryLinkList")
    @ApiOperation(value = "查询友情鏈接")
    ResultFormatPaging queryLinkList(PgLinks pgLinks, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {
        logger.info("pgLinks={}", pgLinks);
        IPage<PgLinks> pgLinksIPage = pgLinksService.queryLinkList(pgLinks, page, limit);
        List<PgLinks> records = pgLinksIPage.getRecords();
        for (PgLinks record : records) {
            logger.info("record={}", record);
        }
        return ResultPagingUtil.pagingSuccess(0, pgLinksIPage.getTotal(), records);
    }

    /**
     * 添加友情鏈接
     *
     * @return
     */
    @PostMapping("saveLink")
    @ApiOperation(value = "添加友情鏈接")
    public ResultFormat saveLink(PgLinks pgLinks) {
        logger.info("pgLinks={}", pgLinks);
        Integer saveLink = pgLinksService.saveLink(pgLinks);
        logger.info("saveLink={}", saveLink);
        if (saveLink > 0) {
            return ResultUtil.success("操作成功");
        }
        return ResultUtil.success(100, "操作失败");
    }

    /**
     * 编辑友情鏈接
     *
     * @return
     */
    @PutMapping("editLink")
    @ApiOperation(value = "编辑友情鏈接")
    public ResultFormat editLink(PgLinks pgLinks) {
        Integer editLink = pgLinksService.editLink(pgLinks);
        if (editLink > 0) {
            return ResultUtil.success("操作成功");
        }
        return ResultUtil.success(100, "操作失败");
    }

    /**
     * 批量删除友情链接
     * 把状态设置为1：删除
     *
     * @return
     */
    @PutMapping("delBatchLink")
    @ApiOperation(value = "批量删除友情链接")
    public ResultFormat delBatchLink(Integer websiteState, @RequestParam(value = "websiteId[]") Integer[] websiteId) {
        logger.info("websiteState={}", websiteState);

        PgLinks pgLinks = new PgLinks();
        pgLinks.setWebsiteState(1);
        boolean updateById = false;
        for (Integer integer : websiteId) {
            pgLinks.setWebsiteId(integer);
            updateById = pgLinksService.updateById(pgLinks);
        }
        if (updateById) {
            return ResultUtil.success(200, "操作成功");
        }
        return ResultUtil.success(100, "操作失败");
    }

}
