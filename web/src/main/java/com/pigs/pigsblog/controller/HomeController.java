package com.pigs.pigsblog.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @author PIGS
 * @since 2020-04-15
 * <p>
 * 用户前端控制器
 */
@Controller
public class HomeController {
    /**
     * 进入首页
     *
     * @return
     */
    @GetMapping("/403.html")
    public String index() {
        return "403";
    }


}
