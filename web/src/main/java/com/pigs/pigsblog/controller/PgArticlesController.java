package com.pigs.pigsblog.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.*;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.*;
import com.pigs.pigsblog.util.Bse64ToMultipartUtil;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */

@RestController
@Api(value = "PgArticlesController", description = "文章管理相关接口")
@RequestMapping("/pgArticles/")
public class PgArticlesController {

    private Logger logger = LoggerFactory.getLogger(PgArticlesController.class);

    @Autowired
    private PgArticlesService pgArticlesService;

    @Autowired
    private PgLabelsService pgLabelsService;

    @Autowired
    private PgSortsService pgSortsService;

    @Autowired
    private pgArtitleLabelRefService pgArtitleLabelRefService;

    @Autowired
    private pgArtitleSortRefService pgArtitleSortRefService;


    /**
     * 查询 全部文章 并分页
     *
     * @param page  当前页
     * @param limit 每页记录数
     * @return
     */
    @ApiOperation(value = "分页查询全部文章记录")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "page", value = "当前页", required = true, dataType = "Integer", defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "每页记录数", required = true, dataType = "Integer", defaultValue = "5")
    })
    @GetMapping("/queryArticlesList")
    public ResultFormatPaging queryPgArticlesList(String articleTitle, Integer page, Integer limit) {
        System.out.println("articleTitle" + articleTitle);
        if (page != null && limit != null) {
            //设置mybatisPlus分页
            Page<PgArticles> pgArticles = new Page<PgArticles>(page, limit);
            IPage<PgArticles> pgArticlesIPage = pgArticlesService.queryPgArticlesList(articleTitle, pgArticles);
            List<PgArticles> pgArticlesList = pgArticlesIPage.getRecords();

            if (pgArticlesList.size() > 0) {
                for (PgArticles articles : pgArticlesList) {
                    logger.info("查询全部文章成功={}", articles);
                }
                return ResultPagingUtil.pagingSuccess(0, pgArticlesIPage.getTotal(), pgArticlesList);
            }
        }

        return ResultPagingUtil.pagingError(500, 0L, "查询失败");
    }


    /**
     * 点击添加文章时查询全部标签
     *
     * @return
     */
    @ApiOperation(value = "点击添加文章时查询全部标签")
    @GetMapping("/queryLabelsList")
    public List<PgLabels> queryPgLabelsList() {
        return pgLabelsService.list();
    }


    /**
     * 点击添加文章时查询全部标签
     *
     * @return
     */
    @ApiOperation(value = "点击添加文章时查询全部分类")
    @GetMapping("/querySortsList")
    public ResultFormat queryPgSortsList() {
        List<PgSorts> list = pgSortsService.list();
        if (list.size() > 0) {
            return ResultUtil.success(list);
        }
        return ResultUtil.error(400, "查询分类失败");
    }


    /**
     * 发布文章
     *
     * @return
     */
    @ApiOperation(value = "发布文章")
    @PutMapping("/insertArticle")
    public ResultFormat insertPgArticle(@RequestBody Map<String, Object> maps) {
        logger.info("发布文章={}", maps);
        Integer integer = pgArticlesService.insertPgArticles(maps);
        if (integer > 0) {
            return ResultUtil.success("操作成功");
        }
        return ResultUtil.error(400, "操作失败");
    }

    /**
     * 修改操作数据回显
     *
     * @param articleId
     * @return
     */
    @ApiOperation(value = "修改操作数据回显")
    @GetMapping("/queryArticleById")
    public ResultFormat queryArticleById(String articleId) {

        PgArticles article = pgArticlesService.getById(articleId);
        pgLabelsService.getById(articleId);

        // 根据文章id查询标签列表
        QueryWrapper<pgArtitleLabelRef> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("article_id", articleId);
        List<pgArtitleLabelRef> pgArtitleLabelRefList = pgArtitleLabelRefService.list(queryWrapper);


        pgArtitleSortRef ArtitleSort = pgArtitleSortRefService.getById(articleId);


        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("article", article);
        maps.put("pgArtitleLabelRefList", pgArtitleLabelRefList);
        maps.put("ArtitleSort", ArtitleSort);

        if (article != null) {
            return ResultUtil.success(maps);
        }
        return ResultUtil.error(400, "根据文章id查询失败");
    }


    /**
     * 修改文章
     * @return
     */
    @ApiOperation(value = "修改文章")
    @PutMapping("/updateArticle")
    public ResultFormat updatePgArticle(@RequestBody Map<String, Object> maps) {
        System.out.println("修改文章" + maps);
        Integer integer = pgArticlesService.updatePgArticles(maps);
        if (integer > 0) {
            return ResultUtil.success("操作成功");
        }
        return ResultUtil.error(400, "操作失败");
    }


    /**
     * 根据id修改文章的状态码为 2
     * @param articleId
     * @return
     */
    @ApiOperation(value = "根据id修改文章的状态码为 2")
    @DeleteMapping("/delArticleById")
    public ResultFormat delPgArticleById(String articleId) {
       PgArticles pgArticles = new PgArticles();
        pgArticles.setArticleState(2);

        UpdateWrapper<PgArticles> updateWrapper = new UpdateWrapper<PgArticles>();
        updateWrapper.eq("article_id",articleId);
        boolean update = pgArticlesService.update(pgArticles,updateWrapper);
        if (update) {
            return ResultUtil.success("操作成功");
        }
        return ResultUtil.error(400, "操作失败");
    }

    /**
     * 修改文章的状态（文章展示或不展示）
     *
     * @param articleId
     * @param articleState
     * @return
     */
    @ApiOperation(value = "修改文章的状态（展示或不展示）")
    @PostMapping("/updateArticleState")
    public Boolean updateArticleState(String articleId, String articleState) {
        PgArticles pgArticles = new PgArticles();
        pgArticles.setArticleState(Integer.valueOf(articleState));
        // 根据 UpdateWrapper 条件，更新记录 需要设置sqlset
        UpdateWrapper<PgArticles> updateWrapper = new UpdateWrapper<PgArticles>();
        updateWrapper.eq("article_id", articleId);
        return pgArticlesService.update(pgArticles, updateWrapper);
    }

    /**
     * 修改用户头像
     * 接受 base64 传过来的字节码
     *
     * @param
     * @return
     */
    @PostMapping("/editArticleImage")
    @ApiOperation(value = "添加文章封面图")
    public ResultFormat editArticleImage(@RequestParam(value = "img", required = false) String img, HttpServletRequest request, HttpServletResponse response) throws IOException {

        long startTime = System.currentTimeMillis();
        MultipartFile myFile = Bse64ToMultipartUtil.base64ToMultipart(img);

        logger.info("文件类型={}", myFile.getContentType());
        logger.info("文件大小={}", myFile.getSize());
        logger.info("文件原名称={}", myFile.getOriginalFilename());

        if (!myFile.isEmpty()) {
            String myFileOriginalFilename = myFile.getOriginalFilename();
            FileUtils.copyInputStreamToFile(myFile.getInputStream(), new File("D:\\apache-tomcat-9.0.0\\webapps\\image\\", myFileOriginalFilename));
            logger.info("上传成功");

            /**
             * 运行成功之后的时间
             * 上传成功之后保存到tomcat容器中
             */
            long endTime = System.currentTimeMillis();
            float excTime = (float) (endTime - startTime) / 1000;
            System.out.println("执行时间：" + excTime + "s");
            return ResultUtil.success(200, "上传成功..", "http://192.168.100.4:8080/image/" + myFileOriginalFilename);

        }
        return ResultUtil.error(100, "上传失败..");
    }

}
