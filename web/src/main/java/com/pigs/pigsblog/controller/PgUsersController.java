package com.pigs.pigsblog.controller;


import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgOnLine;
import com.pigs.pigsblog.entity.PgUsers;
import com.pigs.pigsblog.mapper.PgOnLineMapper;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgUsersService;
import com.pigs.pigsblog.util.*;
import eu.bitwalker.useragentutils.UserAgent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author PIGS
 * @since 2020-04-15
 * <p>
 * 用户前端控制器
 */
@RestController
@RequestMapping("/pgUsers/")
@Api(value = "/pgUsers", description = "用户前端控制器")
public class PgUsersController {

    private Logger logger = LoggerFactory.getLogger(PgUsersController.class);

    @Autowired
    private OosUtil ossUtil;

    @Autowired
    private PgUsersService pgUsersService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SessionDAO sessionDAO;

    @Autowired
    private PgOnLineMapper pgOnLineMapper;

    /**
     * 登录的用户信息保存到在线用户
     * 同一浏览器如果上一个用户没有退出然后返回登录页面进行登录，这时候sessionID是一致的导致存数据的时候是上一次的用户登录的信息
     * 如果登录的时候SESSIONID一致判断当前登录的用户信息用户名是否一致，如果一致就不更改在线用户信息，如果不一致则修改
     */
    public void onlineUser() {
        Collection<Session> sessions = sessionDAO.getActiveSessions();
        System.out.println("进来");
        for (Session session : sessions) {
            System.out.println("进来");
            PgOnLine pgOnLine = new PgOnLine();
            PgUsers user = new PgUsers();
            SimplePrincipalCollection principalCollection = new SimplePrincipalCollection();
            if (session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY) == null) {
                continue;
            } else {
                principalCollection = (SimplePrincipalCollection) session
                        .getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
                user = (PgUsers) principalCollection.getPrimaryPrincipal();
                pgOnLine.setLineLoginName(user.getUserName());
            }
            pgOnLine.setOnLineId((String) session.getId());
            pgOnLine.setLineIpAddress("0:0:0:0:0:0:0:1".equals(session.getHost()) ? "127.0.0.1" : session.getHost());
            pgOnLine.setLineCreateTime(TimeUtitl.setTime(session.getStartTimestamp()));
            pgOnLine.setLineLastTime(TimeUtitl.setTime(session.getLastAccessTime()));

            UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
            // 获取客户端操作系统
            String os = userAgent.getOperatingSystem().getName();
            // 获取客户端浏览器
            String browser = userAgent.getBrowser().getName();
            pgOnLine.setLineBrowserType(browser);
            pgOnLine.setLineOs(os);
            session.setTimeout(1L);
            Long timeout = session.getTimeout();

            if (timeout == 0L) {
                pgOnLine.setLineState(1);
            } else {
                pgOnLine.setLineState(0);
            }
            Integer outTime = Math.toIntExact(timeout);
            pgOnLine.setLineExpireTime(outTime);
            PgOnLine online = pgOnLineMapper.selectById(pgOnLine);
            logger.info("online={}", online);

            if (online == null) {
                Integer insertOnline = pgOnLineMapper.insert(pgOnLine);
                logger.info("insertOnline={}", insertOnline);
            } else if (online != null) {
                if (online.getOnLineId().equals(session.getId())) {
                    Integer updateOnlineById = pgOnLineMapper.updateById(pgOnLine);
                    logger.info("updateOnlineById={}", updateOnlineById);
                } else if (!online.getLineLoginName().equals(pgOnLine.getLineLoginName())) {
                    Integer updateOnlineById = pgOnLineMapper.updateById(pgOnLine);
                    logger.info("updateOnlineById={}", updateOnlineById);
                }
            }
        }
    }


    /**
     * 用户登录
     *
     * @param pgUsers
     * @return
     */
    @PostMapping("goLogin")
    @ApiOperation(value = "用户登录")
    @CrossOrigin
    public ResultFormat pgUsersLogin(PgUsers pgUsers) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(pgUsers.getUserName(), pgUsers.getUserPassword());
        try {
            subject.login(token);
            if (subject.isAuthenticated()) {
                logger.info("登录成功");
              /*  onlineUser();*/
                return ResultUtil.success(200, "登录成功");
            }
        } catch (LockedAccountException l) {
            logger.info("账号不存在：");
            return ResultUtil.error(100, "账户已被锁定");
        } catch (UnknownAccountException e) {
            logger.info("账号不存在：");
            return ResultUtil.error(404, "账号不存在");
        } catch (IncorrectCredentialsException ice) {
            logger.info("密码错误：");
            return ResultUtil.error(100, "密码输入错误");
        } catch (Exception e) {
            logger.info("发生错误={}", e.getMessage());
        }

        return ResultUtil.error(500, "稍后重试！");
    }

    /**
     * 从shiro中获取用户信息
     *
     * @return
     */
    @GetMapping("getShiroUserInfo")
    @ApiOperation(value = "从shiro中获取用户信息")
    public ResultFormat getShiroUserInfo() {
        Subject subject = SecurityUtils.getSubject();
        PgUsers pgUsersSubject = (PgUsers) subject.getPrincipal();
        PgUsers pgUsers = pgUsersService.queryUserInfo(pgUsersSubject);
        logger.info("pgUsers ={}", pgUsers);
        if (pgUsers != null) {
            return ResultUtil.success(pgUsers);
        }

        return ResultUtil.error(100, "请重新登录！");
    }

    /**
     * 查询用户列表
     * 并分页
     * 首先是从service返回的数据
     * 再从shiro中获取当前登录的用户
     * 然后过滤掉当前登录的用户
     *
     * @param pgUsers
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("queryUserList")
    @ApiOperation(value = "查询用户列表")
    ResultFormatPaging queryUserList(PgUsers pgUsers, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {
        logger.info("pgUsers={}", pgUsers);
        IPage<PgUsers> pgUsersIPage = pgUsersService.queryUserList(pgUsers, page, limit);
        List<PgUsers> records = pgUsersIPage.getRecords();
        Subject subject = SecurityUtils.getSubject();
        PgUsers pgUsersSubject = (PgUsers) subject.getPrincipal();
        List<PgUsers> collect = records.stream().filter(PgUsers -> !(PgUsers.getUserId().equals(pgUsersSubject.getUserId()))).collect(Collectors.toList());
        return ResultPagingUtil.pagingSuccess(0, pgUsersIPage.getTotal(), collect);
    }

    /**
     * 查询用户信息
     *
     * @return
     */
    @PostMapping("queryUserInfo")
    @ApiOperation(value = "查询用户信息")
    ResultFormat queryUserInfo(PgUsers pgUsers) {

        PgUsers pgUsers1 = pgUsersService.queryUserInfo(pgUsers);
        if (pgUsers1 != null) {
            return ResultUtil.success(pgUsers1);
        }
        return ResultUtil.success(100, "暂无数据..");
    }


    /**
     * 修改 用户 状态
     *
     * @param userState
     * @param userId
     * @return
     */
    @PutMapping("updateUserState")
    @ApiOperation(value = "修改用户状态")
    public ResultFormat updateUserState(Integer userState, Long userId) {
        PgUsers pgUsers = new PgUsers();
        logger.info("userState,userId={}", userState + " -- " + userId);

        if (userState != null && userId != null) {
            pgUsers.setUserId(userId);
            pgUsers.setUserState(userState);
            if (pgUsers.getUserState() == 1) {
                Long delete = redisUtil.delete("pgUsers:" + userId);
                logger.info("delete={}", delete);
            }
            pgUsers.setUserRegistrationTime(null);
            boolean update = pgUsersService.updateById(pgUsers);
            logger.info("update={}", update);

            return ResultUtil.success(update);
        }

        return ResultUtil.error(100, "修改失败");
    }

    /**
     * 修改 用户信息
     *
     * @param pgUsers
     * @param roleId
     * @param request
     * @return
     */
    @PutMapping("editUser")
    @ApiOperation(value = "修改用户信息")
    public ResultFormat editUser(PgUsers pgUsers, Integer roleId, HttpServletRequest request) throws Exception {

        logger.info("pgUsers={}", pgUsers);
        logger.info("roleId={}", roleId);
        pgUsers.setUserIp(SystemUtils.getIpAddress(request));
        Integer editUser = pgUsersService.editUser(pgUsers, roleId);
        logger.info("editUser={}", editUser);
        if (editUser == 1) {
            return ResultUtil.success(editUser, "修改成功");
        }

        if (editUser == 101) {
            return ResultUtil.success(101, "用户名已存在");
        }

        return ResultUtil.error(100, "修改失败");
    }

    /**
     * 添加用户
     *
     * @return
     */
    @PostMapping("saveUser")
    @ApiOperation(value = "添加用户")
    public ResultFormat saveUser(PgUsers pgUsers, Integer roleId, HttpServletRequest request) throws Exception {
        pgUsers.setUserIp(SystemUtils.getIpAddress(request));
        Integer integer = pgUsersService.saveUser(pgUsers, roleId);
        return ResultUtil.success(integer, "成功");
    }

    /**
     * 修改用户头像
     * 接受 base64 传过来的字节码
     *
     * @param
     * @return
     */
    @PutMapping("/editUserImage")
    @ApiOperation(value = "修改用户头像")
    public ResultFormat editUserImage(@RequestParam(value = "img", required = false) String img) throws IOException {
        MultipartFile myFile = Bse64ToMultipartUtil.base64ToMultipart(img);
        logger.info("文件类型={}", myFile.getContentType());
        logger.info("文件大小={}", myFile.getSize());
        logger.info("文件原名称={}", myFile.getOriginalFilename());

        if (!myFile.isEmpty()) {
            /**
             * 上传成功之后保存到阿里云oos中
             */
            String img2Oss = ossUtil.uploadImg2Oss(myFile);
            logger.info("上传成功={}", img2Oss);
            Subject subject = SecurityUtils.getSubject();
            PgUsers pgUsersSubject = (PgUsers) subject.getPrincipal();
            PgUsers pgUsers = new PgUsers();
            AbstractWrapper wrapper = new QueryWrapper();
            if (pgUsersSubject != null) {
                wrapper.eq("user_id", pgUsersSubject.getUserId());
                pgUsers.setUserProfilePhoto("https://pigs.oss-cn-shenzhen.aliyuncs.com/image/" + img2Oss);
                boolean update = pgUsersService.update(pgUsers, wrapper);
                logger.info("flag = {}", update);
                if (update) {
                    return ResultUtil.success();
                }
            }
            return ResultUtil.success(200, "更新成功..");
        }
        return ResultUtil.error(100, "更新失败..");
    }


    /**
     * 修改用户密码
     *
     * @param pgUsers
     * @param formerPwd
     * @return
     */
    @PostMapping("editUserPwd")
    @ApiOperation(value = "修改用户密码")
    public ResultFormat editUserPwd(PgUsers pgUsers, String formerPwd) {
        Integer editUserPwd = pgUsersService.editUserPwd(pgUsers, formerPwd);
        logger.info("editUserPwd={}", editUserPwd);
        return ResultUtil.success(editUserPwd, "");
    }


}
