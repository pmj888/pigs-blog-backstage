package com.pigs.pigsblog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgSorts;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgSortsService;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 分类 控制层
 * <p>
 * 前端控制器
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/pgSorts")
public class PgSortsController {

    private Logger logger = LoggerFactory.getLogger(PgLabelsController.class);

    @Autowired
    private PgSortsService pgSortsService;


    /**
     * 分页全查询标签信息
     *
     * @return
     */
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "page", value = "当前页", required = true, dataType = "Integer", defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "每页记录数", required = true, dataType = "Integer", defaultValue = "5")
    })

    /**
     * 全查询分类信息
     *
     * @return
     */
    @ApiOperation("查询分类信息")
    @GetMapping("/list")
    public ResultFormatPaging pgPgSortsList(PgSorts pgSorts, Integer page, Integer limit) {
        logger.info("pgSorts={}", pgSorts);
        IPage<PgSorts> pgSortsListIPage = pgSortsService.selectSortsList(pgSorts, page, limit);
        List<PgSorts> pgSortsList = pgSortsListIPage.getRecords();
        logger.info("pgSortsList={}", pgSortsList);
        return ResultPagingUtil.pagingSuccess(0, pgSortsListIPage.getTotal(), pgSortsList);
    }

    /**
     * 添加分类信息
     *
     * @param pgSorts
     * @return
     */
    @ApiOperation("添加分类信息")
    @PostMapping("/save")
    @ResponseBody
    public ResultFormat savePgSorts(PgSorts pgSorts) {

        Integer insert = pgSortsService.savePgSorts(pgSorts);

        if (insert == 101) {
            logger.info("相同分类名已存在");
            return ResultUtil.error(101, "");
        }

        if (insert > 0) {
            logger.info("添加成功");
            return ResultUtil.success();
        } else {
            logger.info("添加失败");
            return ResultUtil.error(100, "添加失败");
        }

    }


    /**
     * 修改分类信息
     *
     * @param pgSorts
     * @return
     */
    @ApiOperation("修改分类信息")
    @PutMapping("/update")
    @ResponseBody
    public ResultFormat updatePgSorts(PgSorts pgSorts) {

        Integer updatetSorts = pgSortsService.updatePgSorts(pgSorts);

        if (updatetSorts == 101) {
            logger.info("相同分类名已存在");
            return ResultUtil.error(101, "相同分类名已存在！");
        }

        if (updatetSorts > 0) {
            logger.info("修改成功");
            return ResultUtil.success();
        } else {
            logger.info("修改失败");
            return ResultUtil.error(100, "修改失败");
        }

    }


    /**
     * 修改分类状态
     *
     * @param pgSorts
     * @return
     */
    @ApiOperation("修改分类状态")
    @PutMapping("/delete")
    public ResultFormat deletePgSorts(PgSorts pgSorts) {

        if (pgSorts.getSortState() != null && pgSorts.getSortId() != null) {
            logger.info("pgLabels={}", pgSorts);
            boolean flag = pgSortsService.updateById(pgSorts);

            if (flag) {
                logger.info("修改成功");
                return ResultUtil.success();
            }
        }
        return ResultUtil.error(100, "修改失败");
    }


    /**
     * 删除分类信息
     * 把分类状态改为 1
     *
     * @param sortId
     * @return
     */
    @DeleteMapping("delSorts")
    @ApiOperation(value = "删除菜单")
    public ResultFormat delSorts(Long sortId) {

        logger.info("sortId={}", sortId);
        Integer delSortsNum = pgSortsService.deletePgSorts(sortId);
        logger.info("delSortsNum={}", delSortsNum);
        return ResultUtil.success(delSortsNum, "");
    }


}
