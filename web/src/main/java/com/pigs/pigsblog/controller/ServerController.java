package com.pigs.pigsblog.controller;


import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.system.SystemServer;
import com.pigs.pigsblog.util.ResultUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务器监控
 *
 * @author pigs
 */
@Controller
@RequestMapping("/system/")
public class ServerController {

    @GetMapping("/systemInfo")
    public String server(ModelMap map) throws Exception {
        SystemServer server = new SystemServer();
        server.copyTo();
        map.put("server", server);
        String format = new DecimalFormat("0.00").format(((100 - server.getCpu().getFree())));
        map.put("cpuUse",format);
        return "systemInfo";
    }

    @GetMapping("/resourceUsage")
    @ResponseBody
    public ResultFormat mem() throws Exception {
        SystemServer server = new SystemServer();
        server.copyTo();
        List list = new ArrayList();
        list.add(server.getMem().getUsage());
        list.add(server.getMem().getTotal());
        return ResultUtil.success(list);
    }
}
