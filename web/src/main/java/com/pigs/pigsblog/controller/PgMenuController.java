package com.pigs.pigsblog.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.PgMenu;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgMenuService;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 *
 * @author PIGS
 * @since 2020-05-09
 */
@RestController
@RequestMapping("/pgMenu/")
public class PgMenuController {

    @Autowired
    private PgMenuService pgMenuService;

    private Logger logger = LoggerFactory.getLogger(PgMenuController.class);

    /**
     * 条件查询菜单
     *
     * @param pgMenu
     * @pgPermission
     */
    @GetMapping("queryMenuList")
    @ApiOperation(value = "条件查询菜单")
    public ResultFormatPaging queryMenuList(PgMenu pgMenu) {
        logger.info("pgMenu={}", pgMenu);
        List<PgMenu> pgMenuList = pgMenuService.queryPgMenuList(pgMenu);
        return ResultPagingUtil.pagingSuccess(0, (long) pgMenuList.size(), pgMenuList);
    }

    /**
     * 通过菜单id
     * 查询上级菜单
     * 查询菜单权限
     *
     * @param menuId
     * @return
     */
    @GetMapping("queryMenuByMenuId")
    @ApiOperation(value = "菜单id查询菜单信息")
    public ResultFormat queryMenuByMenuId(Long menuId) {
        logger.info("menuId={}", menuId);
        return ResultUtil.success(pgMenuService.queryMenuByMenuId(menuId));
    }


    /**
     * 修改 菜单 状态
     *
     * @param pgMenu
     * @return
     */
    @PutMapping("updateMenuState")
    @ApiOperation(value = "修改角色状态")
    public ResultFormat updatePermissionState(PgMenu pgMenu) {
        logger.info("pgMenu={}", pgMenu);
        if (pgMenu != null) {
            if (pgMenu.getMenuId() != null && pgMenu.getState() != null) {
                boolean updateById = pgMenuService.updateById(pgMenu);
                if (updateById) {
                    return ResultUtil.success(200, "修改成功");
                }
            }
        }
        return ResultUtil.error(100, "修改失败");
    }

    /**
     * 查询菜单转树形
     *
     * @pgPermission
     */
    @GetMapping("queryMenuTree")
    @ApiOperation(value = "查询菜单转树形")
    public ResultFormat queryMenuTree() {
        System.out.println("进来");
        List<PgMenuVo> allMenus = pgMenuService.getAllMenus();
        for (PgMenuVo allMenu : allMenus) {
            logger.info("allMenu={}", allMenu);
        }

        return ResultUtil.success(allMenus);
    }

    /**
     * 添加菜单
     *
     * @pgPermission
     */
    @PostMapping("saveMenu")
    @ApiOperation(value = "添加菜单")
    public ResultFormat saveMenu(PgMenu pgMenu, Long select) {
        logger.info("pgMenu={}", pgMenu);
        logger.info("select={}", select);
        Integer save = pgMenuService.saveMenu(pgMenu, select);
        logger.info("save={}", save);
        return ResultUtil.success(save, "");
    }


    /**
     * 编辑菜单
     *
     * @pgPermission
     */
    @PostMapping("editMenu")
    @ApiOperation(value = "编辑菜单")
    public ResultFormat editMenu(PgMenu pgMenu, Long select) {
        logger.info("pgMenu={}", pgMenu);
        logger.info("select={}", select);
        Integer save = pgMenuService.editMenu(pgMenu, select);
        logger.info("save={}", save);
        return ResultUtil.success(save, "");
    }

    /**
     * 删除 菜单 把菜单的状态设置为 2
     * 0 显示
     * 1 隐藏
     * 2 删除
     * 判断 是否是父菜单
     * 主有是子菜单才能设置状态为 2
     *
     * @param menuId
     * @return 101 存在子菜单,不允许删除
     * 102 菜单已分配,不允许删除
     * 100 删除失败
     * 200 删除成功
     */
    @DeleteMapping("delMenu")
    @ApiOperation(value = "删除菜单")
    public ResultFormat delMenu(Long menuId) {
        logger.info("menuId={}", menuId);
        Integer delMenuNum = pgMenuService.delMenu(menuId);
        logger.info("delMenuNum={}", delMenuNum);
        return ResultUtil.success(delMenuNum, "");
    }


}
