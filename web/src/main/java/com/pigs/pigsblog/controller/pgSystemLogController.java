package com.pigs.pigsblog.controller;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pigs.pigsblog.entity.PgSystemLog;
import com.pigs.pigsblog.mapper.PgSystemLogMapper;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 安详的苦丁茶
 * @date 2020/6/2011:07
 */

@RestController
@RequestMapping("/pgSystemLog/")
public class pgSystemLogController {

    @Autowired
    private PgSystemLogMapper pgSystemLogMapper;


    private Logger logger = LoggerFactory.getLogger(pgSystemLogController.class);

    /**
     * 查看所有系统日志
     *
     * @return
     */
    @ApiOperation("查询日志")
    @GetMapping("querySystemLog")
    public ResultFormatPaging querySystemLog(PgSystemLog systemLog, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {

        AbstractWrapper wrapper = new QueryWrapper<PgSystemLog>();
        if (systemLog != null) {
            if (!StringUtils.isEmpty(systemLog.getLogUserName())) {

            }
        }

        wrapper.orderByDesc("log_start_time");
        Page<PgSystemLog> iPage = new Page<PgSystemLog>(page, limit);
        Page selectPage = pgSystemLogMapper.selectPage(iPage, wrapper);
        List<PgSystemLog> records = selectPage.getRecords();
        return ResultPagingUtil.pagingSuccess(0, selectPage.getTotal(), records);
    }


    /**
     * 删除日志
     *
     * @param logId
     * @return
     */
    @ApiOperation("删除日志")
    @PutMapping("/delete")
    public ResultFormat delSystemLog(Integer logId) {
        logger.info("logId=={}", logId);

        PgSystemLog pgSystemLog = new PgSystemLog();
        pgSystemLog.setLogId(logId);
        pgSystemLog.setLogState(1);
        Integer flag = pgSystemLogMapper.updateById(pgSystemLog);

        if (flag > 0) {
            logger.info("修改成功");
            return ResultUtil.success();
        } else {
            logger.info("修改失败");
            return ResultUtil.error(100, "修改失败");
        }
    }


}
