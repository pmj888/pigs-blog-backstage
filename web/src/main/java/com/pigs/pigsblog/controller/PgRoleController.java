package com.pigs.pigsblog.controller;


import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pigs.pigsblog.entity.*;
import com.pigs.pigsblog.entity.vo.PgMenuVo;
import com.pigs.pigsblog.model.ResultFormat;
import com.pigs.pigsblog.model.ResultFormatPaging;
import com.pigs.pigsblog.service.PgRoleService;
import com.pigs.pigsblog.util.RedisUtil;
import com.pigs.pigsblog.util.ResultPagingUtil;
import com.pigs.pigsblog.util.ResultUtil;
import com.pigs.pigsblog.util.TimeUtitl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/pgRole/")
@Api(value = "/pgRole", description = "角色前端控制器")
public class PgRoleController {

    @Autowired
    private PgRoleService pgRoleService;

    @Autowired
    private RedisUtil redisUtil;

    private Logger logger = LoggerFactory.getLogger(PgRoleController.class);

    /**
     * 查询角色信息
     *
     * @return
     */
    @GetMapping("queryRoleInfo")
    @ApiOperation(value = "用户信息")
    public ResultFormat queryRoleInfo() {

        AbstractWrapper wrapper = new QueryWrapper();
        wrapper.eq("role_state", 0);
        List<PgRole> sqlList = pgRoleService.list(wrapper);
        if (sqlList.size() > 0 && sqlList != null) {

            redisUtil.del("pgRoleId");

            for (PgRole pgRole : sqlList) {
                redisUtil.del("pgRole:" + pgRole.getRoleId());
                redisUtil.set("pgRole:" + pgRole.getRoleId(), pgRole);
                redisUtil.lSet("pgRoleId", pgRole.getRoleId());
            }
            return ResultUtil.success(sqlList);
        }

        return ResultUtil.success(100, "暂无数据..");
    }

    /**
     * 通过id查询用户角色信息
     *
     * @param userId
     * @return
     */
    @PostMapping("queryUserRoleInfo")
    @ApiOperation(value = "通过id查询用户角色信息")
    public ResultFormat queryUserRoleInfo(Integer userId) {
        logger.info("userId={}", userId);
        PgUsersRoleRef pgUsersRoleRef = pgRoleService.queryUserRoleInfo(userId);
        if (pgUsersRoleRef != null) {
            return ResultUtil.success(pgUsersRoleRef);
        }
        return ResultUtil.success(100, "暂无数据");
    }

    /**
     * 修改 角色 状态
     *
     * @param
     * @param
     * @return
     */
    @PutMapping("updateRoleState")
    @ApiOperation(value = "修改角色状态")
    public ResultFormat deleteRole(PgRole pgRole) {
        if (pgRole.getRoleState() != null && pgRole.getRoleId() != null) {
            logger.info("pgRole={}", pgRole);
            boolean flag = pgRoleService.updateById(pgRole);
            if (flag) {
                logger.info("修改成功");
                return ResultUtil.success();
            }
        }
        return ResultUtil.error(100, "修改失败");
    }

    /**
     * 条件查询角色信息并分页
     *
     * @param pgRole
     * @return
     */
    @GetMapping("queryRoleList")
    @ApiOperation(value = "条件查询角色信息并分页")
    public ResultFormatPaging queryRoleList(PgRole pgRole, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {
        IPage<PgRole> pgRoleIPage = pgRoleService.queryRoleList(pgRole, page, limit);
        List<PgRole> records = pgRoleIPage.getRecords();
        logger.info("recordsPgRole={}", records);
        return ResultPagingUtil.pagingSuccess(0, pgRoleIPage.getTotal(), records);
    }

    /**
     * 添加角色
     *
     * @return
     */
    @PostMapping("saveRole")
    @ApiOperation(value = "添加角色")
    public ResultFormat saveRole(PgRole pgRole, String select) throws Exception {
        Integer saveRole = pgRoleService.saveRole(pgRole, select);
        return ResultUtil.success(saveRole, "");
    }

    /**
     * 查询角色
     *
     * @return
     */
    @PostMapping("queryRoleInfo")
    @ApiOperation(value = "添加角色")
    public ResultFormat queryRoleInfo(PgRole pgRole) throws Exception {
        PgRole queryRoleInfo = pgRoleService.queryRoleInfo(pgRole);
        if (queryRoleInfo != null) {
            return ResultUtil.success(queryRoleInfo);
        }
        return ResultUtil.success(100, "暂无数据");
    }


    /**
     * 修改角色信息
     *
     * @return
     */
    @PutMapping("editRole")
    @ApiOperation(value = "修改角色信息")
    public ResultFormat editRole(PgRole pgRole, String select) {
        logger.info("select={}", select);

        Integer editRole = pgRoleService.editRole(pgRole, select);

        return ResultUtil.success(editRole, "");
    }

    /**
     * 通过角色id查询菜单权限
     *
     * @param roleId
     * @pgPermission
     */
    @GetMapping("queryRoleMenuRefListByRoleId")
    @ApiOperation(value = "查询角色菜单关联数据")
    public ResultFormat queryRoleMenuRefList(Long roleId) {
        logger.info("roleId={}", roleId);
        List<PgMenuVo> queryRoleMenuRefListByRoleId = pgRoleService.queryRoleMenuRefListByRoleId(roleId);
        if (queryRoleMenuRefListByRoleId.size() > 0 && queryRoleMenuRefListByRoleId != null) {
            return ResultUtil.success(200, "查询成功", queryRoleMenuRefListByRoleId);
        }
        return ResultUtil.success(100, "操作失败");
    }

}
