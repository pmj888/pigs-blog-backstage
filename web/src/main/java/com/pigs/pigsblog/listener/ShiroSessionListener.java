package com.pigs.pigsblog.listener;

import java.util.concurrent.atomic.AtomicInteger;

import com.pigs.pigsblog.entity.PgOnLine;
import com.pigs.pigsblog.handle.BeanHeader;
import com.pigs.pigsblog.service.PgOnLineService;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Shiro会话监听器
 */

public class ShiroSessionListener implements SessionListener{

    private final AtomicInteger sessionCount = new AtomicInteger(0);

    private Logger logger = LoggerFactory.getLogger(ShiroSessionListener.class);


    public  boolean forceUserLogout(String sessionId) {
        if (sessionId != null) {
            PgOnLine online = new PgOnLine();
            online.setOnLineId(sessionId);
            online.setLineState(1);
            PgOnLineService beanTiOnlineService = BeanHeader.getBean(PgOnLineService.class);
            boolean editOnlineStateById = beanTiOnlineService.updateById(online);
            return editOnlineStateById;
        }
        return false;
    }

    @Override
    public void onStart(Session session) {
        System.out.println("会话创建：" + session.getId());
        sessionCount.incrementAndGet();
    }

    @Override
    public void onStop(Session session) {
        System.out.println("停止会话：" + session.getId());
        boolean forceLogoutOnStop = forceUserLogout((String) session.getId());
        logger.info("forceLogoutOnStop={}", forceLogoutOnStop);
        sessionCount.decrementAndGet();
    }

    @Override
    public void onExpiration(Session session) {
        System.out.println("超时会话：" + session.getId());
        boolean forceLogoutOnExpiration =forceUserLogout((String) session.getId());
        logger.info("forceLogoutOnExpiration={}", forceLogoutOnExpiration);
        sessionCount.decrementAndGet();
    }
}
