package com.pigs.pigsblog;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author plog
 */

@MapperScan("com.pigs.pigsblog.mapper")
@SpringBootApplication()
@EnableSwagger2
public class PigsBlogBackstageApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(PigsBlogBackstageApplication.class, args);
            System.out.println("Successful Start");
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}
