package com.pigs.pigsblog.config;


import com.pigs.pigsblog.listener.ShiroSessionListener;
import com.pigs.pigsblog.shiro.UserRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author PIGS-猪农·杨
 * @version 1.0
 * @date 2020/3/18 22:47
 * @effect shiro 配置类
 */
@Configuration
public class ShiroConfig {

    /**
     * 过滤器工厂
     *
     * @param securityManager
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        System.out.println("启动 ShiroConfiguration.shirFilter()");
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        /**
         * 拦截器 map 集合.
         */
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();

        /**
         * 配置不会被拦截的链接 顺序判断
         * anon 匿名 访问
         */
        filterChainDefinitionMap.put("/static/adminView/login/login.html", "anon");
        filterChainDefinitionMap.put("/pgUsers/goLogin", "anon");
        filterChainDefinitionMap.put("/static/**", "anon");
        filterChainDefinitionMap.put("/swagger-ui.html", "anon");
        filterChainDefinitionMap.put("/swagger-resources", "anon");
        filterChainDefinitionMap.put("/v2/api-docs", "anon");
        filterChainDefinitionMap.put("/webjars/springfox-swagger-ui/**", "anon");
        filterChainDefinitionMap.put("/configuration/security", "anon");
        filterChainDefinitionMap.put("/configuration/ui", "anon");
        filterChainDefinitionMap.put("/pgUsers/getShiroUserInfo", "anon");
        filterChainDefinitionMap.put("/pgUsers/editUserImage", "anon");

        filterChainDefinitionMap.put("/login.html", "anon");
        filterChainDefinitionMap.put("/index.html", "authc");

        /**
         * 用户管理 过率链
         */
        filterChainDefinitionMap.put("/pgUsers/queryUserList", "perms[pgUsers:queryUserList]");
        filterChainDefinitionMap.put("/pgUsers/updateUserState", "perms[pgUsers:updateUserState]");
        filterChainDefinitionMap.put("/pgUsers/saveUser", "perms[pgUsers:saveUser]");
        filterChainDefinitionMap.put("/pgUsers/queryUserInfo", "perms[pgUsers:queryUserInfo]");
        filterChainDefinitionMap.put("/pgUsers/editUser", "perms[pgUsers:editUser]");
        filterChainDefinitionMap.put("/pgUsers/editUserPwd", "perms[pgUsers:editUserPwd]");

        /**
         * 角色管理过率链
         */
        filterChainDefinitionMap.put("/pgRole/queryRoleList", "perms[pgRole:queryRoleList]");
        /*filterChainDefinitionMap.put("/pgRole/queryUserRoleInfo", "perms[pgRole:queryUserRoleInfo]");*/
        filterChainDefinitionMap.put("/pgRole/queryRoleInfo", "perms[pgRole:queryRoleInfo]");
        filterChainDefinitionMap.put("/pgRole/updateRoleState", "perms[pgRole:updateRoleState]");
        filterChainDefinitionMap.put("/pgRole/saveRole", "perms[pgRole:saveRole]");
        filterChainDefinitionMap.put("/pgRole/editRole", "perms[pgRole:editRole]");
        filterChainDefinitionMap.put("/pgRole/queryRoleMenuRefListByRoleId", "perms[pgRole:queryRoleMenuRefListByRoleId]");

        /**
         * 菜单管理过率链
         */
        filterChainDefinitionMap.put("/pgMenu/updateMenuState", "perms[pgMenu:updateMenuState]");
        filterChainDefinitionMap.put("/pgMenu/queryMenuList", "perms[pgMenu:queryMenuList]");
        filterChainDefinitionMap.put("/pgMenu/queryMenuInfo", "perms[pgMenu:queryMenuInfo]");
        filterChainDefinitionMap.put("/pgMenu/updateMenu", "perms[pgMenu:updateMenu]");
        filterChainDefinitionMap.put("/pgMenu/saveMenu", "perms[pgMenu:saveMenu]");
        /*filterChainDefinitionMap.put("/pgMenu/queryMenuTree", "perms[pgMenu:queryMenuTree]");*/
        filterChainDefinitionMap.put("/pgMenu/delMenu", "perms[pgMenu:delMenu]");
        filterChainDefinitionMap.put("/pgMenu/queryMenuByMenuId", "perms[pgMenu:queryMenuByMenuId]");

        /**
         * 文章管理过率链
         */
       /* filterChainDefinitionMap.put("/pgArticles/queryArticlesList", "perms[pgArticles:queryArticlesList]");
        filterChainDefinitionMap.put("/pgArticles/queryLabelsList", "perms[pgArticles:queryLabelsList]");
        filterChainDefinitionMap.put("/pgArticles/querySortsList", "perms[pgArticles:querySortsList]");*/
        filterChainDefinitionMap.put("/pgArticles/insertArticle", "perms[pgArticles:insertArticle]");
        filterChainDefinitionMap.put("/pgArticles/queryArticleById", "perms[pgArticles:queryArticleById]");
        filterChainDefinitionMap.put("/pgArticles/updateArticle", "perms[pgArticles:updateArticle]");
        filterChainDefinitionMap.put("/pgArticles/editArticleImage", "perms[pgArticles:editArticleImage]");
        filterChainDefinitionMap.put("/pgArticles/delArticleById", "perms[pgArticles:delArticleById]");
        filterChainDefinitionMap.put("/pgArticles/updateArticleState", "perms[pgArticles:updateArticleState]");

        filterChainDefinitionMap.put("/pgArticles/editArticleImage", "anon");

        /**
         * 配置退出 过滤器,其中的具体的退出代码Shiro已经实现了
         */
        filterChainDefinitionMap.put("/logout", "logout");

        /**
         * authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问
         * 过滤链定义，从上向下顺序执行，一般将/**放在最为下边
         */
        filterChainDefinitionMap.put("/swagger-ui.html", "anon");
        filterChainDefinitionMap.put("/**", "authc");

        /**
         * 如果不设置默认会自动寻找Web工程根目录下的"/login"页面
         */
        shiroFilterFactoryBean.setLoginUrl("/login.html");

        /**
         *
         * 登录成功后要跳转的链接
         * shiroFilterFactoryBean.setSuccessUrl("/index.html");
         */

        /**
         * 未授权界面
         */
        shiroFilterFactoryBean.setUnauthorizedUrl("/403.html");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }


    /**
     * 身份认证realm; (账号密码校验；权限等)
     *
     * @return MyShiroRealm
     */
    @Bean
    public UserRealm myShiroRealm() {
        UserRealm myShiroRealm = new UserRealm();
        myShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());
        return myShiroRealm;
    }


    /**
     * 安全管理器
     *
     * @return
     */
    @Bean
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(myShiroRealm());
        return securityManager;
    }


    /**
     * 凭证匹配器
     * （密码校验交给Shiro的SimpleAuthenticationInfo进行处理了
     * 修改下doGetAuthenticationInfo中的代码;
     *
     * @return
     */
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();

        /**
         * 散列算法:使用MD5算法
         * 加密一次
         */
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(2);
        return hashedCredentialsMatcher;
    }


    /**
     * Shiro生命周期处理器 * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    /**
     * 顾问自动代理创建者
     *
     * @return
     */
    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    /**
     * 授权属性来源顾问
     *
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
        return authorizationAttributeSourceAdvisor;
    }

    /**
     * redis管理
     * @return
     */
    public RedisManager redisManager() {
        RedisManager redisManager = new RedisManager();
        return redisManager;
    }

    /**
     * Redis缓存管理器
     * @return
     */
    public RedisCacheManager cacheManager() {
        RedisCacheManager redisCacheManager = new RedisCacheManager();
        redisCacheManager.setRedisManager(redisManager());
        return redisCacheManager;
    }


    /**
     * Redis作为缓存实现
     *
     * @return
     */
    @Bean
    public RedisSessionDAO sessionDAO() {
        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
        redisSessionDAO.setRedisManager(redisManager());
        return redisSessionDAO;
    }


    /**
     * 在Shiro中，SessionDao通过
     * org.apache.shiro.session.mgt.SessionManager进行管理
     * 登录时去掉jsessionId
     *
     * @return
     */
    @Bean
    public SessionManager sessionManager() {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        Collection<SessionListener> listeners = new ArrayList<SessionListener>();
        listeners.add(new ShiroSessionListener());
        sessionManager.setSessionListeners(listeners);
        sessionManager.setSessionDAO(sessionDAO());
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }

}
