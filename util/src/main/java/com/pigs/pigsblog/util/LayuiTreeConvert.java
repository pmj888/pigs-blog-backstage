package com.pigs.pigsblog.util;

import java.util.function.Function;

/**
 * @author PIGS
 * @version 0.0.1
 * @date 2020/5/18 15:18
 * <p>
 * layui 转换树
 **/
@FunctionalInterface
public interface LayuiTreeConvert<T, R> {

    /**
     * 实体转换函数
     *
     * @param function 类型转换函数
     * @param t        原类型
     * @return R 转换后的类型
     */
    R convertTreeNode(Function<T, R> function, T t);

}
