package com.pigs.pigsblog.exception.code;

public interface ResponseCodeInterface {
    int getCode();

    String getMsg();
}
