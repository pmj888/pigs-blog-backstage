package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 安详的苦丁茶
 * @date 2020/6/2011:04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pg_system_log")
public class PgSystemLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志id
     */
    @TableId(value = "log_id", type = IdType.AUTO)
    private Integer logId;

    /**
     * 用户名
     */
    private String logUserName;

    /**
     * 操作IP地址
     */
    private String logIpAddr;

    /**
     * 跟
     */
    private String logBasePath;

    /**
     * 描述
     */
    private String logDescription;

    /**
     * uri
     */
    private String logUri;

    /**
     * url
     */
    private String logUrl;

    /**
     * 请求类型
     */
    private String logMethod;

    /**
     * 0成功 1 失败 2 不显示
     */
    private Integer logState;

    /**
     * 请求参数
     */

    private Object logParameter;

    /**
     * 请求返回的结果
     */
    private String logResult;

    /**
     * 开始时间
     */
    private String logStartTime;

    /**
     * 消耗时间
     */
    private Integer logSpendTime;
}
