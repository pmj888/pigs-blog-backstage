package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class pgArtitleLabelRef implements Serializable {


    /**
     * 文章ID
     */
    @TableId(value = "article_id")
    private Long articleId;

    /**
     * 标签ID
     */
    private Long labelId;
}
