package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PgArticles implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 博文ID
     */
    @TableId(value = "article_id", type = IdType.AUTO)
    private Long articleId;

    /**
     * 发表用户ID
     */
    private Long userId;

    /**
     * 博文标题
     */
    private String articleTitle;

    /**
     * 博文内容
     */
    private String articleContent;

    /**
     * 浏览量
     */
    private Long articleViews;

    /**
     * 评论总数
     */
    private Long articleCommentCount;

    /**
     * 发表时间
     */
    private String articleDate;

    /**
     * 点赞次数
     */
    private Long articleLikeCount;

    /**
     * 文章状态
     */
    private Integer articleState;

    /**
     * Editor编辑器的格式
     */
    private String pgEditorFormat;

    /**
     * 发表用户
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 文章分类（类）
     */
    @TableField(exist = false)
    private PgSorts pgSorts;

    /**
     * 文章标签（集合）
     */
    @TableField(exist = false)
    private List<PgLabels> pgLabels;


}
