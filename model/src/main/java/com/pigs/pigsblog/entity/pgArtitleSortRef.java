package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文章与分类关系实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class pgArtitleSortRef {

    //文章id
    @TableId(value = "article_id")
    private Long articleId;

    //分类id
    private Long sortId;
}
