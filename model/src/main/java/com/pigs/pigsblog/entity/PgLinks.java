package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *  友情鏈接模型
 * </p>
 *
 * @author PIGS
 * @since 2020-05-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PgLinks implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 网站id
     */
    @TableId(value = "website_id", type = IdType.AUTO)
    private Integer websiteId;

    /**
     * 网站名称
     */
    private String websiteName;

    /**
     * 网站地址
     */
    private String websiteUrl;

    /**
     * 创建时间
     */
    private String creationTime;

    /**
     * 站长描述
     */
    private String authorMail;

    /**
     * 网站描述
     */
    private String websiteDescribe;


    /**
     * 链接状态
     */
    private Integer websiteState;


}
