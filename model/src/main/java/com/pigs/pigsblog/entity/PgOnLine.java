package com.pigs.pigsblog.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.shiro.session.mgt.SimpleSession;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PgOnLine extends SimpleSession implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户会话id
     */
    @TableId
    private String onLineId;

    /**
     * 登录账号
     */
    private String lineLoginName;

    /**
     * 登录IP地址
     */
    private String lineIpAddress;

    /**
     * 登录地点
     */
    private String lineLoginSite;

    /**
     * 浏览器类型
     */
    private String lineBrowserType;

    /**
     * 操作系统
     */
    private String lineOs;

    /**
     * 在线状态0在线 1 离线
     */
    private Integer lineState;

    /**
     * session创建时间
     */
    private String lineCreateTime;

    /**
     * session最后访问时间
     */
    private String lineLastTime;

    /**
     * 超时时间，单位为分钟
     */
    private Integer lineExpireTime;

    @Override
    public void setAttribute(Object key, Object value) {
        super.setAttribute(key, value);
    }

    @Override
    public Object removeAttribute(Object key) {
        return super.removeAttribute(key);
    }
}
