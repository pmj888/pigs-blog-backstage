package com.pigs.pigsblog.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("pg_sorts")
public class PgSorts implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类ID
     */
    @TableId(type = IdType.AUTO)
    private Long sortId;

    /**
     * 分类名称
     */
    @NotNull(message = "分类名称不能为空！")
    private String sortName;

    /**
     * 分类别名
     */
    @NotNull(message = "分类别名不能为空！")
    private String sortAlias;

    /**
     * 分类描述
     */
    @NotNull(message = "分类描述不能为空！")
    private String sortDescription;

    /**
     * 父分类ID
     */
    @NotNull(message = "父分类ID不能为空！")
    private Long parentSortId;

    /**
     * 分类状态
     */
    @NotNull(message = "分类状态不能为空！")
    private Integer sortState;


}
