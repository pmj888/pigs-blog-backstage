package com.pigs.pigsblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author PIGS
 * @version 0.0.1
 * @date 2020/5/18 16:53
 * 菜单vo类
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PgMenuVo {

    private Integer menuId;
    private String name;
    private Integer value;
    private Integer parentId;
    private Integer state;

    private List<PgMenuVo> children;
}
