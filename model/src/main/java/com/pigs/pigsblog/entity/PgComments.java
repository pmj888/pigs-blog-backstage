package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;



import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;


/**
 * 评论管理表
 *
 * @author manager
 * @email *****@mail.com
 * @date 2020-06-01 11:07:48
 */
@Data
@TableName("pg_comments")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

public class PgComments implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论ID
	 */
	@TableId(value = "comment_id", type = IdType.AUTO)
	private Long commentId;

	/**
	 * 发表用户ID
	 */
	@TableField("user_id")
	private Long userId;

	/**
	 * 评论博文ID
	 */
	@TableField("article_id")
	private Long articleId;

	/**
	 * 点赞数
	 */
	@TableField("comment_like_count")
	private Long commentLikeCount;

	/**
	 * 评论日期
	 */
	@TableField("comment_date")
//    @DateTimeFormat(pattern="yyyy-MM-dd")
      @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
//   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date commentDate;

	/**
	 * 评论内容
	 */
	@TableField("comment_content")
	private String commentContent;

	/**
	 * 父评论ID
	 */
	@TableField("parent_comment_id")
	private Long parentCommentId;

	/**
	 * 评论状态
	 */
	@TableField("comment_state")
	private Integer commentState;

    @ApiModelProperty(value = "第几页")
    @TableField(exist = false)
    private int page=1;

    @ApiModelProperty(value = "分页数量")
    @TableField(exist = false)
    private int limit=10;

    @TableField(exist = false)
    private String userName;

    @TableField(exist = false)
    private String articleTitle;

}
