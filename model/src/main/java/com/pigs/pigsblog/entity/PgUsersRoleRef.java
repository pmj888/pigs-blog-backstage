package com.pigs.pigsblog.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author PIGS
 * @since 2020-05-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PgUsersRoleRef implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId
    private Long usersId;

    /**
     * 角色
     */
    private Integer roleId;

    /**
     * 角色用户关联表创建时间
     */
    private String usersRoleRefCreateTime;

    /**
     * 角色用户关联表更新时间
     */
    private String usersRoleRefUpdateTime;

    /**
     * 角色用户关联状态
     */
    private Integer usersRoleRefState;


}
