package com.pigs.pigsblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author PIGS
 * @since 2020-04-15
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("pg_labels")
public class PgLabels implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标签ID
     */
    @TableId(type = IdType.AUTO)
    private Long labelId;

    /**
     * 标签名称
     */
    @NotNull(message = "标签名称不能为空！")
    private String labelName;

    /**
     * 标签别名
     */
    @NotNull(message = "标签别名不能空！")
    private String labelAlias;

    /**
     * 标签描述
     */
    @NotNull(message = "标签描述不能为空！")
    private String labelDescription;

    /**
     * 标签状态
     */
    @NotNull(message = "标签状态不能为空！")
    private Integer labelState;


}
